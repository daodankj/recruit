var app = getApp();
const Toptips = require('../../assets/libs/zanui/toptips/index');
const Toast = require('../../assets/libs/zanui/toast/toast');

Page({
  data: {
    canSendData:true,
    invite_id: 0, 
    content: 'toptips',
    duration: 3000,
    $zanui: {
      toptips: {
        show: false
      }
    },
    goldList:['3000-4000','4000-5000','5000-8000','8000-10000'],
    jobList:['普工','电工','叉车','焊工','文员','仓管','其他'],
    rangeKey:0,
    rangeKey2:0
  },
  showTopTips(mes) {
    this.setData({
      content:mes,
      $zanui: {
        toptips: {
          show: true
        }
      }
    });

    setTimeout(() => {
      this.setData({
        $zanui: {
          toptips: {
            show: false
          }
        }
      });
    }, this.data.duration);
  },
  
  onLoad: function (options) {
    this.setData({
        invite_id:options.invite_id
    })
  },

  onShow: function () {
  
  },

  formSubmit: function (e) {
    
    var that = this;
    //首先验证表单
    var ShowErr_Me = "";
    let Outdata = e.detail.value;
    Outdata.invite_id = that.data.invite_id;
    console.log(Outdata);
    if (Outdata.name.length < 2 || Outdata.name.length > 20) {
      ShowErr_Me = "请正确填写您的姓名";
    }
    if (!(/^1[34578]\d{9}$/.test(Outdata.phone))) {
      ShowErr_Me = "正确输入正确的手机号";
    } 
    if (ShowErr_Me != "") {
        that.showTopTips(ShowErr_Me);
        console.log(ShowErr_Me);
        return false;
    }
    if (!that.data.canSendData) {
        return;
    }
    that.data.canSendData = false;
    app.request('/Agent/invite_add', Outdata, function (data, ret) {
      console.log(data);
      app.success('提交成功!');
      //wx.navigateBack({});
      setTimeout(function () { that.data.canSendData = true; }, 1000)
      wx.switchTab({
        url: '/page/zh_index/index'
      })
    }, function (data, ret) {
      app.error(ret.msg);
      setTimeout(function () { that.data.canSendData = true; }, 1000)
    });
  },
  getPhoneNumber: function (e) {
    console.log(e.detail.errMsg);
    if (e.detail.iv == undefined) {
      return;
    }
    console.log(e.detail.iv);
    console.log(e.detail.encryptedData);

    let dataTMp = [];
    dataTMp.encryptedData = e.detail.encryptedData;
    dataTMp.iv = e.detail.iv;
    var that = this;
    app.request('/Usewechat/get_PhoneNum', dataTMp, function (data, ret) {
      console.log(data);
      app.success('成功获取手机号');
      //data = data.replace(/(^\s*)|(\s*$)/g, "");
      that.setData({
        phoneNumber: data.phoneNumber
      });
    }, function (data, ret) {
      //app.error(ret.msg);
    });
  },
  bindPickerChange: function(e){
    this.setData({
      rangeKey:e.detail.value
    })
  },
  bindPickerChange2: function(e){
    this.setData({
      rangeKey2:e.detail.value
    })
  },
})