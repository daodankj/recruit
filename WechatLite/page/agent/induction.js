// page/agent/induction.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    noNum:0,
    yesNum:0,
    loading: false,
    nodata: false,
    nomore: false,
  },
  page: 1,
  page_block: 10,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  getList:function(cb){
    let that = this;
    if (that.data.nomore == true || that.data.loading == true) {
      return;
    }
    this.setData({ loading: true });
    app.request('/agent/bm_list', {
      page: that.page,
      page_block: that.page_block,
    }, function (data, ret) {
      //console.log(data);
      that.setData({
        loading: false,
        noNum: data.noNum,
        yesNum: data.yesNum,
        nodata: that.page == 1 && data.list.length == 0 ? true : false,
        nomore: (that.page > 1 && data.list.length == 0) ||
          (that.page == 1 && data.list.length < that.page_block && data.list.length > 0) ? true : false,
        list: that.page > 1 ? that.data.list.concat(data.list) : data.list
      })
      that.page++;
      typeof cb == 'function' && cb(data);
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  //确认入职
  sureInduction: function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确认入职吗?',
      success: function (res) {
        if (res.confirm) {         
          app.request('/agent/sure_induction', {
            id: id,
          }, function (data, ret) {
            app.success('操作成功');
            that.onPullDownRefresh();
          }, function (data, ret) {
            app.error(ret.msg);
          });
        } else if (res.cancel) {
          
        }
      }
    })
  },

  //确认签合同
  sureContract:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确认签合同吗?',
      success: function (res) {
        if (res.confirm) {         
          app.request('/agent/sure_contract', {
            id: id,
          }, function (data, ret) {
            app.success('操作成功');
            that.onPullDownRefresh();
          }, function (data, ret) {
            app.error(ret.msg);
          });
        } else if (res.cancel) {
          
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    this.getList(function (data) {
      if (data.list.length == 0) {
        app.info("暂无更多数据");
      }
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})