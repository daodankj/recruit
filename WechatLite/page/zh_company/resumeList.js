// page/train/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  cash_model: [0, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000],
  data: {
    inputValue:'',
    YueXin: '月薪范围',
    XueLi: '学历要求',
    YueXin_num: 0,
    XueLi_num: 0,
    currentTab: -1,
    list:[],
    loading: false,
    nodata: false,
    nomore: false,
  },
  page: 1,
  page_block: 10,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  getList:function(cb){
    let that = this;
    if (that.data.nomore == true || that.data.loading == true) {
      return;
    }
    this.setData({ loading: true });
    app.request('/companyjob/get_resumelist', {
      page: that.page,
      page_block: that.page_block,
      Re_input: that.data.inputValue,
      YueXin: that.cash_model[that.data.YueXin_num],
      XueLi_num: that.data.XueLi_num,
      ShowCity_id:app.globalData.locData['ShowCity_id']
    }, function (data, ret) {
      //console.log(data);
      that.setData({
        loading: false,
        nodata: that.page == 1 && data.list.length == 0 ? true : false,
        nomore: (that.page > 1 && data.list.length == 0) ||
          (that.page == 1 && data.list.length < that.page_block && data.list.length > 0) ? true : false,
        list: that.page > 1 ? that.data.list.concat(data.list) : data.list
      })
      that.page++;
      typeof cb == 'function' && cb(data);
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.page = 1;
    // this.setData({ nodata: false, nomore: false });
    // this.getList(function () {
    //   wx.stopPullDownRefresh();
    // });
  },
  searchChange(e) {
    this.setData({
      inputValue: e.detail.value
    });
  },

  searchDone(e) {
    //console.error('search', e.detail.value)
    this.onPullDownRefresh();
  },

  handleCancel() {
    //console.error('cancel')
    this.onPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getList(function (data) {
      if (data.length == 0) {
        app.info("暂无更多数据");
      }
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 下拉切换
  hideNav: function () {
    this.setData({
      displays: "none",
      currentTab: -1,
    })
  },
  // 区域
  tabNav: function (e) {
    console.log('出问题' + e.target.dataset.current);
    if (e.target.dataset.current == undefined){
      return;
    }
    this.setData({
      displays: "block"
    })
  
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {

      var showMode = e.target.dataset.current == 0;

      this.setData({
        currentTab: e.target.dataset.current,
        isShow: showMode
      })
    }
  },
  clickYueXinNum: function (e) {
    console.log(e.target.dataset.num)
    if (e.target.dataset.num == this.data.YueXin_num){
      return;
    }

    this.setData({
      YueXin_num: e.target.dataset.num
    })
    this.setData({
      YueXin: e.target.dataset.name
    })
    this.setData({
      displays: "none"
    })
    console.log(e.target.dataset.name)

    this.onPullDownRefresh();
  },
  clickXueLiNum: function (e) {
    console.log(e.target.dataset.num)
    if (e.target.dataset.num == this.data.XueLi_num) {
      return;
    }
    this.setData({
      XueLi_num: e.target.dataset.num
    })
    this.setData({
      XueLi: e.target.dataset.name
    })
    this.setData({
      displays: "none"
    })
    console.log(e.target.dataset.name)
    this.onPullDownRefresh();
  },
  Cnagetabfun: function (e) {
    let num = e.detail.current;
    //console.log('自家家的+'+num);
    this.setData({
      currentTab: num
    })
  }
})