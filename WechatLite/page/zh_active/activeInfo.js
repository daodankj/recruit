var app = getApp();
const Dialog = require('../../assets/libs/zanui/dialog/dialog');
const util = require('../../utils/util');
Page({

  data: {
    id:null,
    agentId:0,
    info:{},
    phoneNumber:'',
    canSendData: true,
    content: 'toptips',
    duration: 3000,
    $zanui: {
      toptips: {
        show: false
      }
    },
    identify_status:0
  },
  getData: function(){
    //post 拖取数据
    var that = this;
    app.request('/zm_active/info', { id: that.data.id,agentId:that.data.agentId }, function (data, ret) {
      console.log(data);
      if (data.activeInfo.content != '') {
        data.activeInfo.content = data.activeInfo.content.replace(/[\n\r]/g, '<br>');
        data.activeInfo.content = app.towxml.toJson(data.activeInfo.content.replace(/: /g, ':'), 'html', that);
        //data.activeInfo.content = app.towxml.toJson(data.activeInfo.content, 'html', that);
        that.traverse(data.activeInfo.content);
      }
      
      that.setData({
        info: data,
      })

    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  onShow: function () {
    var that = this;
    //这里间隔获得
    app.Log_after_fun(function () {
      that.getData();
    });
    //this.getData();
  },
  onLoad: function (options) {
    var that = this;
    that.setData({
      identify_status: app.globalData.userInfo.identify_status
    });
    console.log(options);
    if (options.id != undefined) {
      that.setData({
        id: options.id
      })
    }
    if (options.agentId != undefined) {
      that.setData({
        agentId: options.agentId
      })
    }
    if (options.scene != undefined) {
      var scene = decodeURIComponent(options.scene);
      that.setData({
        id: scene
      })
    }
    // that.setData({
    //   QrcodeImg: (app.Isdebug ? app.QrPngUrl : app.apiUrl) + "Usewechat/get_Job_QrPng?id=" + that.data.id,
    // }) 
  },
 
  traverse: function (obj) {
    for (var a in obj) {
      if (typeof (obj[a]) == "object") {
        this.traverse(obj[a]); //递归遍历  
      } else {
        if (a == 'src' && obj[a].indexOf("/uploads/") == 0) {
          //console.log(a + "=" + obj[a]); 
          obj[a] = app.Domain_Public + obj[a];
        }
      }
    }
  },
  // telmake: function (options) {
  //   var that = this;
  //   wx.makePhoneCall({
  //     phoneNumber: that.data.comjob.recruitcompany.tel
  //   })
  // },
  share: function () {
    wx.showShareMenu({});
  },
  onShareAppMessage: function () {
    var that = this;
    var url = '/page/zh_active/activeInfo?id=' + that.data.id;
    return {
      title: '' + that.data.info.activeInfo.name,
      path: url
    }
  },
  getPhoneNumber: function (e) {
    console.log(e.detail.errMsg);
    if (e.detail.iv == undefined){
      return;
    }
    console.log(e.detail.iv);
    console.log(e.detail.encryptedData);

    let dataTMp = [];
    dataTMp.encryptedData = e.detail.encryptedData;
    dataTMp.iv = e.detail.iv;
    var that = this;
    app.request('/Usewechat/get_PhoneNum', dataTMp, function (data, ret) {
      console.log(data);
      app.success('成功获取手机号');
      //data = data.replace(/(^\s*)|(\s*$)/g, "");
      that.setData({
        phoneNumber: data.phoneNumber
      });
    }, function (data, ret) {
      //app.error(ret.msg);
    });
  },
  formSubmit: function (e) {
    var that = this;
    //首先验证表单
    var ShowErr_Me = "";
    let Outdata = e.detail.value;
    Outdata.active_id = that.data.id;
    Outdata.agent_user_id = that.data.agentId;
    console.log(Outdata);
    
    if (!(/^1[34578]\d{9}$/.test(Outdata.phone))) {
      ShowErr_Me = "正确输入正确的手机号";
    } 
    if (Outdata.name.length < 2 || Outdata.name.length > 20) {
      ShowErr_Me = "请正确填写您的姓名";
    }
    if (ShowErr_Me != "") {
      that.showTopTips(ShowErr_Me);
      console.log(ShowErr_Me);
      return false;
    }
    if (!that.data.canSendData) {
      return;
    }
    that.data.canSendData = false;
    app.request('/zm_active/add_baoming', Outdata, function (data, ret) {
      console.log(data);
      app.success('报名成功!');
      /*

      */
      setTimeout(function () { 
        // wx.redirectTo({
        //   url: '/page/zh_active/activeInfo?id=' + that.data.id
        // });
        that.getData();
        that.data.canSendData = true; 
        }, 1500)

    }, function (data, ret) {
      app.error(ret.msg);

      setTimeout(function () { that.data.canSendData = true; }, 1000)
    });
    
  },
  showTopTips(mes) {
    this.setData({
      content: mes,
      $zanui: {
        toptips: {
          show: true
        }
      }
    });

    setTimeout(() => {
      this.setData({
        $zanui: {
          toptips: {
            show: false
          }
        }
      });
    }, this.data.duration);
  },
  telmake: function (options) {
    var that = this;
    wx.makePhoneCall({
      phoneNumber: that.data.info.link_phone
    })
  },
})