// page/zh_active/addActive.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    start_date:'2021-12-01',
    end_date:'2021-12-01',
    head_open:true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  bindDateChange: function (e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      start_date: e.detail.value
    })
  },
  bindDateChange2: function (e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      end_date: e.detail.value
    })
  },
  headOpenChange:function(e){
    this.setData({
      head_open: e.detail.value
    })
  },
  formSubmit: function (event) {
    console.log(event.detail.value);
    var that = this;
    if (event.detail.value.name == '') {
      app.error('活动不能为空');
      return;
    }
    if (event.detail.value.settlement_type == '') {
      app.error('请选择结算方式');
      return;
    }
    if (event.detail.value.user_num <1) {
      app.error('招聘人数必须大于0');
      return;
    }
    if (event.detail.value.price < 1) {
      app.error('工时价必须大于1');
      return;
    }
    if (event.detail.value.content == '') {
      app.error('请输入活动说明');
      return;
    }
    app.request('/zm_active/add_active', event.detail.value, function (data) {
      app.success('添加成功!', function () {
        setTimeout(function () {
          //要延时执行的代码
          wx.navigateBack({delta:1});
        }, 2000); //延迟时间
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})