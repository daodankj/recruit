// page/zh_active/myActive.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeList:[],
    loading: false,
    nodata: false,
    nomore: false,
  },
  page: 1,
  page_block: 10,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },
  getList:function(){
    let that = this;
    if (that.data.nomore == true || that.data.loading == true) {
      return;
    }
    this.setData({ loading: true });
    app.request('/zm_active/get_my_active', {
      page: that.page,
      page_block: that.page_block,
    }, function (data, ret) {
      console.log(data);
      that.setData({
        loading: false,
        nodata: that.page == 1 && data.length == 0 ? true : false,
        nomore: (that.page > 1 && data.length == 0) ||
          (that.page == 1 && data.length < that.page_block && data.length > 0) ? true : false,
          activeList: that.page > 1 ? that.data.activeList.concat(data) : data
      })
      that.page++;
      typeof cb == 'function' && cb(data);
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    this.getList(function (data) {
      if (data.length == 0) {
        app.info("暂无更多数据");
      }
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})