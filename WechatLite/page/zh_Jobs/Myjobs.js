var app = getApp();
const Toptips = require('../../assets/libs/zanui/toptips/index');
const Toast = require('../../assets/libs/zanui/toast/toast');



Page({


  data: {
    comjob: [],
    ShowNoCom:false,
    Company:[]
  },

  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    app.Log_after_fun(function (ret) {
      //获得用户的职位列表
      app.request('/Companyjob/get_all_myjob', {}, function (data, ret) {
        that.setData({
          comjob: data.JobsList,
          Company: data.Company,
          ShowNoCom: false
        })

      }, function (data, ret) {
        //app.error(ret.msg);
        if (data == null && ret.msg == "不存在企业") {
          that.setData({
            ShowNoCom: true
          })
        }

      });
    });
  },
  zhuandaozj: function (e) {
    var that = this;
    wx.navigateTo({
      url: '/page/zh_Jobs/AddJob?com=' + that.data.Company.Id + '&name='+ that.data.Company.name 
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    //var that = this;
    if(e.from=="menu"){
      return false;
    }
    //新增发布次数
    app.request('/Companyjob/add_job_count', {c_id:this.data.Company.Id}, function (data, ret) {
       //app.success();
    }, function (data, ret) {
      //app.error(ret.msg);
    });
    let name = e.target.dataset.name;
    let job_id = e.target.dataset.id;
    return {
      title: name,
      path: '/page/zh_Jobs/ShowOneJob?id=' + job_id
    }
  },
  settop:function(e){
    var that = this;
    var job_id = e.target.dataset.id;
    app.request('/Wechatpay/createOrder', {money:1,type:2,job_id:job_id}, function (data,ret) {
      if(ret.code==0){
        app.error(ret.msg);
        return;
      }
      wx.requestPayment({
        timeStamp: data.timeStamp,
        nonceStr: data.nonceStr,
        package: data.package,
        signType: data.signType,
        paySign: data.paySign,
        success (res) {
          app.success('置顶成功!')
        },
        fail (res) {
          //console(res)
         }
      })
    }, function (data, ret) {
      app.error(ret.msg);
    });
},
  
})