
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo:null,
    idcard_img_front:'',
    idcard_img_backend:'',
    add_img_front:'/assets/img/card-z.png',
    add_img_backend:'/assets/img/card-f.png',
    status:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({ userInfo: app.globalData.userInfo });
    if(this.data.userInfo.idcard!=''){
      this.setData({ status: 1 });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!app.globalData.userInfo) {
      app.error("请登录后再操作", function () {
        setTimeout(function () { wx.navigateBack({}) }, 2000);
      });
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  edit: function (){
    this.setData({ status: 0 });
  },
  
  //上传正面
  uploadFront: function () {
    var that = this;
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              app.success('上传成功');
              that.setData({ 
                idcard_img_front: data.data.fullurl ,
                add_img_front:data.data.fullurl
              });
            }
          }
        });
      }
    });
  },
  //上传反面
  uploadBackend: function () {
    var that = this;
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              app.success('上传成功');
              that.setData({ 
                idcard_img_backend: data.data.fullurl ,
                add_img_backend:data.data.fullurl
              });
            }
          }
        });
      }
    });
  },
  formSubmit: function (event) {
    console.log(event.detail.value);
    var that = this;
    if (event.detail.value.username == '') {
      app.error('姓名不能为空');
      return;
    }
    if (event.detail.value.idcard == '') {
      app.error('身份证号码不能为空');
      return;
    }
    if (event.detail.value.mobile == '') {
      app.error('手机号码不能为空');
      return;
    }
    
    if (event.detail.value.idcard_img_front == '') {
      app.error('请上传身份证正面照片');
      return;
    }
    if (event.detail.value.idcard_img_backend == '') {
      app.error('请上传身份证反面照片');
      return;
    }
    app.request('/user/identify', event.detail.value, function (data) {
      that.setData({ userInfo: data.userInfo });
      app.globalData.userInfo = data.userInfo;
      app.success('提交成功!', function () {
        setTimeout(function () {
          //要延时执行的代码
          wx.navigateBack({delta:1});
        }, 2000); //延迟时间
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  // onPullDownRefresh: function () {

  // },

  /**
   * 页面上拉触底事件的处理函数
   */
  // onReachBottom: function () {

  // },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})