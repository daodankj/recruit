define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'user/user/index',
                    add_url: 'user/user/add',
                    edit_url: 'user/user/edit',
                    del_url: 'user/user/del',
                    multi_url: 'user/user/multi',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'user.id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'group.name', title: __('Group')},
                        {field: 'username', title: __('Username'), operate: 'LIKE'},
                        {field: 'nickname', title: __('Nickname'), operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        {field: 'avatar', title: __('Avatar'), events: Table.api.events.image, formatter: Table.api.formatter.image, operate: false},
                        //{field: 'level', title: __('Level'), operate: 'BETWEEN', sortable: true},
                        //{field: 'gender', title: __('Gender'), visible: false, searchList: {1: __('Male'), 0: __('Female')}},
                        {field: 'score', title: __('Score'), operate: 'BETWEEN', sortable: true},
                        //{field: 'successions', title: __('Successions'), visible: false, operate: 'BETWEEN', sortable: true},
                        //{field: 'maxsuccessions', title: __('Maxsuccessions'), visible: false, operate: 'BETWEEN', sortable: true},
                        //{field: 'logintime', title: __('Logintime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        //{field: 'loginip', title: __('Loginip'), formatter: Table.api.formatter.search},
                        {field: 'jointime', title: __('Jointime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        //{field: 'joinip', title: __('Joinip'), formatter: Table.api.formatter.search},
                        {field: 'identify_status', title: __('实名状态'), searchList: {0: __('未认证'),'-1':'待审核',1: __('已认证'),2:'认证失败'},formatter: Controller.api.formatter.identify_status},
                        {field: 'idcard', title: __('身份证号码'), operate: 'LIKE'},
                        {field: 'idcard_img_front', title: __('身份证正面'),operate:false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'idcard_img_backend', title: __('身份证反面'),operate:false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'status', title: __('Status'), formatter: Table.api.formatter.status, searchList: {normal: __('Normal'), hidden: __('Hidden')}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('审核'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'user/user/check',
                                extend:'data-area=["500px","450px"]',
                                hidden:function(value,row){
                                    if(value.identify_status == -1){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ],
                         formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==1?'确认通过吗？':'确认拒绝审核吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });       
            })
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                identify_status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">未认证</font>';
                    }else if(value==1){
                        return '<font color="green">已认证</font>';
                    }else if(value==2){
                        return '<font color="red">认证失败</font>';
                    }else if(value==-1){
                        return '<font color="blue">待审核</font>';
                    }
                },
            }
        }
    };
    return Controller;
});