define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'recruit/company/index',
                    add_url: 'recruit/company/add',
                    edit_url: 'recruit/company/edit',
                    del_url: 'recruit/company/del',
                    multi_url: 'recruit/company/multi',
                    table: 'recruit_company',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'Id',
                sortName: 'Id',
                //启用固定列
                fixedColumns: true,
                //固定右侧列数
                fixedRightNumber: 2,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'Id', title: __('Id')},
                        {field: 'type', title: __('类型'),searchList: {"1":'知名企业',"2":'最新企业',"3":'高薪急聘'},formatter: Controller.api.formatter.typeName},
                        {field: 'name', title: __('Name')},
                        {field: 'tel', title: __('Tel')},
                        {field: 'no', title: __('No')},
                        {field: 'xinzhi', title: __('Xinzhi'), visible:false, searchList: {"0":__('Xinzhi 0'),"1":__('Xinzhi 1'),"2":__('Xinzhi 2'),"3":__('Xinzhi 3'),"4":__('Xinzhi 4')}},
                        {field: 'xinzhi_text', title: __('Xinzhi'), operate:false},
                        {field: 'adress', title: __('Adress'),formatter: Controller.api.formatter.adress},
                        {field: 'cimage', title: __('Cimage'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'cimages', title: __('Cimages'), events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'money', title: __('余额'),operate:'BETWEEN'},
                        {field: 'score', title: __('评分'),operate:'BETWEEN'},
                        {field: 'user.username', title: __('User.username')},
                        {field: 'user.nickname', title: __('User.nickname')},
                        {field: 'recruitopencity.city', title: __('所属区域'),searchable:false},
                        /*{field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},*/     
                        {field: 'c_id', title: __('招聘职位'), table: table, buttons: [
                                {name: 'detail', text: '查看', title: '职位详情', icon: 'fa fa-list', classname: 'btn btn-xs btn-primary btn-dialog', 
                                url: 'recruit/job/index?c_id={row.Id}'},
                            ], operate:false, formatter: Table.api.formatter.buttons
                        },   
                        {field: 'job_count', title: __('剩余次数'),operate:'BETWEEN'},              
                        {field: 'status', title: __('Status'),searchList: {"0":'待审核',"1":'已通过',"2":'已拒绝'},formatter: Controller.api.formatter.status,sortable: true},
                        {
                            field: 'city_id', 
                            title: __('所属区域'),
                            visible: false,
                            addclass: 'selectpage',
                            extend: 'data-source="recruit/opencity/index" data-field="city" ',
                            formatter: Table.api.formatter.search
                        },
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('审核'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'recruit/company/check',
                                extend:'data-area=["500px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==1?'确认通过吗？':'确认拒绝审核吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });       
            })
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter:{
                adress:function (value, row, index) {
                    //console.log(row);
                    var length = value.length;
                    var count = 16;
                    if(length&&length>count){
                      return "<span title ='"+value+"'>"+value.substring(0,count)+"...</span>";
                    }else{
                      return value;
                    }
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审核</font>';
                    }else if(value==1){
                        return '<font color="green">已通过</font>';
                    }else{
                        return '<font color="red">已拒绝</font>';
                    }
                },
                typeName:function(value, row, index){
                    if (value==1) {
                        return '<font color="#FF9800">知名企业</font>';
                    }else if(value==2){
                        return '<font color="green">最新企业</font>';
                    }else{
                        return '<font color="red">高薪急聘</font>';
                    }
                },
            }
        }
    };
    return Controller;
});