define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'work_recod/index' + location.search,
                    //add_url: 'work_recod/add',
                    //edit_url: 'work_recod/edit',
                    //del_url: 'work_recod/del',
                    multi_url: 'work_recod/multi',
                    import_url: 'work_recod/import',
                    table: 'work_recod',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'phone', title: __('Phone'), operate: 'LIKE'},
                        {field: 'labor_name', title: __('Labor_id'),operate:false},
                        {field: 'work_name', title: __('Work_id'),operate:false},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        /*{field: 'user_id', title: __('User_id')},
                        {field: 'bm_id', title: __('Bm_id')},
                        {field: 'agent_user_id', title: __('Agent_user_id')},                     
                        {field: 'city_id', title: __('City_id')},
                        {field: 'service_id', title: __('Service_id')}, */                      
                        {field: 'work_time', title: __('Work_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'unbind_time', title: __('Unbind_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});