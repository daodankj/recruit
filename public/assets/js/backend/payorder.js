define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'payorder/index' + location.search,
                    add_url: 'payorder/add',
                    edit_url: 'payorder/edit',
                    del_url: 'payorder/del',
                    multi_url: 'payorder/multi',
                    import_url: 'payorder/import',
                    table: 'payorder',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id'),visible:false,addClass: "selectpage", extend: 'data-source="user/user/index" data-field="username" '},
                        {field: 'username', title: __('User_id'),operate:false},
                        {field: 'out_trade_no', title: __('Out_trade_no'), operate: 'LIKE'},
                        {field: 'amount', title: __('Amount'), operate:'BETWEEN'},
                        {field: 'status', title: __('Status'),searchList: {"0":'待支付',"1":'已支付'},formatter: Controller.api.formatter.status},
                        {field: 'remark', title: __('备注'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==1) {
                        return '<font color="green">已支付</font>';
                    }else{
                        return '<font color="#999999">待支付</font>';
                    }
                },
            }
        }
    };
    return Controller;
});