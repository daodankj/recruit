define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        //劳务工档案
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'active_bmlist/index' + location.search,
                    add_url: 'active_bmlist/add',
                    edit_url: 'active_bmlist/edit',
                    del_url: 'active_bmlist/del',
                    multi_url: 'active_bmlist/multi',
                    import_url: 'active_bmlist/import',
                    table: 'active_bmlist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'labor_id', title: __('劳务机构'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_labor/index" data-field="name" '},
                        {field: 'labor_name', title: __('劳务机构'), operate:false},
                        {field: 'name', title: __('姓名'), operate: 'LIKE'},
                        {field: 'phone', title: __('电话'), operate: 'LIKE'},
                        {field: 'idcard', title: __('身份证'), operate: 'LIKE'},
                        {field: 'user.nickname', title: __('Nickname'), operate: 'LIKE'},
                        {field: 'user.avatar', title: __('头像'), events: Table.api.events.image, formatter: Table.api.formatter.image, operate: false},
                        {field: 'user.level', title: __('等级'), operate: 'BETWEEN', sortable: true},
                        {field: 'user.gender', title: __('性别'), visible: false, searchList: {1: __('男'), 0: __('女')}},
                        {field: 'user.score', title: __('Score'), operate: 'BETWEEN', sortable: true},                     
                        {field: 'agent_user_id', title: __('推广员/团长'),visible:false,addclass: 'selectpage',extend: 'data-source="agent/index" data-field="username" data-primary-key="user_id"'},
                        {field: 'agent_name', title: __('推广员/团长'),operate:false},
                        {field: 'status', title: __('Status'),searchList: {"0":'待入职',"1":'已入职，待签合同',"2":'已签合同','3':'工作中','-1':'已失效'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'operate', title: __('Operate'),align:"right", table: table, events: Table.api.events.operate,
                            buttons:[
                                {
                                    name: 'showdaka',
                                    text: __('查看考勤'),
                                    icon: 'fa fa-eye',
                                    classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                    url: 'daka/index?user_id={row.user_id}'
                                },
                                {
                                    name: 'contract',
                                    text: __('查看合同'),
                                    icon: 'fa fa-file-powerpoint-o',
                                    classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                    url: 'active_bmlist/contract',
                                    //extend:'data-area=["400px","450px"]',
                                    hidden:function(value,row){
                                        if(value.status > 1){
                                            return false;
                                        }else return true;
                                    },
                                    success:function(data,ret){
                                        $(".btn-refresh").trigger('click');
                                    }
                                },
                                {
                                    name: 'leave',
                                    text: __('离职'),
                                    icon: 'fa fa-blind',
                                    classname: 'btn btn-xs btn-danger btn-magic btn-ajax',
                                    url: 'active_bmlist/leave',
                                    confirm: '确认离职？',
                                    refresh:true,
                                    hidden:function(value,row){
                                        if(value.status == 1||value.status == 2){
                                            return false;
                                        }else return true;
                                    },
                                    success:function(data,ret){
                                        $(".btn-refresh").trigger('click');
                                    }
                                }
                            ], 
                            formatter: function (value, row, index) {
                                if(row.status > 0){
                                    var that = $.extend({}, this);
                                    var table = $(that.table).clone(true);
                                    $(table).data("operate-del", null);
                                    that.table = table;
                                    return Table.api.formatter.operate.call(that, value, row, index);
                                    //return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index);
                            }
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        //报名列表
        worklist: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'active_bmlist/worklist' + location.search,
                    //add_url: 'active_bmlist/add',
                    //edit_url: 'active_bmlist/edit',
                    del_url: 'active_bmlist/del',
                    multi_url: 'active_bmlist/multi',
                    import_url: 'active_bmlist/import',
                    table: 'active_bmlist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'active_id', title: __('Active_id'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_active/index" data-field="name" '},
                        {field: 'active.name', title: __('Active_id'),operate:false},
                        {field: 'user_id', title: __('User_id'),visible: false,addclass: 'selectpage',extend: 'data-source="user/user/index" data-field="username" '},
                        /*{field: 'agent_user_id', title: __('Agent_user_id')},*/
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'phone', title: __('Phone'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'agent_user_id', title: __('Agent_user_id'),visible:false,addclass: 'selectpage',extend: 'data-source="agent/index" data-field="username" data-primary-key="user_id"'},
                        {field: 'agent_name', title: __('Agent_user_id'),operate:false},
                        {field: 'status', title: __('Status'),searchList: {"0":'待入职',"1":'已入职，待签合同',"2":'已签合同','3':'工作中','-1':'已离职'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.status > 0){
                                    var that = $.extend({}, this);
                                    var table = $(that.table).clone(true);
                                    $(table).data("operate-del", null);
                                    that.table = table;
                                    return Table.api.formatter.operate.call(that, value, row, index);
                                    //return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待入职</font>';
                    }else if(value==1){
                        return '<font color="blue">已入职，待签合同</font>';
                    }else if(value==2){
                        return '<font color="green">已签合同</font>';
                    }else if(value==3){
                        return '<font color="red">工作中</font>';
                    }else if(value==-1){
                        return '<font color="#999">已离职</font>';
                    }
                },
            }
        }
    };
    return Controller;
});