define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'agent/index' + location.search,
                    add_url: 'agent/add',
                    edit_url: 'agent/edit',
                    del_url: 'agent/del',
                    multi_url: 'agent/multi',
                    import_url: 'agent/import',
                    table: 'agent',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user.username', title: __('User_id'),operate:false},
                        {field: 'idcard', title: __('Idcard'), operate: 'LIKE'},
                        /*{field: 'city_name', title: __('City_id'), operate:false},*/
                        {field: 'commission', title: __('提成/小时'),operate:'BETWEEN'},
                        {field: 'service_name', title: __('Service_id'), operate:false},
                        {field: 'labor_name', title: __('Labor_id'), operate:false},
                        {field: 'status', title: __('Status'),searchList: {"0":'已解雇',"1":'正常'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'yeji',
                                text: __('查看业绩'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'daka/index?agent_user_id={row.user_id}',
                                /*extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }*/
                            },
                            {
                                name: 'user',
                                text: __('查看挂钩人员'),
                                icon: 'fa fa-eye',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'active_bmlist/index?agent_user_id={row.user_id}'
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==1) {
                        return '<font color="green">正常</font>';
                    }else{
                        return '<font color="#999999">已解雇</font>';
                    }
                },
            }
        }
    };
    return Controller;
});