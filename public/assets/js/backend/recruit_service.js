define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'recruit_service/index' + location.search,
                    add_url: 'recruit_service/add',
                    edit_url: 'recruit_service/edit',
                    del_url: 'recruit_service/del',
                    multi_url: 'recruit_service/multi',
                    import_url: 'recruit_service/import',
                    table: 'recruit_service',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {
                            field: 'city_id', 
                            title: __('City_id'),
                            visible: false,
                            addclass: 'selectpage',
                            extend: 'data-source="recruit/opencity/index" data-field="city" ',
                            formatter: Table.api.formatter.search
                        },
                        {field: 'city_name', title: __('City_id'), operate:false},
                        {field: 'link_name', title: __('联系人'), operate: 'LIKE'},
                        {field: 'link_phone', title: __('联系电话'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'),searchList: {"0":'已关闭',"1":'正常'},formatter:Table.api.formatter.toggle,sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==1) {
                        return '<font color="green">正常</font>';
                    }else{
                        return '<font color="#999999">已关闭</font>';
                    }
                },
            }
        }
    };
    return Controller;
});