define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'invite_list/index' + location.search,
                    //add_url: 'invite_list/add',
                    //edit_url: 'invite_list/edit',
                    del_url: 'invite_list/del',
                    multi_url: 'invite_list/multi',
                    import_url: 'invite_list/import',
                    table: 'invite_list',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'invite_id', title: __('Invite_id'),visible:false,addClass: "selectpage", extend: 'data-source="user/user/index" data-field="username" '},
                        //{field: 'user_id', title: __('User_id')},
                        {field: 'user_name', title: __('Invite_id'), operate: false},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'phone', title: __('Phone'), operate: 'LIKE'},
                        {field: 'gold', title: __('Gold'),searchList: {"3000-4000":'3000-4000',"4000-5000":'4000-5000',"5000-8000":'5000-8000',"8000-10000":'8000-10000'}},
                        {field: 'job', title: __('Job'),searchList: {"普工":'普工',"电工":'电工',"叉车":'叉车',"焊工":'焊工',"文员":'文员',"仓管":'仓管',"其他":'其他'}},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});