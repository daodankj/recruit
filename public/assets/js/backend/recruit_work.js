define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'recruit_work/index' + location.search,
                    add_url: 'recruit_work/add',
                    edit_url: 'recruit_work/edit',
                    del_url: 'recruit_work/del',
                    multi_url: 'recruit_work/multi',
                    import_url: 'recruit_work/import',
                    table: 'recruit_work',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'service_id', title: __('Service_id'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_service/index" data-field="name" '},
                        {field: 'labor_id', title: __('Labor_id'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_labor/index" data-field="name" '},
                        {field: 'company_id', title: __('Company_id'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit/company/index" data-field="name" '},
                        {field: 'service_name', title: __('Service_id'), operate:false},
                        {field: 'labor_name', title: __('Labor_id'), operate:false},
                        {field: 'company_name', title: __('Company_id'), operate:false},
                        {field: 'start_time', title: __('Start_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'end_time', title: __('End_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'price', title: __('白班工价'), operate:'BETWEEN'},
                        {field: 'price2', title: __('夜班工价'), operate:'BETWEEN'},
                        {field: 'address', title: __('Address'), operate: 'LIKE'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                         buttons:[
                            {
                                name: 'select',
                                text: __('添加人员'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'recruit_work/select?labor_id={row.labor_id}',
                            },
                            {
                                name: 'work_list',
                                text: __('查看人员'),
                                icon: 'fa fa-eye',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'recruit_work/work_list?work_id={row.id}',
                            }
                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        select: function (){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'recruit_work/select' + location.search,
                    table: 'active_bmlist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //启用跨页选择
                maintainSelected: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        //{field: 'active_id', title: __('招募活动'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_active/index" data-field="name" '},
                        //{field: 'active.name', title: __('招募活动'),operate:false},
                        {field: 'labor_name', title: __('劳务机构'), operate:false},
                        {field: 'user_id', title: __('用户'),visible: false,addclass: 'selectpage',extend: 'data-source="user/user/index" data-field="username" '},
                        {field: 'name', title: __('用户名称'), operate: 'LIKE'},
                        {field: 'phone', title: __('电话'), operate: 'LIKE'},                      
                        {field: 'agent_user_id', title: __('所属推广员'),visible:false,addclass: 'selectpage',extend: 'data-source="agent/index" data-field="username" data-primary-key="user_id"'},
                        {field: 'agent_name', title: __('所属推广员'),operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                         buttons:[
                            {
                                name: 'sure_select',
                                text: __('选择'),
                                icon: 'fa fa-check',
                                classname: 'btn btn-xs btn-danger btn-magic btn-ajax',
                                url: 'recruit_work/sure_select/work_id/{$row.work_id}',
                                confirm: '确认选择？',
                                refresh:true
                            }
                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            // 批量选择按钮事件
            $(".btn-select").on('click',function(){
                var that = this;
                var ids = Table.api.selectedids(table);
                var url = $(that).data("url") ? $(that).data("url") : $(that).attr("href");
                var work_id = $(that).data("workid") ? $(that).data("workid") : 0;
                Layer.confirm(
                    '是否确认选择所选的'+ids.length+'项',
                    {icon: 3, title: __('Warning'), offset: 0, shadeClose: true, btn: [__('OK'), __('Cancel')]},
                    function (index) {
                        Fast.api.ajax({
                            url: url,
                            data: {ids: ids,work_id:work_id}
                        }, function () {
                            table.trigger("uncheckbox");
                            table.bootstrapTable('refresh');
                        });
                        Layer.close(index);
                    }
                );
            });
        },
        work_list: function (){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'recruit_work/work_list' + location.search,
                    table: 'active_bmlist',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //启用跨页选择
                maintainSelected: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('用户'),visible: false,addclass: 'selectpage',extend: 'data-source="user/user/index" data-field="username" '},
                        {field: 'name', title: __('用户名称'), operate: 'LIKE'},
                        {field: 'phone', title: __('电话'), operate: 'LIKE'},                      
                        {field: 'agent_user_id', title: __('所属推广员'),visible:false,addclass: 'selectpage',extend: 'data-source="agent/index" data-field="username" data-primary-key="user_id"'},
                        {field: 'agent_name', title: __('所属推广员'),operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                         buttons:[
                            {
                                name: 'unbind',
                                text: __('解绑'),
                                icon: 'fa fa-del',
                                classname: 'btn btn-xs btn-danger btn-magic btn-ajax',
                                url: 'recruit_work/unbind',
                                confirm: '确认解绑？',
                                refresh:true
                            }
                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            // 批量解绑按钮事件
            $(".btn-select").on('click',function(){
                var that = this;
                var ids = Table.api.selectedids(table);
                var url = $(that).data("url") ? $(that).data("url") : $(that).attr("href");
                Layer.confirm(
                    '是否确认选择所选的'+ids.length+'项',
                    {icon: 3, title: __('Warning'), offset: 0, shadeClose: true, btn: [__('OK'), __('Cancel')]},
                    function (index) {
                        Fast.api.ajax({
                            url: url,
                            data: {ids: ids}
                        }, function () {
                            table.trigger("uncheckbox");
                            table.bootstrapTable('refresh');
                        });
                        Layer.close(index);
                    }
                );
            });
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'recruit_work/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), align: 'left'},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'recruit_work/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'recruit_work/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});