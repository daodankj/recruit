define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'contract/index' + location.search,
                    //add_url: 'contract/add',
                    //edit_url: 'contract/edit',
                    del_url: 'contract/del',
                    multi_url: 'contract/multi',
                    import_url: 'contract/import',
                    table: 'contract',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        //{field: 'flow_id', title: __('Flow_id'), operate: 'LIKE'},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'mobile', title: __('Mobile'), operate: 'LIKE'},
                        {field: 'company_name', title: __('公司名称'), operate: 'LIKE'},
                        {field: 'address', title: __('工作地点'), operate: 'LIKE'},
                        {field: 'start_date', title: __('开始日期'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'end_date', title: __('结束日期'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'price', title: __('Price'), operate:'BETWEEN'},
                        {field: 'price2', title: __('Price2'), operate:'BETWEEN'},
                        {field: 'status', title: __('Status'),searchList: {"1":'待签署',"2":'部分签署',"3":'已拒签',"4":'已签署',"5":'已过期',"6":'已撤销'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, 
                        buttons:[
                            {
                                name: 'show_contract',
                                text: __('查看合同'),
                                icon: 'fa fa-eye',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'contract/show_contract',
                                extend:'data-area=["400px","450px"]',
                            }
                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==1) {
                        return '<font color="black">待签署</font>';
                    }else if(value==2){
                        return '<font color="blue">部分签署</font>';
                    }else if(value==3){
                        return '<font color="red">已拒签</font>';
                    }else if(value==4){
                        return '<font color="green">已签署</font>';
                    }else if(value==5){
                        return '<font color="#999999">已过期</font>';
                    }else if(value==6){
                        return '<font color="#999999">已撤销</font>';
                    }
                },
            }
        }
    };
    return Controller;
});