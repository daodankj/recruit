define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance_wagesfail/index' + location.search,
                    // add_url: 'finance_wagesfail/add',
                    // edit_url: 'finance_wagesfail/edit',
                    // del_url: 'finance_wagesfail/del',
                    // multi_url: 'finance_wagesfail/multi',
                    // import_url: 'finance_wagesfail/import',
                    table: 'finance_wagesfail',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'city_id', title: __('City_id')},
                        {field: 'service_id', title: __('Service_id')},*/
                        {field: 'labor_id', title: __('Labor_id'),visible:false,addclass: 'selectpage',extend: 'data-source="recruit_labor/index" data-field="name" '},
                        {field: 'labor_name', title: __('Labor_id'),operate:false},
                        {field: 'day', title: __('Day'), operate: 'LIKE'},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'idcard', title: __('Idcard')},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});