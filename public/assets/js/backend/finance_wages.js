define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance_wages/index' + location.search,
                    add_url: 'finance_wages/add',
                    edit_url: 'finance_wages/edit',
                    del_url: 'finance_wages/del',
                    multi_url: 'finance_wages/multi',
                    import_url: 'finance_wages/import',
                    table: 'finance_wages',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'year', title: __('Year'),searchList: {"2021":'2021年',"2022":'2022年',"2023":'2023年'}},
                        {field: 'month', title: __('Month'),searchList: {"1":'1月份',"2":'2月份',"3":'3月份',"4":'4月份',"5":'5月份',"6":'6月份',"7":'7月份',"8":'8月份',"9":'9月份',"10":'10月份',"11":'11月份',"12":'12月份'}},
                        {field: 'user_id', title: __('User_id'),visible:false,addclass: 'selectpage',extend: 'data-source="user/user/index" data-field="username" '},
                        {field: 'user_name', title: __('User_id'),operate:false},
                        /*{field: 'city_id', title: __('City_id')},
                        {field: 'service_id', title: __('Service_id')},*/
                        {field: 'labor_id', title: __('Labor_id'),visible:false,addclass: 'selectpage',extend: 'data-source="recruit_labor/index" data-field="name" '},
                        {field: 'labor_name', title: __('Labor_id'),operate:false},
                        {field: 'total_time', title: __('Total_time'), operate:'BETWEEN'},
                        {field: 'total_money', title: __('Total_money'), operate:'BETWEEN'},
                        {field: 'yf_money', title: __('应发工资'), operate:'BETWEEN'},
                        {field: 'send_money', title: __('Send_money'), operate:'BETWEEN'},
                        {field: 'status', title: __('Status'),searchList: {"0":'待确认',"1":'已确认',"2":'已反馈',"3":'已转账'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待确认</font>';
                    }else if(value==1){
                        return '<font color="green">已确认</font>';
                    }else if(value==2){
                        return '<font color="red">已反馈</font>';
                    }else if(value==3){
                        return '<font color="blue">已转账</font>';
                    }
                },
            }
        }
    };
    return Controller;
});