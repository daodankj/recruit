define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'daka/index' + location.search,
                    add_url: 'daka/add',
                    edit_url: 'daka/edit',
                    del_url: 'daka/del',
                    multi_url: 'daka/multi',
                    import_url: 'daka/import',
                    table: 'daka',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'date', title: __('Date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'type', title: __('班次'),searchList: {"1":'白班',"2":'夜班'},formatter: Controller.api.formatter.typeName,sortable: true},
                        {field: 'work_id', title: __('所属工作任务'),visible:false,addclass: 'selectpage',extend: 'data-source="recruit_work/index" data-field="name" '},
                        {field: 'work.name', title: __('所属工作任务'),operate:false},
                        /*{field: 'bm_id', title: __('Bm_id')},*/
                        {field: 'user_id', title: __('User_id'),visible: false,addclass: 'selectpage',extend: 'data-source="user/user/index" data-field="username" '}, 
                        {field: 'user.username', title: __('User_id'),operate:false},
                        {field: 'in_time', title: __('In_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'out_time', title: __('Out_time'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'image', title: __('Image'), operate: false, events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'remark', title: __('Remark'), operate: 'LIKE'},
                        {field: 'work_time', title: __('工作时长/h'), operate:'BETWEEN'},
                        {field: 'price', title: __('Price'), operate:'BETWEEN'},
                        {field: 'agent_user_id', title: __('Agent_user_id'),visible:false,addclass: 'selectpage',extend: 'data-source="agent/index" data-field="username" data-primary-key="user_id"'},
                        {field: 'agent_name', title: __('Agent_user_id'),operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                typeName:function(value, row, index){
                    if (value==1) {
                        return '<font color="green">白班</font>';
                    }else{
                        return '<font color="red">夜班</font>';
                    }
                },
            }
        }
    };
    return Controller;
});