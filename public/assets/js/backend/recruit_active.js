define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'recruit_active/index' + location.search,
                    add_url: 'recruit_active/add',
                    edit_url: 'recruit_active/edit',
                    del_url: 'recruit_active/del',
                    multi_url: 'recruit_active/multi',
                    import_url: 'recruit_active/import',
                    table: 'recruit_active',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //启用固定列
                fixedColumns: true,
                //固定右侧列数
                fixedRightNumber: 2,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'labor_id', title: __('劳务机构'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_labor/index" data-field="name" '},
                        {field: 'labor_name', title: __('劳务机构'), operate:false},
                        {field: 'user_num', title: __('User_num')},
                        {field: 'bm_num', title: __('报名人数'),operate:false},
                        {field: 'type', title: __('Type'),searchList: {"1":'拼团',"2":'限时'},formatter: Controller.api.formatter.type},                      
                        {field: 'settlement_type', title: __('Settlement_type'),searchList: {"1":'月结',"2":'周结',"3":"日结"},formatter: Controller.api.formatter.settlement},
                        {field: 'head_open', title: __('Head_open'),searchList: {"0":'关闭',"1":'开启'},formatter: Controller.api.formatter.headopen},
                        {field: 'price', title: __('工资/小时'),operate:'BETWEEN'},
                        {field: 'start_date', title: __('开始日期'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.date},
                        {field: 'end_date', title: __('结束日期'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.date},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'check_admin_name', title: __('审核人'), operate:'like'},
                        {field: 'check_time', title: __('审批时间'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime}, 
                        
                        {field: 'status', title: __('Status'),operate:false,searchList: {"0":'待审核',"1":'已通过',"2":'已拒绝'},formatter: Controller.api.formatter.status,sortable: true},              
                        {field: 'operate', title: __('Operate'), align:'right',table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('审核'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'recruit_active/check',
                                extend:'data-area=["500px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            },
                            {
                                name: 'head',
                                text: __('参团申请'),
                                icon: 'fa fa-flag-checkered',
                                classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                url: 'headjoin_apply/index?active_id={row.id}',
                            },
                            {
                                name: 'user',
                                text: __('报名人员'),
                                icon: 'fa fa-user',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'active_bmlist/worklist?active_id={row.id}'
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                //var value = $(this).attr("href").replace('#','');
                var field = $(this).closest("ul").data("field");
                var value = $(this).data("value");
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                var queryParams = options.queryParams;
                options.queryParams = function (params) {
                    var params = queryParams(params);
                    var filter = params.filter ? JSON.parse(params.filter) : {};
                    var op = params.op ? JSON.parse(params.op) : {};
                    if (value=='all') {
                        delete filter[field];
                        delete op[field];
                    }else{
                        filter[field] = value;
                        op[field] = '=';
                    }  
                    params.filter = JSON.stringify(filter);   
                    params.op = JSON.stringify(op);           
                    return params;
                };

                table.bootstrapTable('refresh', {});
                return false;
            });
        },
        recyclebin: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    'dragsort_url': ''
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: 'recruit_active/recyclebin' + location.search,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name'), align: 'left'},
                        {
                            field: 'deletetime',
                            title: __('Deletetime'),
                            operate: 'RANGE',
                            addclass: 'datetimerange',
                            formatter: Table.api.formatter.datetime
                        },
                        {
                            field: 'operate',
                            width: '130px',
                            title: __('Operate'),
                            table: table,
                            events: Table.api.events.operate,
                            buttons: [
                                {
                                    name: 'Restore',
                                    text: __('Restore'),
                                    classname: 'btn btn-xs btn-info btn-ajax btn-restoreit',
                                    icon: 'fa fa-rotate-left',
                                    url: 'recruit_active/restore',
                                    refresh: true
                                },
                                {
                                    name: 'Destroy',
                                    text: __('Destroy'),
                                    classname: 'btn btn-xs btn-danger btn-ajax btn-destroyit',
                                    icon: 'fa fa-times',
                                    url: 'recruit_active/destroy',
                                    refresh: true
                                }
                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==1?'确认通过吗？':'确认拒绝审核吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });       
            })
        }, 
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审核</font><br/>('+row.active_status+')';
                    }else if(value==1){
                        return '<font color="green">已通过</font><br/>('+row.active_status+')';
                    }else{
                        return '<font color="red">已拒绝</font><br/>('+row.active_status+')';
                    }
                },
                type:function(value, row, index){
                    if (value==1) {
                        return '<font color="#FF9800">拼团</font>';
                    }else{
                        return '<font color="red">限时</font>';
                    }
                },
                settlement:function(value, row, index){
                    if (value==1) {
                        return '<font color="#FF9800">月结</font>';
                    }else if(value==2){
                        return '<font color="green">周结</font>';
                    }else{
                        return '<font color="blue">日结</font>';
                    }
                },
                headopen:function(value, row, index){
                    if (value==1) {
                        return '<font color="#FF9800">开启</font>';
                    }else{
                        return '<font color="red">关闭</font>';
                    }
                },
            }
        }
    };
    return Controller;
});