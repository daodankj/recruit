define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'advance/index' + location.search,
                    add_url: 'advance/add',
                    edit_url: 'advance/edit',
                    del_url: 'advance/del',
                    multi_url: 'advance/multi',
                    import_url: 'advance/import',
                    table: 'advance',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //启用固定列
                fixedColumns: true,
                //固定右侧列数
                fixedRightNumber: 2,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id'),visible: false,addclass: 'selectpage',extend: 'data-source="user/user/index" data-field="username" '},
                        {field: 'username', title: __('Username'), operate: 'LIKE'},
                        {field: 'phone', title: __('电话'), operate: 'LIKE'},
                        {field: 'service_id', title: __('Service_id'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_service/index" data-field="name" '},
                        {field: 'labor_id', title: __('Labor_id'),visible: false,addclass: 'selectpage',extend: 'data-source="recruit_labor/index" data-field="name" '},
                        {field: 'service_name', title: __('Service_id'), operate:false},
                        {field: 'labor_name', title: __('Labor_id'), operate:false},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'agent_name', title: __('推广员'), operate:false},
                        {field: 'check_admin_name', title: __('审核人'), operate:'like'},
                        {field: 'check_time', title: __('审批时间'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime}, 
                        {field: 'result', title: __('审批内容'), operate:false},
                        {field: 'status', title: __('Status'),operate:false,searchList: {"0":'待审核',"1":'已通过',"2":'已拒绝'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('复审'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'advance/check',
                                extend:'data-area=["500px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 1){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            },
                            {
                                name: 'showdaka',
                                text: __('查看考勤'),
                                icon: 'fa fa-eye',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'daka/index?user_id={row.user_id}'
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                //var value = $(this).attr("href").replace('#','');
                var field = $(this).closest("ul").data("field");
                var value = $(this).data("value");
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                var queryParams = options.queryParams;
                options.queryParams = function (params) {
                    var params = queryParams(params);
                    var filter = params.filter ? JSON.parse(params.filter) : {};
                    var op = params.op ? JSON.parse(params.op) : {};
                    if (value=='all') {
                        delete filter[field];
                        delete op[field];
                    }else{
                        filter[field] = value;
                        op[field] = '=';
                    }  
                    params.filter = JSON.stringify(filter);   
                    params.op = JSON.stringify(op);           
                    return params;
                };

                table.bootstrapTable('refresh', {});
                return false;
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==3?'确认通过吗？':'确认拒绝审核吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });       
            })
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待初审</font>';
                    }else if(value==1){
                        return '<font color="blue">待复审</font>';
                    }else if(value==3){
                        return '<font color="green">复审通过</font>';
                    }else{
                        return '<font color="red">已拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});