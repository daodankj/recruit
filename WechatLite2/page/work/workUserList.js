const util = require('../../utils/util');

var app = getApp();

Page({
  data: {
    inputValue:'',
    work_id:0,
    inputValue: '',
    comjob:[],
    checkList:[],
    loading: false,
    nodata: false,
    nomore: false,
  },
  page: 1,
  page_block: 10,

  onLoad: function (options) {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    if (options.work_id != undefined) {
      this.setData({
        work_id: options.work_id
      })
    }
    this.loadJobs(function () {
      wx.stopPullDownRefresh();
    });
  },
 
  onShow: function () {
   
  },
  loadJobs: function (cb) {
    let that = this;
    if (that.data.nomore == true || that.data.loading == true) {
      return;
    }
    if (!util.isEmptyObject(app.globalData.locData)) {
      this.setData({ loading: true });
      app.request('/agent/work_user_list', {
        page: that.page,
        page_block: that.page_block,
        Re_input: that.data.inputValue,
        work_id: that.data.work_id
      }, function (data, ret) {
        that.setData({
          loading: false,
          nodata: that.page == 1 && data.length == 0 ? true : false,
          nomore: (that.page > 1 && data.length == 0) ||
            (that.page == 1 && data.length < that.page_block && data.length > 0) ? true : false,
          comjob: that.page > 1 ? that.data.comjob.concat(data) : data
        })
        that.page++;
        typeof cb == 'function' && cb(data);
      }, function (data, ret) {
        app.error(ret.msg);
      });
    }
    //setTimeout(function () { that.loadJobs(cb); }, 500);
  },
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.loadJobs(function () {
      wx.stopPullDownRefresh();
    });
  },
  onReachBottom: function () {
    var that = this;
    this.loadJobs(function (data) {
      if (data.length == 0) {
        app.info("暂无更多数据");
      }
    });
  },
  onShareAppMessage: function () {
  
  },
  checkboxChange(e) {
    console.log('checkbox发生change事件，携带value值为：', e.detail.value)

    const items = this.data.comjob
    const values = e.detail.value
    const list = []
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false

      for (let j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (items[i].id == values[j]) {
          items[i].checked = true
          list.push(items[i])
          break
        }
      }
    }

    this.setData({
      comjob:items,
      checkList:list
    })
  },
  godb(){
    if(this.data.checkList.length<1){
      app.info("请至少选择1个员工");
      return false;
    }
    var that = this;
    console.log(this.data.checkList)
    var bm_ids = [];
    this.data.checkList.forEach(info => {
        bm_ids.push(info.id);
    });
    app.request('/agent/unbind', {
        bm_ids: bm_ids,
      }, function (data, ret) {
        app.success(ret.msg)
        that.setData({
            checkList:[]
        })
        that.onPullDownRefresh()
      }, function (data, ret) {
        app.error(ret.msg);
      });
  },

  searchChange(e) {
    this.setData({
      inputValue: e.detail.value
    });
  },

  searchDone(e) {
    //console.error('search', e.detail.value)
    this.onPullDownRefresh();
  },

  handleCancel() {
    //console.error('cancel')
    this.onPullDownRefresh();
  },
})