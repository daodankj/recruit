// page/location/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:{},
    longitude: 113.324520,
    latitude: 23.099994,
    image:'',
    markers:[{
      id: 0,
      iconPath: "../../assets/images/tabbar/location-s.png",
      latitude: 23.099994,
      longitude: 113.324520,
      width: 50,
      height: 50
    }],
    showRemark:false,
    remark:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (!app.globalData.userInfo){
      wx.redirectTo({url:'/page/my/login?bind=1'});
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    /*wx.getLocation({
      type: 'gcj02', //返回可以用于wx.openLocation的经纬度
      success (res) {
        const latitude = res.latitude
        const longitude = res.longitude
        wx.openLocation({
          latitude,
          longitude,
          scale: 18
        })
      }
     })*/
     var that = this;
     wx.getLocation({
       type: "gcj02",//wgs84
       success: function(res){
         that.setData({
          latitude: res.latitude,
          longitude: res.longitude,
          markers:[{
            latitude: res.latitude,
            longitude: res.longitude
          }]
         });
         that.getInfo();
       }
     })
  },

  //获取个人信息
  getInfo(){
    var that = this;
    app.request('/clock/index', {
      lat: that.data.latitude,
      lng: that.data.longitude,
    }, function (data, ret) {
       that.setData({
         info:data,
         showRemark:false
       })
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  //签到签退
  daka (e) {
    var that = this;
    var type = e.currentTarget.dataset.type;
    var msg = type==1?"确认签到吗？":"确认签退吗？";
    wx.showModal({
      title: '提示',
      content: msg,
      success: function (res) {
        if (res.confirm) {         
          app.request('/clock/daka', {
            type: type,
          }, function (data, ret) {
            app.success(ret.msg);
            that.getInfo();
          }, function (data, ret) {
            app.error(ret.msg);
          });
        } else if (res.cancel) {
          
        }
      }
    })
  },
  //上传照片
  uploadImage: function () {
    var that = this;
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],//original
      sourceType:['camera'],//album
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              // app.success('上传成功');
              // that.setData({ 
              //   image: data.data.fullurl ,
              // });
              app.request('/clock/daka', {
                type: 1,
                remark:that.data.remark,
                image:data.data.fullurl
              }, function (data, ret) {
                app.success(ret.msg);
                that.getInfo();
              }, function (data, ret) {
                app.error(ret.msg);
              });
            }
          }
        });
      }
    });
  },

  addRemark(){
    this.setData({ 
      showRemark: true
    });
  },
  formSubmit: function (event) {
    //console.log(event.detail.value);
    // if (event.detail.value.remark == '') {
    //   app.error('请输入打卡备注');
    //   return;
    // }
    this.setData({ 
      remark: event.detail.value.remark
    });
    this.uploadImage();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})