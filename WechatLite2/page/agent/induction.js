// page/agent/induction.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[],
    noNum:0,
    yesNum:0,
    loading: false,
    nodata: false,
    nomore: false,
    showadd:false,
    birthday:'请选择出生日期',
    userInfo: null,
    idcard_img_front:'',
    idcard_img_backend:'',
    add_img_front:'/assets/img/card-z.png',
    add_img_backend:'/assets/img/card-f.png',
  },
  page: 1,
  page_block: 10,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
    this.setData({ userInfo: app.globalData.userInfo });
  },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  getList:function(cb){
    let that = this;
    if (that.data.nomore == true || that.data.loading == true) {
      return;
    }
    this.setData({ loading: true });
    app.request('/agent/bm_list', {
      page: that.page,
      page_block: that.page_block,
    }, function (data, ret) {
      //console.log(data);
      that.setData({
        loading: false,
        noNum: data.noNum,
        yesNum: data.yesNum,
        nodata: that.page == 1 && data.list.length == 0 ? true : false,
        nomore: (that.page > 1 && data.list.length == 0) ||
          (that.page == 1 && data.list.length < that.page_block && data.list.length > 0) ? true : false,
        list: that.page > 1 ? that.data.list.concat(data.list) : data.list
      })
      that.page++;
      typeof cb == 'function' && cb(data);
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  //确认入职
  sureInduction: function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确认入职吗?',
      success: function (res) {
        if (res.confirm) {         
          app.request('/agent/sure_induction', {
            id: id,
          }, function (data, ret) {
            app.success('操作成功');
            that.onPullDownRefresh();
          }, function (data, ret) {
            app.error(ret.msg);
          });
        } else if (res.cancel) {
          
        }
      }
    })
  },

  //确认签合同
  sureContract:function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '/page/agent/createContract?bm_id='+id
    })
    return;
    wx.showModal({
      title: '提示',
      content: '确认签合同吗?',
      success: function (res) {
        if (res.confirm) {         
          app.request('/agent/sure_contract', {
            id: id,
          }, function (data, ret) {
            app.success('操作成功');
            that.onPullDownRefresh();
          }, function (data, ret) {
            app.error(ret.msg);
          });
        } else if (res.cancel) {
          
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this;
    this.getList(function (data) {
      if (data.list.length == 0) {
        app.info("暂无更多数据");
      }
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  addworker: function(){
    this.setData({
      showadd: true
    })
  },
  formSubmit: function (event) {
    console.log(event.detail.value);
    var that = this;
    if (event.detail.value.name == '') {
      app.error('请输入姓名');
      return;
    }
    if (event.detail.value.phone == '') {
      app.error('请输入电话');
      return;
    }
    if (event.detail.value.idcard == '') {
      app.error('请输入身份证号码');
      return;
    }
    if (event.detail.value.birthday == '请选择出生日期') {
      app.error('请选择出生日期');
      return;
    }
    if (event.detail.value.idcard_img_front == '') {
      app.error('请上传身份证正面照片');
      return;
    }
    if (event.detail.value.idcard_img_backend == '') {
      app.error('请上传身份证反面照片');
      return;
    }
    app.request('/agent/add_worker', event.detail.value, function (data) {
      app.success('操作成功!', function () {
        that.setData({
          showadd: false
        });
        event.detail.value = null;
        that.onPullDownRefresh();
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  bindDateChange: function (e) {
    //console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      birthday: e.detail.value
    })
  },
  //上传正面
  uploadFront: function () {
    var that = this;
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              app.success('上传成功');
              that.setData({ 
                idcard_img_front: data.data.fullurl ,
                add_img_front:data.data.fullurl
              });
            }
          }
        });
      }
    });
  },
  //上传反面
  uploadBackend: function () {
    var that = this;
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              app.success('上传成功');
              that.setData({ 
                idcard_img_backend: data.data.fullurl ,
                add_img_backend:data.data.fullurl
              });
            }
          }
        });
      }
    });
  },
})