
var app = getApp();
Page({
    /**
     * 页面的初始数据
     */
    data: {
        list:[],
        loading: false,
        nodata: false,
        nomore: false,
        showApply:false,
        applyId:''
    },
    page: 1,
    page_block: 10,
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.page = 1;
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
        this.getList();
    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    check: function(e){
        let applyId = e.currentTarget.dataset.id;
        this.setData({
            showApply: true,
            applyId: applyId
        })
    },
    getList:function(cb){
        let that = this;
        if (that.data.nomore == true || that.data.loading == true) {
          return;
        }
        this.setData({ loading: true });
        app.request('/agent/get_advance', {
          page: that.page,
          page_block: that.page_block,
        }, function (data, ret) {
          that.setData({
            loading: false,
            nodata: that.page == 1 && data.list.length == 0 ? true : false,
            nomore: (that.page > 1 && data.list.length == 0) ||
              (that.page == 1 && data.list.length < that.page_block && data.list.length > 0) ? true : false,
            list: that.page > 1 ? that.data.list.concat(data.list) : data.list
          })
          that.page++;
          typeof cb == 'function' && cb(data);
        }, function (data, ret) {
          app.error(ret.msg);
        });
    },
    formSubmit: function (event) {
        if(!this.data.applyId){
           app.error('请选择一条记录');
           return;
        }
        //console.log(event.detail.value);
        var that = this;
        // if (event.detail.value.remark == '') {
        //   app.error('请输入审核意见');
        //   return;
        // }
        event.detail.value.id = this.data.applyId;
        app.request('/agent/advance_check', event.detail.value, function (data) {
          app.success('操作成功!', function () {
            that.setData({
                showApply: false,
                applyId:''
            });
            that.onPullDownRefresh();
          });
        }, function (data, ret) {
          app.error(ret.msg);
        });
      },  
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
        this.page = 1;
        this.setData({ nodata: false, nomore: false });
        this.getList(function () {
            wx.stopPullDownRefresh();
        });
    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.getList(function (data) {
        if (data.list.length == 0) {
            app.info("暂无更多数据");
        }
        });
    },

})