// page/agent/createContract.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        bmId:0,
        start_date:'2022-12-01',
        end_date:'2022-12-01',
        contractInfo : null
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.setData({
            bmId: options.bm_id
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        this.getContract()
    },
    getContract: function(){
        var that = this;
        app.request('/agent/getContract', {bm_id:this.data.bmId}, function (data) {
            that.setData({
                contractInfo:data
            })
          }, function (data, ret) {
            app.error(ret.msg);
          });
    },

    contractInfo:function(){
        var that = this;
        wx.navigateToMiniProgram({  
            appId:'wxa023b292fd19d41d', // 电子签小程序的appId  
            path:'pages/guide?from=SFY&to=CONTRACT_DETAIL&id='+that.data.contractInfo.flow_id+'&name='+that.data.contractInfo.name+'&phone='+that.data.contractInfo.mobile, //${flowId}为流程 id，name、phone 按需给  
            envVersion:'release',
            success(res){
                // 打开成功 
            } 
        })
    },

    cancelFlow:function(){
        var that = this;
        wx.showModal({
            title: '提示',
            content: '确认撤销合同吗?',
            success: function (res) {
              if (res.confirm) {         
                app.request('/agent/cancelFlow', {flow_id:that.data.contractInfo.flow_id}, function (data) {
                    app.success('撤销成功!', function () {
                      setTimeout(function () {
                        //要延时执行的代码
                        wx.navigateBack({delta:1});
                      }, 2000); //延迟时间
                    });
                  }, function (data, ret) {
                    app.error(ret.msg);
                  });
              } else if (res.cancel) {
                
              }
            }
        })
    },

    bindDateChange: function (e) {
        //console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
          start_date: e.detail.value
        })
    },
    bindDateChange2: function (e) {
        //console.log('picker发送选择改变，携带值为', e.detail.value)
        this.setData({
          end_date: e.detail.value
        })
    },
    formSubmit: function (event) {
        console.log(event.detail.value);
        if(this.data.bmId<1){
           app.error('参数错误');
           return;
        }
        var that = this;
        if (event.detail.value.company_name == '') {
          app.error('公司名称不能为空');
          return;
        }
        if (event.detail.value.start_date == event.detail.value.end_date) {
          app.error('开始日期和结束日期不能相同');
          return;
        }
        if (event.detail.value.address == '') {
            app.error('工作地点不能为空');
            return;
          }
        if (event.detail.value.price <1) {
          app.error('白班工时价必须大于0');
          return;
        }
        if (event.detail.value.price2 < 1) {
          app.error('晚班工时价必须大于0');
          return;
        }
        event.detail.value.bm_id = this.data.bmId;
        app.request('/agent/createContract', event.detail.value, function (data) {
          app.success('创建成功!', function () {
            setTimeout(function () {
              //要延时执行的代码
              wx.navigateBack({delta:1});
            }, 2000); //延迟时间
          });
        }, function (data, ret) {
          app.error(ret.msg);
        });
      },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})