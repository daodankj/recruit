// page/train/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    inputValue:'',
    list:[],
    loading: false,
    nodata: false,
    nomore: false,
  },
  page: 1,
  page_block: 10,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  getList:function(cb){
    let that = this;
    if (that.data.nomore == true || that.data.loading == true) {
      return;
    }
    this.setData({ loading: true });
    app.request('/user/score_top', {
      page: that.page,
      page_block: that.page_block,
      Re_input: that.data.inputValue,
    }, function (data, ret) {
      //console.log(data);
      that.setData({
        loading: false,
        nodata: that.page == 1 && data.list.length == 0 ? true : false,
        nomore: (that.page > 1 && data.list.length == 0) ||
          (that.page == 1 && data.list.length < that.page_block && data.list.length > 0) ? true : false,
        list: that.page > 1 ? that.data.list.concat(data.list) : data.list
      })
      that.page++;
      typeof cb == 'function' && cb(data);
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.page = 1;
    // this.setData({ nodata: false, nomore: false });
    // this.getList(function () {
    //   wx.stopPullDownRefresh();
    // });
  },
  searchChange(e) {
    this.setData({
      inputValue: e.detail.value
    });
  },

  searchDone(e) {
    //console.error('search', e.detail.value)
    this.onPullDownRefresh();
  },

  handleCancel() {
    //console.error('cancel')
    this.onPullDownRefresh();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.getList(function () {
      wx.stopPullDownRefresh();
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getList(function (data) {
      if (data.length == 0) {
        app.info("暂无更多数据");
      }
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})