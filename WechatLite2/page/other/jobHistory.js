const util = require('../../utils/util');

var app = getApp();

Page({
  data: {
    id:0,
    inputValue: '',
    comjob:[],
    checkList:[],
    loading: false,
    nodata: false,
    nomore: false,
  },
  page: 1,
  page_block: 10,

  onLoad: function (options) {
    this.page = 1;
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    if (options.id != undefined) {
      this.setData({
        id: options.id
      })
    }
    this.loadJobs(function () {
      wx.stopPullDownRefresh();
    });
  },
 
  onShow: function () {
   
  },
  loadJobs: function (cb) {
    let that = this;
    if (that.data.nomore == true || that.data.loading == true) {
      return;
    }
    if (!util.isEmptyObject(app.globalData.locData)) {
      this.setData({ loading: true });
      app.request('/Companyjob/get_jobhistory', {
        page: that.page,
        page_block: that.page_block,
        Re_input: that.data.inputValue,
        job_id:that.data.id
      }, function (data, ret) {
        console.log(data);
        that.setData({
          loading: false,
          nodata: that.page == 1 && data.length == 0 ? true : false,
          nomore: (that.page > 1 && data.length == 0) ||
            (that.page == 1 && data.length < that.page_block && data.length > 0) ? true : false,
          comjob: that.page > 1 ? that.data.comjob.concat(data) : data
        })
        that.page++;
        typeof cb == 'function' && cb(data);
      }, function (data, ret) {
        app.error(ret.msg);
      });
    }
    //setTimeout(function () { that.loadJobs(cb); }, 500);
  },
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false });
    this.loadJobs(function () {
      wx.stopPullDownRefresh();
    });
  },
  onReachBottom: function () {
    var that = this;
    this.loadJobs(function (data) {
      if (data.length == 0) {
        app.info("暂无更多数据");
      }
    });
  },
  onShareAppMessage: function () {
  
  },
  checkboxChange(e) {
    console.log('checkbox发生change事件，携带value值为：', e.detail.value)

    const items = this.data.comjob
    const values = e.detail.value
    const list = []
    for (let i = 0, lenI = items.length; i < lenI; ++i) {
      items[i].checked = false

      for (let j = 0, lenJ = values.length; j < lenJ; ++j) {
        if (items[i].job.Id == values[j]) {
          items[i].checked = true
          list.push(items[i])
          break
        }
      }
    }

    this.setData({
      comjob:items,
      checkList:list
    })
  },
  godb(){
    if(this.data.checkList.length<2){
      app.info("请至少选择2个职位进行对比");
      return false;
    }
    var that = this;
    console.log(this.data.checkList)
    wx.navigateTo({
      url: '/page/other/jobContrast',
      events: {
        // 为指定事件添加一个监听器，获取被打开页面传送到当前页面的数据
        acceptDataFromOpenedPage: function(data) {
          console.log(data)
        },
        someEvent: function(data) {
          console.log(data)
        }
      },
      success: function(res) {
        // 通过eventChannel向被打开页面传送数据
        res.eventChannel.emit('acceptDataFromOpenerPage', { data: that.data.checkList })
      }
    })
  }
})