const util = require('../../utils/util');
var app = getApp();
var context = null;
Page({

  data: {
    bannerList: [],
    companyList:[],
    companyList2:[],
    companyList3:[],
    JobsList:[],
    sxList:[],
    show: false,
    cancelWithMask: true,
    actions: [],
    chose_city:{name:'',index:0},
    cancelText: '关闭',
    activeList:[],
    currentTab:0,
    showhb: false,
    windowW:0,
    windowH:0,
    aername:'',
    score:0,
    hidem:1,
    xyinfo:'',
  },


  selcity() {
    this.setData({
      'show': true
    });
  },
  closeActionSheet() {
    this.setData({
      'show': false
    });
  },
  handleActionClick({ detail }) {
    var that = this;
    // 获取被点击的按钮 index
    const { index } = detail;
    console.log(index);
    if (index == this.data.actions.length-1){
    }else{
      let chose_city = { name: this.data.actions[index].name, index: index };
      //this.setData({
      //  chose_city: chose_city
      //});
      //区域名称
      let aername = chose_city.name.split("/")[2];
      that.setData({
        aername:aername
      })
      app.globalData.locData['ShowCity'] = that.data.actions[index].name
      app.globalData.locData['ShowCity_id'] = app.globalData.city[index].id
      that.get_index_all_data(chose_city);
      this.closeActionSheet();
    }
  },
  MakelocCity: function (cb) {
    var that = this;
    if (!util.isEmptyObject(app.globalData.locData)){
      let tmp_index = 0
      app.globalData.city.forEach(function (item, index, arr) {
        let onei = {
          name: item.city,
          subname: '',
          loading: false
        };
        that.data.actions.push(onei);

        //这里根据前面获得的数据 来抓取 当下在的城市
        if (app.globalData.locData['ShowCity'] == item.city) {
          tmp_index = index;
          //app.globalData.locData['ShowCity_id'] = item.id
          console.log(index);
        }
      });
      let onei = {
        name: '',
        subname: '更多城市，暂未开通，敬请期待',
        loading: false
      };
      that.data.actions.push(onei);
      /*
      if (tmp_index == 0){
        app.globalData.locData['ShowCity'] = that.data.actions[tmp_index].name
        app.globalData.locData['ShowCity_id'] = app.globalData.city[tmp_index].id
      }
      */
      let chose_city = { name: that.data.actions[tmp_index].name, index: tmp_index };
      //区域名称
      let aername = chose_city.name.split("/")[2];
      that.setData({
        aername:aername,
        score:app.globalData.userInfo.score
      })

      that.get_index_all_data(chose_city);
      // 海报背景图线上地址      
      var url = app.Domain_Public+'assets/img/share_bg1.png'            
      that.getBG(url).then(function (locationData) {
          that.setData({    
              bgpic: locationData  
          })  
      })    
      // 小程序二维码  
      //var urlqCord = app.Domain_Public+'uploads/bjqc_t.jpg'
      var urlqCord = app.apiUrl+'usewechat/index?uid='+app.globalData.userInfo.id  //测试加env_version=trial
      that.getBG(urlqCord).then(function (locationData) {      
          that.setData({      
              qCord: locationData      
          })       
      }) 
      return;
    }
    setTimeout(function () { that.MakelocCity(cb); }, 500);
  },
  onLoad: function (options) {
    var that = this;
    console.log(options)
    //这里间隔获得
    app.Log_after_fun(function () {
      //这里把app中的城市数据整理出来
      that.MakelocCity();
    });
    // 获取设备宽高，以备海报全屏显示      
    wx.getSystemInfo({      
        success: function (res) {     
            that.setData({      
                windowW: res.windowWidth,     
                windowH: res.windowHeight      
            })     
        },   
    });      
  },
  get_index_all_data: function (chose_city) {
    var that = this;
    app.request('/index/get_index_all_data', { city_id: app.globalData.locData['ShowCity_id'] }, function (data, ret) {
      console.log(data);

      data.bannerList.forEach(function (item) {
        item.url = "/page/zh_news/index?id=" + item.id;
      });
      let hidem = 1;//默认先隐藏
      if(data.xyinfo!=''){//未读
        hidem = 0;
      }
      that.setData({
        bannerList: data.bannerList,
        companyList: data.companyList,
        companyList2: data.companyList2,
        companyList3: data.companyList3,
        JobsList: data.JobsList,
        activeList:data.activeList,
        sxList:data.sxList,
        actions: that.data.actions,
        chose_city: chose_city,
        hidem: hidem,
        xyinfo: data.xyinfo
      });
      wx.stopPullDownRefresh();
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  onShow: function () {
    
  },

  onPullDownRefresh: function () {
    this.data.actions=[];
    this.onLoad();
    /*
    var that = this;
    //这里间隔获得
    app.Log_after_fun(function () {
      app.request('/index/get_index_all_data', {}, function (data, ret) {
        console.log(data);
        data.bannerList.forEach(function (item) {
          item.url = "/page/zh_news/index?id=" + item.id;
        });
        that.setData({
          bannerList: data.bannerList,
          companyList: data.companyList,
          JobsList: data.JobsList,
          actions: that.data.actions
        });

      }, function (data, ret) {
        app.error(ret.msg);
      });
    });
    */
  },

  onShareAppMessage: function () {
  
  },

  gotto_jobslist: function () {
    wx.switchTab({
      url: '/page/zh_Jobs/JobsList'
    })
  },
  // 区域
  tabNav: function (e) {
    console.log('出问题' + e.target.dataset.current);
    if (e.target.dataset.current == undefined){
      return;
    }
  
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      this.setData({
        currentTab: e.target.dataset.current,
      })
    }
  },
  Cnagetabfun: function (e) {
    let num = e.detail.current;
    //console.log('自家家的+'+num);
    this.setData({
      currentTab: num
    })
  },

  //跳转商城
  goshop(){
    if(app.globalData.userInfo.id){
      wx.navigateTo({
        url: '/page/location/webview?uid='+app.globalData.userInfo.id
      })
    }else{
      wx.navigateTo({
        url: '/page/location/webview'
      })
    }
  },
  //海报方法
  closeImg(){
      this.setData({       
        showhb:false        
      })
      //context.clearRect(0, 0, this.data.windowW, this.data.windowH);
  },  
  // 点击生成并保持海报到手机相册    
  clickMe() {      
      var that = this       
      that.setData({       
        showhb:true        
      })      
      that.drawCanvas()      
  },   
  // 绘制canvas  
  drawCanvas() {   
      var that = this     
      var windowW = that.data.windowW*0.9      
      var windowH = that.data.windowH*0.9     
      // 使用 wx.createContext 获取绘图上下文 context  
      context = wx.createCanvasContext('firstCanvas') 
      //context.setFillStyle('#FFC078')
      //context.fillRect(0, 0, windowW, windowH)
      // 海报背景图
      //context.drawImage(that.data.bgpic, (windowW - 280) / 2, (windowH -450) / 2, 280, 450)   
      context.drawImage(that.data.bgpic, 0, 0, windowW, windowH) 
      // 识别小程序二维码
      var qrW = windowW*0.23;
      context.drawImage(that.data.qCord, 5, (windowH-qrW-2), qrW, qrW) 
      context.draw() 
  },
      
  // 点击保存按钮，同时将画布转化为图片    
  daochu: function () {   
      var that = this;  
      wx.canvasToTempFilePath({ 
          x: 0,
          y: 0, 
          canvasId: 'firstCanvas',  
          fileType: 'jpg',    
          quality: 1,       
          success: function (res) {  
              that.setData({ 
                  shareImage: res.tempFilePath
              })
              setTimeout(function(){
                  wx.showModal({
                      title: '提示',
                      content: '将生成的海报保存到手机相册，可以发送给微信好友或分享到朋友圈',
                      success(res) {
                          if (res.confirm) {
                              that.eventSave()
                          } else if (res.cancel) {
                              console.log('用户点击取消')
                          }
                      }
                  }) 
              },1000)
          }
      })  
  },
      
  // 将商品分享图片保存到本地
  eventSave() { 
      wx.saveImageToPhotosAlbum({   
          filePath: this.data.shareImage,
          success(res) {
              wx.showToast({
                  title: '保存图片成功',
                  icon: 'success',
                  duration: 2000
              })
          }
      })   
  },
      
  //将线上图片地址下载到本地，此函数进行了封装，只有在需要转换url的时候调用即可  
  getBG(url) {  
      // Promise函数给我很大的帮助，让我能return出回调函数中的值 
      return new Promise(function (resolve) { 
          wx.downloadFile({
              url: url, 
              success: function (res) {
                  url = res.tempFilePath
                  resolve(url);
              }
          })
      }) 
  },
      
  // canvas多文字换行
  newLine(txt, context) {    
      var txtArr = txt.split('') 
      var temp = ''   
      var row = [] 
      for (var i = 0; i < txtArr.length; i++) {
          if (context.measureText(temp).width < 210) { 
              temp += txtArr[i]
          } else {
              i--
              row.push(temp)
              temp = ''
          }
      }
      row.push(temp) 
      //如果数组长度大于3 则截取前三个
      if (row.length > 3) {
          var rowCut = row.slice(0, 3);
          var rowPart = rowCut[2];
          var test = "";
          var empty = [];
          for (var a = 0; a < rowPart.length; a++) { 
              if (context.measureText(test).width < 180) { 
                  test += rowPart[a];
              } else {
                  break;
              }
          } 
          empty.push(test);
          var group = empty[0] + "..." //这里只显示三行，超出的用...表示      
          rowCut.splice(2, 1, group);
          row = rowCut;
      }
      
      return row
      
  },
  //确认协议
  qrxy(){
    var that = this;
    app.request('/index/sure_xy',{}, function (data) {
      app.success('确认成功!', function () {
        that.setData({
          hidem : 1
        })
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  telmakekf: function (options) {
    wx.makePhoneCall({
      phoneNumber: '0758-3616910'
    })
  },
})