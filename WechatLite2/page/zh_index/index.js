const util = require('../../utils/util');
const Dialog = require('../../assets/libs/zanui/dialog/dialog');
var app = getApp();
Page({
  data: {
    hidem:1,
    xyinfo:'',
    work:{},
    userInfo: {
      id: 0,
      avatar: '/assets/img/avatar.png',
      username: '未登入',
      group_id:1,
    },
    groupName:['','劳务工','企业','推广员','团长'],
  },
  onLoad: function (options) {
    var that = this;
    //这里间隔获得
    that.checkUser();
  },
  checkUser:function(cb){
    var that = this;
    if (!app.globalData.userInfo && !app.globalData.openid){
      setTimeout(function () { that.checkUser(cb); }, 500);
    }else{
      if (!app.globalData.userInfo){
        //wx.redirectTo({url:'/page/my/login?bind=1'});
        return;
      }else{
        that.setData({ userInfo: app.globalData.userInfo});
        that.get_index_all_data();
      }
    }
  },
  onShow: function () {
    if (app.globalData.userInfo) {
      this.setData({ userInfo: app.globalData.userInfo});
    }
  },

  get_index_all_data:function(){
    var that = this;
    app.request('/user/getXy', { openid: app.globalData.openid }, function (data, ret) {
      let hidem = 1;//默认先隐藏
      if(data.xyinfo!=''){//未读
        hidem = 0;
      }
      that.setData({
        hidem: hidem,
        xyinfo: data.xyinfo,
        work: data.work
      });
      wx.stopPullDownRefresh();
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  //确认协议
  qrxy2(){
    var that = this;
    app.request('/user/sure_xy',{ openid: app.globalData.openid }, function (data) {
      app.success('确认成功!', function () {
        that.setData({
          hidem : 1
        })
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  onPullDownRefresh: function () {
    
  },

  onShareAppMessage: function () {
  
  },
  //确认协议
  qrxy(){
    var that = this;
    app.request('/index/sure_xy',{}, function (data) {
      app.success('确认成功!', function () {
        that.setData({
          hidem : 1
        })
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  telmakekf: function (options) {
    wx.makePhoneCall({
      phoneNumber: '0758-3616910'
    })
  },
  change_js: function () {
    var that = this;
    Dialog({
      title: '选择您的角色',
      message: '“八戒求才”，帮企业找人，帮蓝领找活，请选择您的角色',
      selector: '#zan-dialog-test',
      buttonsShowVertical: true,
      buttons: [{
        text: '劳务工',
        color: '#3CC51F',
        type: 1
      },
      {
        // 按钮文案
        text: '推广员',
        // 按钮文字颜色
        color: 'blue',
        // 按钮类型，用于在 then 中接受点击事件时，判断是哪一个按钮被点击
        type: 3
      },
      {
          text: '取消',
          type: 'cancel'
       }]
    }).then((res) => {
      console.log(res.type);
      if (res.type =='cancel'){
        return;
      }
      //数据库更新，用户类别
      app.request('/user/group', res, function (data) {
        app.success('角色选择成功!', function () {
          app.globalData.userInfo.group_id = res.type;
          that.setData({ userInfo: app.globalData.userInfo });
        });
      }, function (data, ret) {
        app.error(ret.msg);
      });

    })
  },
})