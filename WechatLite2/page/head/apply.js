// page/head/apply.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    applyInfo:null,
    rangeKey:0,
    openCity:[],
    idcard_img_front:'',
    idcard_img_backend:'',
    add_img_front:'/assets/img/add.png',
    add_img_backend:'/assets/img/add.png',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      openCity : app.globalData.city
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getInfo();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  //获取团长信息
  getInfo(){
    var that = this;
    app.request('/head/apply_info', {}, function (data, ret) {
      console.log(data);
      that.setData({
        applyInfo: data
      })
    }, function (data, ret) {
 
    });
  },
  bindPickerChange: function(e){
     this.setData({
       rangeKey:e.detail.value
     })
  },
  //上传正面
  uploadFront: function () {
    var that = this;
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              app.success('上传成功');
              that.setData({ 
                idcard_img_front: data.data.fullurl ,
                add_img_front:data.data.fullurl
              });
            }
          }
        });
      }
    });
  },
  //上传反面
  uploadBackend: function () {
    var that = this;
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              app.success('上传成功');
              that.setData({ 
                idcard_img_backend: data.data.fullurl ,
                add_img_backend:data.data.fullurl
              });
            }
          }
        });
      }
    });
  },
  formSubmit: function (event) {
    console.log(event.detail.value);
    var that = this;
    if (event.detail.value.name == '') {
      app.error('姓名不能为空');
      return;
    }
    if (event.detail.value.idcard == '') {
      app.error('身份证号码不能为空');
      return;
    }
    if (event.detail.value.phone == '') {
      app.error('手机号码不能为空');
      return;
    }
    if (event.detail.value.city_id == '') {
      app.error('请选择工作区域');
      return;
    }
    if (event.detail.value.idcard_img_front == '') {
      app.error('请上传身份证正面照片');
      return;
    }
    if (event.detail.value.idcard_img_backend == '') {
      app.error('请上传身份证反面照片');
      return;
    }
    app.request('/head/apply', event.detail.value, function (data) {
      app.success('申请成功!', function () {
        setTimeout(function () {
          //要延时执行的代码
          //wx.navigateBack({delta:1});
          that.getInfo();
        }, 2000); //延迟时间
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  // onPullDownRefresh: function () {

  // },

  /**
   * 页面上拉触底事件的处理函数
   */
  // onReachBottom: function () {

  // },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})