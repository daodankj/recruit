// page/my/contract.js
var app = getApp();
// canvas 全局配置
var context = null;// 使用 wx.createContext 获取绘图上下文 context
var isButtonDown = false;
var arrx = [];
var arry = [];
var arrz = [];
var canvasw = 1200;
var canvash = 300;
// //获取系统信息
// wx.getSystemInfo({
//   success: function (res) {
//     canvasw = res.windowWidth;//设备宽度
//     canvash = res.windowWidth*7/15;
//   }
// });
//注册页面
Page({
  canvasIdErrorCallback: function (e) {
    console.error(e.detail.errMsg)
  },
  canvasStart: function (event){
    isButtonDown = true;
    arrz.push(0);
    arrx.push(event.changedTouches[0].x);
    arry.push(event.changedTouches[0].y);
    //context.moveTo(event.changedTouches[0].x, event.changedTouches[0].y);
   
  },
  canvasMove: function (event) {
    if (isButtonDown) {
      arrz.push(1);
      arrx.push(event.changedTouches[0].x);
      arry.push(event.changedTouches[0].y);
      // context.lineTo(event.changedTouches[0].x, event.changedTouches[0].y);
      // context.stroke();
      // context.draw()
 
    }; 
    
    for (var i = 0; i < arrx.length; i++) {
      if (arrz[i] == 0) {
        context.moveTo(arrx[i], arry[i])
      } else {
        context.lineTo(arrx[i], arry[i])
      };
 
    };
    context.clearRect(0, 0, canvasw, canvash);
 
    context.setStrokeStyle('#000000');
    context.setLineWidth(4);
    context.setLineCap('round');
    context.setLineJoin('round');
    context.stroke();
    
    context.draw(false);
  },
  canvasEnd: function (event) {
    isButtonDown = false; 
  },
  cleardraw: function () {
    //清除画布
    arrx = [];
    arry = [];
    arrz = [];
    context.clearRect(0, 0, canvasw, canvash);
    context.draw(true);
  },
  getimg: function(){
    let that = this;
    if (arrx.length==0){
      wx.showModal({
        title: '提示',
        content: '签名内容不能为空！',
        showCancel: false
      });
      return false;
    };
    let formData = [];
    if (app.globalData.userInfo) {
        formData['user_id'] = app.globalData.userInfo.id;
        formData['token'] = app.globalData.userInfo.token;
    }
    //生成图片
    wx.canvasToTempFilePath({
      canvasId: 'canvas',
      success: function (res) {
        console.log(res.tempFilePath);
        //存入服务器
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: res.tempFilePath,
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              //app.success('上传成功');
              that.setData({ ["src"]: data.data.fullurl });
              that.saveContract();
            }
          },
          fail: function (res) {
            console.log(res);
          },
          complete: function (res) {
            
          }
        });
      }
    })
 
  },
  /**
   * 页面的初始数据
   */
  data: {
    src: "",
    signStatus:false,
    info:null,
    contract_tpl:''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 使用 wx.createContext 获取绘图上下文 context
    context = wx.createCanvasContext('canvas');
    context.beginPath() 
    context.setStrokeStyle('#000000');
    context.setLineWidth(4);
    context.setLineCap('round');
    context.setLineJoin('round');
 
    this.cleardraw();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getInfo();
  },
  //获取工作信息
  getInfo: function () {
      let that = this;
      app.request('/my/my_contract', {}, function (data, ret) {
          that.setData({
              info:  data.info,
              //contract_tpl: app.towxml.toJson(data.contract_tpl.replace(/: /g, ':'), 'html', that)
              contract_tpl: data.contract_tpl
          })
      }, function (data, ret) {
          app.error(ret.msg);
      });
  },
  changeShow: function(){
    var that = this;
    wx.navigateToMiniProgram({  
        appId:'wxa023b292fd19d41d', // 电子签小程序的appId  
        path:'pages/guide?from=SFY&to=CONTRACT_DETAIL&id='+that.data.info.contract.flow_id+'&name='+that.data.info.contract.name+'&phone='+that.data.info.contract.mobile, //${flowId}为流程 id，name、phone 按需给  
        envVersion:'release',
        success(res){
            // 打开成功 
        } 
    })
      // this.setData({
      //   signStatus: true,
      // })
  },
  //保存合同信息
  saveContract: function(){
    if(!this.data.src){
      app.error(签名未保存成功);
      return false;
    }
    let that = this;
      app.request('/my/save_contract', {
        id:that.data.info.id,
        c_url:that.data.src
      }, function (data, ret) {
          that.setData({
            signStatus:  false
          });
          that.getInfo();
      }, function (data, ret) {
          app.error(ret.msg);
      });
  }
})