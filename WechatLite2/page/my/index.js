const Dialog = require('../../assets/libs/zanui/dialog/dialog');
var app = getApp();
Page({
  data: {
    isWxapp: true,
    userInfo: {
      id: 0,
      avatar: '/assets/img/avatar.png',
      nickname: '游客',
      balance: 0,
      score: 0,
      level: 0,
      group_id:1,
    },
    com_name:'',
    jobs_num:0,
    resum_num:0,
    JobfairCount:0,
    activeCount:0,
    jobpub_num:3,
    groupName:['','我是劳务工','我是企业','我是推广员','我是团长'],
    finance_wages:0,
    work_num:0,
    drz_num:0,
    active_num:0,
    notice_num:0,
    showrc:0
  },
  onLoad: function () {
    
  },
  onShow: function () {
    var that = this;
    if (app.globalData.userInfo) {
      that.setData({ userInfo: app.globalData.userInfo, isWxapp: that.isWxapp() });
      //如果是企业 获得企业数据
     // if (app.globalData.userInfo.group_id==2){
        app.request('/Companyjob/get_my_companyName_jobsnum', {}, function (data, ret) {
          console.log(data);
          that.setData({
            com_name: data.name,
            jobs_num: data.jobCount,
            resum_num: data.resum_num,
            JobfairCount: data.JobfairCount,
            activeCount: data.activeCount,
            jobpub_num:data.job_count,
            finance_wages:data.finance_wages,
            work_num:data.work_num,
            drz_num:data.drz_num,
            active_num:data.active_num,
            notice_num:data.notice_num,
            showrc:data.showrc,
            c_id : data.Id
          })
        }, function (data, ret) {
          console.log(data);
          console.log(ret);
        });
        
     // }
    }else{
      app.error("请先登录", function () {
        setTimeout(function () { wx.redirectTo({url:'login'}) }, 2000);
      });
    }
  },
  login: function () {
    var that = this;
    app.login(function () {
      that.setData({ userInfo: app.globalData.userInfo, isWxapp: that.isWxapp() });
    });
  },
  isWxapp: function () {
    return  true;
    //return app.globalData.userInfo ? app.globalData.userInfo.username.match(/^u\d+$/) : true;
  },
  showTips: function (event) {
    var tips = {
      balance: '余额通过插件的出售获得',
      score: '积分可以通过回答问题获得',
      level: '等级通过官网活跃进行升级',
    };
    var type = event.currentTarget.dataset.type;
    var content = tips[type];
    wx.showModal({
      title: '温馨提示',
      content: content,
      showCancel: false
    });
  },
  //点击头像上传
  uploadAvatar: function () {
    var that = this;
    //var formData = app.globalData.config.upload.multipart;
    //var token = wx.getStorageSync('token') || '';
    let formData = [];
    if (app.globalData.userInfo) {
      formData['user_id'] = app.globalData.userInfo.id;
      formData['token'] = app.globalData.userInfo.token;
    }
    //formData.token = token;
    wx.chooseImage({
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        wx.uploadFile({
          url: app.globalData.config.upload.uploadurl,
          filePath: tempFilePaths[0],
          name: 'file',
          formData: formData,
          success: function (res) {
            var data = JSON.parse(res.data);
            if (data.code == 1) {
              app.request("/user/avatar", { avatar: data.data.url }, function (data, ret) {
                app.success('头像上传成功!');
                app.globalData.userInfo = data.userInfo;
                that.setData({ userInfo: data.userInfo, isWxapp: that.isWxapp() });
              }, function (data, ret) {
                app.error(ret.msg);
              });
            }
          }, error: function (res) {
            app.error("上传头像失败!");
          }
        });
      }
    });
  },
  onGotUserInfo: function (e) {
    console.log(e.detail.errMsg);
    console.log(e.detail.userInfo);
    console.log(e.detail.rawData);
    if (e.detail.userInfo == undefined){
      return;
    }
   
    //这里先本地赋值;
    app.globalData.userInfo.avatar = e.detail.userInfo.avatarUrl;
    app.globalData.userInfo.nickname = e.detail.userInfo.nickName;
    app.globalData.userInfo.mobile = app.globalData.userInfo.mobile == "NoLoginData" ? '' : app.globalData.userInfo.mobile;
    //提交到 服务器
    var token = wx.getStorageSync('token') || '';
    var that = this;
    wx.request({
      url: app.apiUrl + 'user/Updata_user_hawk',
      data: {
        userInfo: e.detail.rawData,
        mobile: app.globalData.userInfo.mobile,
        token: token,
        wxapp: 'wxapp2'
      },
      method: 'post',
      header: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      success: function (lres) {
        console.log(lres);
        /* wx.redirectTo({
           url: 'index'
         })*/
        that.onShow();
      }
    });
  },
  //新获取用户信息方法
  getUserProfile(){
    var that = this;
    // 推荐使用wx.getUserProfile获取用户信息，开发者每次通过该接口获取用户个人信息均需用户确认
    // 开发者妥善保管用户快速填写的头像昵称，避免重复弹窗
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        if (res.userInfo == undefined){
          return;
        }       
        //这里先本地赋值;
        app.globalData.userInfo.avatar = res.userInfo.avatarUrl;
        app.globalData.userInfo.nickname = res.userInfo.nickName;
        app.globalData.userInfo.mobile = app.globalData.userInfo.mobile == "NoLoginData" ? '' : app.globalData.userInfo.mobile;
        //提交到 服务器
        var token = wx.getStorageSync('token') || '';
        wx.request({
          url: app.apiUrl + 'user/Updata_user_hawk',
          data: {
            userInfo: res.rawData,
            mobile: app.globalData.userInfo.mobile,
            token: token,
            wxapp: 'wxapp2'
          },
          method: 'post',
          header: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          success: function (lres) {
            console.log(lres);
            that.onShow();
          }
        });
      }
    })
  },
  //绑定手机号码
  bindPhone(phoneNumber){
    var that = this;
    app.request('/user/bind_PhoneNum', {phone:phoneNumber}, function (data, ret) {
      app.globalData.userInfo.mobile = phoneNumber;
      app.success('手机号绑定成功');
      that.onShow();
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  //获取手机号码
  getPhoneNumber: function (e) {
    console.log(e.detail.errMsg);
    if (e.detail.iv == undefined){
      return;
    }
    console.log(e.detail.iv);
    console.log(e.detail.encryptedData);

    let dataTMp = [];
    dataTMp.encryptedData = e.detail.encryptedData;
    dataTMp.iv = e.detail.iv;
    var that = this;
    app.request('/Usewechat/get_PhoneNum', dataTMp, function (data, ret) {
      console.log(data);
      //app.success('成功获取手机号');
      //data = data.replace(/(^\s*)|(\s*$)/g, "");
      that.bindPhone(data.phoneNumber);
    }, function (data, ret) {
      //app.error(ret.msg);
    });
  },
  change_js: function () {
    var that = this;
    Dialog({
      title: '选择您的角色',
      message: '“八戒求才”，帮企业找人，帮蓝领找活，请选择您的角色',
      selector: '#zan-dialog-test',
      buttonsShowVertical: true,
      buttons: [{
        text: '劳务工',
        color: '#3CC51F',
        type: 1
      },
      {
        // 按钮文案
        text: '推广员',
        // 按钮文字颜色
        color: 'blue',
        // 按钮类型，用于在 then 中接受点击事件时，判断是哪一个按钮被点击
        type: 3
      },
      // {
      //   // 按钮文案
      //   text: '团长',
      //   // 按钮文字颜色
      //   color: 'green',
      //   // 按钮类型，用于在 then 中接受点击事件时，判断是哪一个按钮被点击
      //   type: 4
      // },
      // {
      //   // 按钮文案
      //   text: '企业',
      //   // 按钮文字颜色
      //   color: 'red',
      //   // 按钮类型，用于在 then 中接受点击事件时，判断是哪一个按钮被点击
      //   type: 2
      // },
      {
          text: '取消',
          type: 'cancel'
       }]
    }).then((res) => {
      console.log(res.type);
      if (res.type =='cancel'){
        return;
      }
      //数据库更新，用户类别
      app.request('/user/group', res, function (data) {
        app.success('角色选择成功!', function () {
          app.globalData.userInfo.group_id = res.type;
          that.setData({ userInfo: app.globalData.userInfo });
         // wx.reLaunch({
         //   url: '/page/index/index'
         // })
        });
      }, function (data, ret) {
        app.error(ret.msg);
      });

    })
  },
  //跳转商城
  goshop(){
    if(app.globalData.userInfo.id){
      wx.navigateTo({
        url: '/page/location/webview?uid='+app.globalData.userInfo.id
      })
    }else{
      wx.navigateTo({
        url: '/page/location/webview'
      })
    }
  },
  telmake: function (options) {
    wx.makePhoneCall({
      phoneNumber: '0758-3616910'
    })
  },
  charge:function(){
    wx.navigateTo({
      url: '/page/charge/index'
    })
  }
})
