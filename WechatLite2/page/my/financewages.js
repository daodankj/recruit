
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
      list:[],
      loading: false,
      nodata: false,
      nomore: false,
      hidem:1,
      info:{},
      remark:'',
      markhide:1,
      objid:0
  },
  page: 1,
  page_block: 10,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.page = 1;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },

  getList:function(cb){
      let that = this;
      if (that.data.nomore == true || that.data.loading == true) {
        return;
      }
      this.setData({ loading: true });
      app.request('/my/get_finance_wages', {
        page: that.page,
        page_block: that.page_block,
      }, function (data, ret) {
        that.setData({
          loading: false,
          nodata: that.page == 1 && data.list.length == 0 ? true : false,
          nomore: (that.page > 1 && data.list.length == 0) ||
            (that.page == 1 && data.list.length < that.page_block && data.list.length > 0) ? true : false,
          list: that.page > 1 ? that.data.list.concat(data.list) : data.list
        })
        that.page++;
        typeof cb == 'function' && cb(data);
      }, function (data, ret) {
        app.error(ret.msg);
      });
  },
  //确认
  sureCheck: function(e){
    let that = this;
    let id = e.currentTarget.dataset.id;
    let status = e.currentTarget.dataset.status;
    wx.showModal({
      title: '提示',
      content: '确认该操作吗?',
      success: function (res) {
        if (res.confirm) {         
          app.request('/my/check_finance_wages', {
            id: id,
            status:status
          }, function (data, ret) {
            app.success('操作成功');
            that.onPullDownRefresh();
          }, function (data, ret) {
            app.error(ret.msg);
          });
        } else if (res.cancel) {
          
        }
      }
    })
  },
  //反馈按钮
  sureCheck2: function(e){
    let id = e.currentTarget.dataset.id;
    this.setData({
      markhide:0,
      objid:id
    })
  },
  //取消备注
  qxmark: function(e){
    this.setData({
      markhide:1,
      remark:''
    })
  },
  //提交反馈
  bindFormSubmit:function(event){
    var that = this
    var remark = event.detail.value.remark
    app.request('/my/check_finance_wages', {
      id: that.data.objid,
      status:2,
      remark:remark
    }, function (data, ret) {
      app.success('操作成功');
      that.onPullDownRefresh();
    }, function (data, ret) {
      app.error(ret.msg);
    });

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.page = 1;
    this.setData({ nodata: false, nomore: false ,markhide:1,remark:''});
    this.getList(function () {
        wx.stopPullDownRefresh();
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.getList(function (data) {
    if (data.list.length == 0) {
        app.info("暂无更多数据");
    }
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  info(e){
    let info = e.currentTarget.dataset.info;
    this.setData({
      hidem : 0,
      info: info
    })
  },
  qrxy(){
    this.setData({
      hidem : 1
    })
  }
})