// page/my/login.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isBind:false,
    mobile:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //console.log(options)
    if(options.bind==1){
      wx.setNavigationBarTitle({
        title : '手机绑定'
      })
      this.setData({
        isBind: true
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
  formSubmit: function (event) {
    var that = this;
    if (event.detail.value.mobile == '') {
      app.error('手机号码不能为空');
      return;
    }
    if (event.detail.value.password == '') {
      app.error('密码不能为空');
      return;
    }
    app.request('/user/login_password', event.detail.value, function (data) {
      app.globalData.userInfo = data.userInfo;
      wx.setStorageSync('token', data.userInfo.token);
      app.success('登入成功!', function () {
        setTimeout(function () {
          //要延时执行的代码
          wx.switchTab({
            url: 'index'
          });
        }, 2000); //延迟时间
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },

  bindKeyMobile:function(e){
    this.setData({
      mobile: e.detail.value
    })
  },

  //获取验证码
  sendCode: function(){
    if(this.data.mobile){
      app.request('/user/getCode', {mobile:this.data.mobile}, function (data, ret) {
        //console.log(data);
        app.success(ret.msg)
      }, function (data, ret) {
        app.error(ret.msg)
      });
    }else{
      app.error("请输入手机号码")
    }
  },
  formSubmit2: function (event) {
    var that = this;
    if (event.detail.value.mobile == '') {
      app.error('手机号码不能为空');
      return;
    }
    if (event.detail.value.captcha == '') {
      app.error('验证码不能为空');
      return;
    }
    event.detail.value.openid = app.globalData.openid;
    app.request('/user/bindMobile2', event.detail.value, function (data) {
      app.success('绑定成功!', function () {
        //app.globalData.userInfo.mobile = event.detail.value.mobile;
        app.globalData.userInfo = data.userInfo;
        wx.setStorageSync('token', data.userInfo.token);
        setTimeout(function () {
          //要延时执行的代码
          wx.switchTab({
            url: '/page/zh_index/index'
          });
        }, 2000); //延迟时间
      });
    }, function (data, ret) {
       app.error(ret.msg);
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})