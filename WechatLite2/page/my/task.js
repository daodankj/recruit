// page/my/task.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        workinfo:null,
        worklist:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.getList();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    //获取工作信息
    getList: function () {
        let that = this;
        app.request('/my/my_work', {}, function (data, ret) {
            that.setData({
                workinfo:  data.workinfo,
                worklist:  data.list
            })
        }, function (data, ret) {
            app.error(ret.msg);
        });
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})