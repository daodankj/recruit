// page/train/info.js
var app = getApp();
const Dialog = require('../../assets/libs/zanui/dialog/dialog');
const util = require('../../utils/util');
Page({

    /**
     * 页面的初始数据
     */
    data: {
        id:null,
        info:{},
        phoneNumber:'',
        canSendData: true,
        content: 'toptips',
        duration: 3000,
        $zanui: {
            toptips: {
                show: false
            }
        },
    },

    getData: function(){
        //post 拖取数据
        var that = this;
        app.request('/train/info', { id: that.data.id }, function (data, ret) {
          console.log(data);
          if (data.content != '') {
            //data.content = data.content.replace(/[\n\r]/g, '<br>');
            //data.content = app.towxml.toJson(data.content.replace(/: /g, ':'), 'html', that);
            //data.content = app.towxml.toJson(data.content, 'html', that);
            that.traverse(data.content);
          }
          
          that.setData({
            info: data,
          })
    
        }, function (data, ret) {
          app.error(ret.msg);
        });
    },
    onShow: function () {
        var that = this;
        //这里间隔获得
        app.Log_after_fun(function () {
          that.getData();
        });
    },
    onLoad: function (options) {
        var that = this;
        console.log(options);
        if (options.id != undefined) {
          that.setData({
            id: options.id
          })
        }
        
        if (options.scene != undefined) {
          var scene = decodeURIComponent(options.scene);
          that.setData({
            id: scene
          })
        }
    },

    onShareAppMessage: function () {
      var that = this;
      var url = '/page/train/info?id=' + that.data.info.id;
      return {
        title: '' + that.data.info.name,
        path: url
      }
    },
     
    traverse: function (obj) {
        for (var a in obj) {
          if (typeof (obj[a]) == "object") {
            this.traverse(obj[a]); //递归遍历  
          } else {
            if (a == 'src' && obj[a].indexOf("/uploads/") == 0) {
              //console.log(a + "=" + obj[a]); 
              obj[a] = app.Domain_Public + obj[a];
            }
          }
        }
    },
    
    getPhoneNumber: function (e) {
        console.log(e.detail.errMsg);
        if (e.detail.iv == undefined){
          return;
        }
        console.log(e.detail.iv);
        console.log(e.detail.encryptedData);
    
        let dataTMp = [];
        dataTMp.encryptedData = e.detail.encryptedData;
        dataTMp.iv = e.detail.iv;
        var that = this;
        app.request('/Usewechat/get_PhoneNum', dataTMp, function (data, ret) {
          console.log(data);
          app.success('成功获取手机号');
          //data = data.replace(/(^\s*)|(\s*$)/g, "");
          that.setData({
            phoneNumber: data.phoneNumber
          });
        }, function (data, ret) {
          //app.error(ret.msg);
        });
    },
    formSubmit: function (e) {
        var that = this;
        //首先验证表单
        var ShowErr_Me = "";
        let Outdata = e.detail.value;
        Outdata.train_id = that.data.id;
        if (!(/^1[34578]\d{9}$/.test(Outdata.phone))) {
          ShowErr_Me = "正确输入正确的手机号";
        } 
        if (Outdata.name.length < 2 || Outdata.name.length > 20) {
          ShowErr_Me = "请正确填写您的姓名";
        }
        if (ShowErr_Me != "") {
          that.showTopTips(ShowErr_Me);
          console.log(ShowErr_Me);
          return false;
        }
        if (!that.data.canSendData) {
          return;
        }
        that.data.canSendData = false;
        app.request('/train/add_baoming', Outdata, function (data, ret) {
          console.log(data);
          app.success('报名成功!');
          /*
    
          */
          setTimeout(function () { 
            that.getData();
            that.data.canSendData = true; 
            }, 1500)
    
        }, function (data, ret) {
          app.error(ret.msg);   
          setTimeout(function () { that.data.canSendData = true; }, 1000)
        });
        
    },
    showTopTips(mes) {
        this.setData({
          content: mes,
          $zanui: {
            toptips: {
              show: true
            }
          }
        });
    
        setTimeout(() => {
          this.setData({
            $zanui: {
              toptips: {
                show: false
              }
            }
          });
        }, this.data.duration);
    },
})