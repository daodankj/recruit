<?php

namespace app\api\controller;

use app\common\controller\Api;
use addons\faqueue\library\QueueApi;
/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    /**
     * 首页
     *
     */
    public function index()
    {
        $this->success('请求成功');
    }

    //每月企业职务发布次数复原
    public function reset_jobcount(){
        
        db('recruit_company')->where('id>0')->update(['job_count'=>3]);
        $this->success('请求成功');
    }

    //超过16小时未签退短信提醒
    public function dk_notice(){
        $num = 0;
        $stime = time() - 16*3600;//16小时之前
        db('daka')->field('id,user_id')
                  ->where(['status'=>0,'out_time'=>0,'in_time'=>['<',$stime]])
                  ->chunk(100, function ($items) use(&$num) {
                        $items = collection($items)->toArray();
                        foreach ($items as $index => $item) {
                            //获取会员信息
                            $uinfo = db('user')->where(['id'=>$item['user_id']])->field('username,mobile')->find();
                            if ($uinfo['mobile']) {
                                //模板短信通知
                                $msg = ['name'=>$uinfo['username']];
                                QueueApi::smsNotice($uinfo['mobile'],$msg,'SMS_238463016');
                            }
                            db('daka')->where(['id'=>$item['id']])->update(['status'=>1]);//修改通知状态
                            $num++;
                        }
                    });
        //echo db('daka')->getLastSql();
        echo '执行完成,受影响条数:'.$num;
    }
}
