<?php

namespace app\admin\behavior;
use think\Db;
use addons\faqueue\library\QueueApi;

class ApplyCheck
{
    /**
     *type标记审核对象data对应申请对象信息
    */
    public function run(&$info)
    {
        
        if (!$info['data']['user_id']) {
            return true;
        }
        
        $msg = $this->getMsgInfo($info);
        if (!$msg) {
            return false;
        }

        if ($info['status']==1) {//审核通过
            $msg = $msg.'已审批通过。';
        }else if($info['status']==2){
            if (isset($info['data']['result'])&&$info['data']['result']) {
                $msg = $msg.'未审批通过！原因：'.$info['data']['result'].'。';
            }else{
                $msg = $msg.'未审批通过！';
            }         
        }else{
            return false;
        }

        //加入系统消息
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => $info['type'],
            'obj_id' => isset($info['data']['id'])?$info['data']['id']:$info['data']['Id'],
            'notice' => $msg,
            'createtime'=> time()
        ];
        db('user_notice')->insert($insert_data);

        //短信通知
        //获取会员信息
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('username,mobile')->find();
        if ($uinfo['mobile']) {
            $mtname = $this->getMsgInfoName($info);
            //模板短信通知
            $msg = ['mtname'=>$mtname,'submittime'=>date('Y-m-d')];
            QueueApi::smsNotice($uinfo['mobile'],$msg,'SMS_232891174');
        }
        //微信通知
    }

    private function getMsgInfo($info){
        $actionName = [
            '1' => '您的团长申请',
            '2' => '您的参团申请',
            '3' => '您的企业入驻申请',
            '4' => '您发布的招募活动',
            '5' => '您的实名认证申请'
        ];
        return isset($actionName[$info['type']])?$actionName[$info['type']]:'';
    }
    private function getMsgInfoName($info){
        $actionName = [
            '1' => '团长',
            '2' => '参团',
            '3' => '企业入驻',
            '4' => '发布的招募活动',
            '5' => '实名认证'
        ];
        return isset($actionName[$info['type']])?$actionName[$info['type']]:'';
    }
}
