<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use addons\faqueue\library\QueueApi;
/**
 * 财务对账管理
 *
 * @icon fa fa-circle-o
 */
class FinanceWages extends Backend
{
    
    /**
     * FinanceWages模型对象
     * @var \app\admin\model\FinanceWages
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\FinanceWages;

    }

    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->where($where_m)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }




    /**
     * 添加
     */
    public function add()
    {
        set_time_limit(0);
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    //查看所有劳务工
                    db('active_bmlist')->where('status>0 and labor_id='.$params['labor_id'])->field('user_id,name,phone')
                    ->chunk(200, function ($items) use (&$params,&$result) {
                        $items = collection($items)->toArray();
                        foreach ($items as $index => $item) {
                            //查看是否已经生成
                            $hasin = db('finance_wages')->where('year='.$params['year'].' and month='.$params['month'].' and user_id='.$item['user_id'])->value('id');
                            if ($hasin) {
                                continue;
                            }
                            $start_time = strtotime($params['year'].'-'.$params['month'].'-01');
                            $end_time = strtotime("+1 month",$start_time);

                            $total_day = db('daka')->where('user_id='.$item['user_id'].' and in_time>'.$start_time.' and in_time<'.$end_time)->count();
                            if ($total_day && $total_day>0) {
                                //分别计算白班和夜班时间
                                $dainfo1 = db('daka')->field("sum(work_time) as total_time,sum(money) as total_money")->where('type=1 and user_id='.$item['user_id'].' and in_time>'.$start_time.' and in_time<'.$end_time)->find();
                                $dainfo2 = db('daka')->field("sum(work_time) as total_time,sum(money) as total_money")->where('type=2 and user_id='.$item['user_id'].' and in_time>'.$start_time.' and in_time<'.$end_time)->find();
                                //空值转换
                                $dainfo1['total_time'] = $dainfo1['total_time']??0;
                                $dainfo1['total_money'] = $dainfo1['total_money']??0;
                                $dainfo2['total_time'] = $dainfo2['total_time']??0;
                                $dainfo2['total_money'] = $dainfo2['total_money']??0;
                                $data = [
                                    'year'    => $params['year'],
                                    'month'   => $params['month'],
                                    'user_id' => $item['user_id'],
                                    'city_id' => 0,
                                    'service_id'=>0,
                                    'labor_id'=> $params['labor_id'],
                                    'total_time' => $dainfo1['total_time']+$dainfo2['total_time'],
                                    'total_money'=> $dainfo1['total_money']+$dainfo2['total_money'],
                                    'total_day'  => $total_day,
                                    'total_time_b'=> $dainfo1['total_time'],
                                    'total_money_b'=> $dainfo1['total_money'],
                                    'total_time_y'=> $dainfo2['total_time'],
                                    'total_money_y'=> $dainfo2['total_money'],
                                    'yf_money'     => $dainfo1['total_money']+$dainfo2['total_money'],
                                    'send_money'     => $dainfo1['total_money']+$dainfo2['total_money']
                                ];
                                $result = $this->model->isUpdate(false)->save($data);
                                //模板短信通知
                                if ($result&&$item['phone']) {
                                    $msg = ['name'=>$item['name'],'submittime'=>$data['year'].'-'.$data['month']];
                                    QueueApi::smsNotice($item['phone'],$msg,'SMS_232906142');
                                }
                            }

                        }
                    },'user_id');
                    
                    //$result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    /**
     * 导入
     */
    public function import()
    {
        //判断当前管理员是否是劳务机构
        if ($this->auth->type!=3||!$this->auth->labor_id) {
            $this->error('您当前账号不属于劳务机构，无法导入');
        }
        $file = $this->request->request('file');
        if (!$file) {
            $this->error(__('Parameter %s can not be empty', 'file'));
        }
        $filePath = ROOT_PATH . DS . 'public' . DS . $file;
        if (!is_file($filePath)) {
            $this->error(__('No results were found'));
        }
        //实例化reader
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        if (!in_array($ext, ['csv', 'xls', 'xlsx'])) {
            $this->error(__('Unknown data format'));
        }
        if ($ext === 'csv') {
            $file = fopen($filePath, 'r');
            $filePath = tempnam(sys_get_temp_dir(), 'import_csv');
            $fp = fopen($filePath, "w");
            $n = 0;
            while ($line = fgets($file)) {
                $line = rtrim($line, "\n\r\0");
                $encoding = mb_detect_encoding($line, ['utf-8', 'gbk', 'latin1', 'big5']);
                if ($encoding != 'utf-8') {
                    $line = mb_convert_encoding($line, 'utf-8', $encoding);
                }
                if ($n == 0 || preg_match('/^".*"$/', $line)) {
                    fwrite($fp, $line . "\n");
                } else {
                    fwrite($fp, '"' . str_replace(['"', ','], ['""', '","'], $line) . "\"\n");
                }
                $n++;
            }
            fclose($file) || fclose($fp);

            $reader = new Csv();
        } elseif ($ext === 'xls') {
            $reader = new Xls();
        } else {
            $reader = new Xlsx();
        }


        //加载文件
        $insert = [];
        try {
            if (!$PHPExcel = $reader->load($filePath)) {
                $this->error(__('Unknown data format'));
            }
            $currentSheet = $PHPExcel->getSheet(0);  //读取文件中的第一个工作表
            $allColumn = $currentSheet->getHighestDataColumn(); //取得最大的列号
            $allRow = $currentSheet->getHighestRow(); //取得一共有多少行
            $maxColumnNumber = Coordinate::columnIndexFromString($allColumn);
            /*$fields = [];
            for ($currentRow = 1; $currentRow <= 1; $currentRow++) {
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $fields[] = $val;
                }
            }*/
            for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                $values = [];
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $values[] = is_null($val) ? '' : $val;
                }
                /*$row = [];
                $temp = array_combine($fields, $values);
                foreach ($temp as $k => $v) {
                    if (isset($fieldArr[$k]) && $k !== '') {
                        $row[$fieldArr[$k]] = $v;
                    }
                }
                if ($row) {
                    $insert[] = $row;
                }*/
                //----
                if ($values) {
                    $insert[] = $values;
                }
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
        if (!$insert) {
            $this->error(__('No rows were updated'));
        }
        //print_r($insert);exit;
        $insertData = [];
        $failData = [];//过滤掉的人员
        $i = 0;
        $j = 0;
        try {
            foreach ($insert as $val) {
                $uinfo = $this->getUserId($val);
                if (!$uinfo) {
                    $failData[$j]['city_id'] = 0;
                    $failData[$j]['service_id'] = 0;
                    $failData[$j]['labor_id'] = $this->auth->labor_id;
                    $failData[$j]['day'] = $val[0];
                    $failData[$j]['name'] = $val[2];
                    $failData[$j]['idcard'] = $val[1];
                    $failData[$j]['remark'] = '未查到该会员';
                    $j++;
                    continue;//如果存在
                }
                $user_id = $uinfo['id'];
                //获取月份
                $datemonth = explode('-', $val[0]);
                //过滤重复月份
                $hasin = db('finance_wages')->where('year='.$datemonth[0].' and month='.$datemonth[1].' and user_id='.$user_id)->value('id');
                if ($hasin) {
                    $failData[$j]['city_id'] = 0;
                    $failData[$j]['service_id'] = 0;
                    $failData[$j]['labor_id'] = $this->auth->labor_id;
                    $failData[$j]['day'] = $val[0];
                    $failData[$j]['name'] = $val[2];
                    $failData[$j]['idcard'] = $val[1];
                    $failData[$j]['remark'] = '已经导入过，重复导入';
                    $j++;
                    continue;
                }
                $insertData[$i]['phone'] = $uinfo['mobile'];//短信通知用,薪资表未存储
                $insertData[$i]['username'] = $uinfo['username'];//短信通知用,薪资表未存储

                $insertData[$i]['year'] = $datemonth[0];
                $insertData[$i]['month'] = $datemonth[1];
                $insertData[$i]['user_id'] = $user_id;
                $insertData[$i]['city_id'] = 0;
                $insertData[$i]['service_id'] = 0;
                $insertData[$i]['labor_id'] = $this->auth->labor_id;
            
                $insertData[$i]['total_day'] = $val[3]?$val[3]:0;
                $insertData[$i]['total_time_b'] = $val[5]?$val[5]:0;
                $insertData[$i]['total_money_b_price'] = $val[6]?$val[6]:0;//工时价
                $insertData[$i]['total_money_b'] = $insertData[$i]['total_time_b']*$insertData[$i]['total_money_b_price'];
                $insertData[$i]['total_time_y'] = $val[8]?$val[8]:0;
                $insertData[$i]['total_money_y_price'] = $val[9]?$val[9]:0;//工时价
                $insertData[$i]['total_money_y'] = $insertData[$i]['total_time_y']*$insertData[$i]['total_money_y_price'];
                $insertData[$i]['subsidy_money_y'] = $val[11]?$val[11]:0;
                $insertData[$i]['subsidy_money_tj'] = $val[12]?$val[12]:0;
                $insertData[$i]['subsidy_money_gw'] = $val[13]?$val[13]:0;
                $insertData[$i]['subsidy_money_cf'] = $val[14]?$val[14]:0;

                $insertData[$i]['deduction_money_wm'] = $val[15]?$val[15]:0;
                $insertData[$i]['deduction_money_gy'] = $val[16]?$val[16]:0;
                $insertData[$i]['deduction_money_cz'] = $val[17]?$val[17]:0;
                $insertData[$i]['deduction__money_cf'] = $val[18]?$val[18]:0;
                $insertData[$i]['deduction_money_sdf'] = $val[19]?$val[19]:0;
                $insertData[$i]['deduction_money_sb'] = $val[20]?$val[20]:0;
                $insertData[$i]['deduction_money_fk'] = $val[21]?$val[21]:0;
                $insertData[$i]['yf_money'] = $insertData[$i]['total_money_b']+$insertData[$i]['total_money_y']+$insertData[$i]['subsidy_money_y']+$insertData[$i]['subsidy_money_tj']+$insertData[$i]['subsidy_money_gw']+$insertData[$i]['subsidy_money_cf']-$insertData[$i]['deduction_money_wm']-$insertData[$i]['deduction_money_gy']-$insertData[$i]['deduction_money_cz']-$insertData[$i]['deduction__money_cf']-$insertData[$i]['deduction_money_sdf']-$insertData[$i]['deduction_money_sb']-$insertData[$i]['deduction_money_fk'];//应发
                $insertData[$i]['deduction_money_gs'] = $val[23]?$val[23]:0;
                $insertData[$i]['deduction_money_yz'] = $val[24]?$val[24]:0;

                $insertData[$i]['total_time'] = $insertData[$i]['total_time_b']+$insertData[$i]['total_time_y'];//总工时
                $insertData[$i]['total_money'] = $insertData[$i]['total_money_b']+$insertData[$i]['total_money_y'];//总工资
                $insertData[$i]['send_money'] = $insertData[$i]['yf_money']-$insertData[$i]['deduction_money_gs']-$insertData[$i]['deduction_money_yz'];//实发  应发工资-个税-预支
                $i++;
            }
            $this->model->allowField(true)->saveAll($insertData);
            //失败记录
            if ($failData) {
                $fmodel = new \app\admin\model\FinanceWagesfail;
                $fmodel->allowField(true)->saveAll($failData);
            }
            //短信通知
            foreach ($insertData as $key => $val) {
                if ($val['phone']) {
                    //模板短信通知
                    $msg = ['name'=>$val['username'],'submittime'=>$val['year'].'-'.$val['month']];
                    QueueApi::smsNotice($val['phone'],$msg,'SMS_232906142');
                }
            }

        } catch (PDOException $exception) {
            $msg = $exception->getMessage();
            if (preg_match("/.+Integrity constraint violation: 1062 Duplicate entry '(.+)' for key '(.+)'/is", $msg, $matches)) {
                $msg = "导入失败，包含【{$matches[1]}】的记录已存在";
            };
            $this->error($msg);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success('成功导入：'.$i.'条数据。导入失败：'.$j.'条');
    }

    //判断员工是否存在
    private function getUserId($val){
        if (!$val[1]) {//名字空的不管
            return false;
        }
        $uinfo = db('user')->where(['idcard'=>$val[1]])->field('id,username,mobile')->find();
        if (!$uinfo) {//不存在
            return false;
        }
        return $uinfo;
    }

    //导出Excel表格,存到文件夹
  public function saveExport($data,$excelFileName,$sheetTitle ,$width =null){
    /* 实例化类 */
    $objPHPExcel = new \PHPExcel();

    /* 设置输出的excel文件为2007兼容格式 */
    //$objWriter=new PHPExcel_Writer_Excel5($objPHPExcel);//非2007格式
    $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);

    /* 设置当前的sheet */
    $objPHPExcel->setActiveSheetIndex(0);

    $objActSheet = $objPHPExcel->getActiveSheet();

    /*设置宽度*/
    if(!empty($width)){
      foreach ($width as $k => $v) {
        $objPHPExcel->getActiveSheet()->getColumnDimension($v[0])->setWidth($v[1]);
      }
    }
    /* sheet标题 */
    $objActSheet->setTitle($sheetTitle);

    $i = 1;
    foreach($data as $value) {
      /* excel文件内容 */
      $j = 'A';
      foreach ($value as $value2) {
        //$value2=iconv("gbk","utf-8",$value2);
        $objActSheet->setCellValueExplicit($j . $i,$value2,\PHPExcel_Cell_DataType::TYPE_STRING);
        $j++;
      }
      $i++;
    }
    $rootPath = ROOT_PATH.'public'.DS.'uploads';
    $filename = $rootPath.DS.$excelFileName.".xlsx";
    $filename=iconv("utf-8","gbk",$filename);
    $objWriter->save($filename);
    return $filename;
  }

}
