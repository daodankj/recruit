<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
/**
 * 时薪职位发布
 *
 * @icon fa fa-circle-o
 */
class SxJob extends Backend
{
    
    /**
     * SxJob模型对象
     * @var \app\admin\model\SxJob
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\SxJob;

    }

    public function import()
    {
        parent::import();
    }

    
    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = false;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                    ->where($where_m)
                    ->where($where)
                    ->order($sort, $order)
                    ->paginate($limit);

            /*foreach ($list as $row) {
                $row->visible(['id','name','type','user_num','settlement_type','head_open','createtime','status']);
                
            }*/

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    //$params['worktime'] = $params['start_date'].'至'.$params['end_date'];
                    //设置所属城市
                    if ($params['labor_id']) {
                        $linfo = db('recruit_labor')->where(['id'=>$params['labor_id']])->field('city_id,service_id')->find();
                        $params['city_id'] = $linfo['city_id'];
                        $params['service_id'] = $linfo['service_id'];
                    }else {
                        $params['city_id'] = $this->auth->city_id;
                        $params['service_id'] = $this->auth->service_id;
                        $params['labor_id'] = $this->auth->labor_id;
                    }
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    //$params['worktime'] = $params['start_date'].'到'.$params['end_date'];
                    //设置所属城市
                    if ($params['labor_id']) {
                        $linfo = db('recruit_labor')->where(['id'=>$params['labor_id']])->field('city_id,service_id')->find();
                        $params['city_id'] = $linfo['city_id'];
                        $params['service_id'] = $linfo['service_id'];
                    }else {
                        $params['city_id'] = $this->auth->city_id;
                        $params['service_id'] = $this->auth->service_id;
                        $params['labor_id'] = $this->auth->labor_id;
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

}
