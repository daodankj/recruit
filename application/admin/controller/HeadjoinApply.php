<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use Exception;
/**
 * 团长参团申请
 *
 * @icon fa fa-circle-o
 */
class HeadjoinApply extends Backend
{
    
    /**
     * HeadjoinApply模型对象
     * @var \app\admin\model\HeadjoinApply
     */
    protected $model = null;
    //protected $searchFields = 'active.name';//默认搜索字段
    //protected $relationSearch=true;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\HeadjoinApply;

    }

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type!=1) {//除了管理员和总公司外
                $where_m['active.service_id'] = $this->auth->city_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['active.service_id'] = $this->auth->city_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->with('active')
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 批量更新
     */
    /*public function multi($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            if ($this->request->has('params')) {
                parse_str($this->request->post("params"), $values);
                $values = $this->auth->isSuperAdmin() ? $values : array_intersect_key($values, array_flip(is_array($this->multiFields) ? $this->multiFields : explode(',', $this->multiFields)));
                if ($values) {
                    $adminIds = $this->getDataLimitAdminIds();
                    if (is_array($adminIds)) {
                        $this->model->where($this->dataLimitField, 'in', $adminIds);
                    }
                    $count = 0;
                    Db::startTrans();
                    try {
                        $list = $this->model->where($this->model->getPk(), 'in', $ids)->select();
                        foreach ($list as $index => $item) {
                            $count += $item->allowField(true)->isUpdate(true)->save($values);
                            if ($values['status']==1) {
                                $infodata = ['status'=>1,'type'=>2,'data'=>$item];
                                \think\Hook::listen("apply_check", $infodata);
                            }else{
                                $infodata = ['status'=>2,'type'=>2,'data'=>$item];
                                \think\Hook::listen("apply_check", $infodata);
                            }
                        }
                        Db::commit();
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                    if ($count) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                } else {
                    $this->error(__('You have no permission'));
                }
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }*/

    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该记录不存在，审核失败'.$id);
            }
            if ($info->status!=0) {
                $this->success('改记录已被审核，不用重新审核');
            }
            $info->status = $status;
            //$info->result = $result;
            //$info->check_admin_id = $this->auth->id;
            //$info->check_admin_name = $this->auth->nickname;
            //$info->check_time = time();
            if ($info->save()) {
                //消息通知
                //监听审核事件
                $infodata = ['status'=>$status,'type'=>2,'data'=>$info];
                \think\Hook::listen("apply_check", $infodata);
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    
    

}
