<?php

namespace app\admin\controller;

use app\common\controller\Backend;

use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 区域服务商
 *
 * @icon fa fa-circle-o
 */
class RecruitService extends Backend
{
    
    /**
     * RecruitService模型对象
     * @var \app\admin\model\RecruitService
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\RecruitService;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 批量更新
     */
    public function multi($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            if ($this->request->has('params')) {
                parse_str($this->request->post("params"), $values);
                $values = $this->auth->isSuperAdmin() ? $values : array_intersect_key($values, array_flip(is_array($this->multiFields) ? $this->multiFields : explode(',', $this->multiFields)));
                if ($values) {
                    $adminIds = $this->getDataLimitAdminIds();
                    if (is_array($adminIds)) {
                        $this->model->where($this->dataLimitField, 'in', $adminIds);
                    }
                    $count = 0;
                    Db::startTrans();
                    try {
                        $list = $this->model->where($this->model->getPk(), 'in', $ids)->select();
                        foreach ($list as $index => $item) {
                            $count += $item->allowField(true)->isUpdate(true)->save($values);
                            //关闭和开启对应管理员
                            if ($values['status']==1) {
                                db('admin')->where(['service_id'=>$item->id])->update(['type'=>2,'status'=>'normal']);
                            }else{
                                db('admin')->where(['service_id'=>$item->id])->update(['type'=>2,'status'=>'hidden']);
                            }
                        }
                        Db::commit();
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                    if ($count) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                } else {
                    $this->error(__('You have no permission'));
                }
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
    

}
