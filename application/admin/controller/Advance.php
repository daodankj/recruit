<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use tools\Wexinpay;

/**
 * 预支申请记录
 *
 * @icon fa fa-circle-o
 */
class Advance extends Backend
{
    
    /**
     * Advance模型对象
     * @var \app\admin\model\Advance
     */
    protected $model = null;
    protected $searchFields = 'username';//默认搜索字段

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Advance;

    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();
            
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function import()
    {
        parent::import();
    }

    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                $result = $status==2?"预支超额":"已打款";
            }
            
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该记录不存在，审核失败'.$id);
            }
            if ($info->status!=1) {
                $this->error('记录不在待复审状态，无法审批');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_admin_id = $this->auth->id;
            $info->check_admin_name = $this->auth->nickname;
            $info->check_time = time();
            if ($info->save()) {
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    
    public function test(){
        $option = [
            'appid' => 'wx7f30b25fcc0561db',
            'out_batch_no' => 'transfer'.date('YmdHis').rand(100,999),
            'batch_name' => '测试转账',
            'batch_remark' => '测试转账啊',
            'total_amount' => 100,
            'total_num' => 1,
            'transfer_detail_list' => [
                [
                    'out_detail_no' => 'test001',
                    'transfer_amount' => 100,
                    'transfer_remark' => '测试',
                    'openid' => 'oGS8C4yOxUFtgqsyiCTtTP_97ucg'
                ]
            ],
            'transfer_scene_id' => "1000"
        ];
        $wexinpay = new Wexinpay;
        $res = $wexinpay->transfer($option);
        print_r($res);
    }
}
