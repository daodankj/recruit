<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;

/**
 * 工作任务管理
 *
 * @icon fa fa-circle-o
 */
class RecruitWork extends Backend
{
    
    /**
     * RecruitWork模型对象
     * @var \app\admin\model\RecruitWork
     */
    protected $model = null;
    protected $noNeedRight = ['sure_select'];//两个权限合并成一个
    protected $searchFields = 'name';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\RecruitWork;

    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function import()
    {
        parent::import();
    }

    /**
     * 添加人员选择页面
     */
    public function select($ids='')
    {
        //当前是否为关联查询
        //$this->relationSearch = true;
        $this->searchFields = "name,phone";
        $this->model = new \app\admin\model\ActiveBmlist;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $labor_id = $this->request->get("labor_id", 0);//显示当前工作所属劳务机构的已签合同的劳务工

            $list = $this->model
                //->with('active')
                ->where(['labor_id'=>$labor_id,'status'=>2])
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
            foreach ($list as $key => &$val) {//加入当前工作任务ID
                $val['work_id'] = $ids;
            }
               
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        $this->view->assign('work_id',$ids);
        return $this->view->fetch();
    }

    //确认选择
    public function sure_select($ids,$work_id){
        if (!$work_id) {
            $this->error('请选择对应工作任务'); 
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            $company_id = db('recruit_work')->where('id='.$work_id)->value('company_id');
            $result = false;
            Db::startTrans();
            //$rs = db('active_bmlist')->where('id', 'in', $ids)->update(['status'=>3,'work_id'=>$work_id,'work_time'=>time()]);
            $list = db('active_bmlist')->where('id', 'in', $ids)->select();
            $recodList = [];
            try {
                foreach ($list as $key => $val) {
                    $result = db('active_bmlist')->where('id='.$val['id'])->update(['status'=>3,'work_id'=>$work_id,'work_time'=>time()]);
                    $recodList[$key] = [
                        'user_id'         => $val['user_id'],
                        'bm_id'           => $val['id'],
                        'agent_user_id'   => $val['agent_user_id'],
                        'company_id'      => $company_id,
                        'name'            => $val['name'],
                        'phone'           => $val['phone'],
                        'work_id'         => $work_id,
                        'city_id'         => $val['city_id'],
                        'service_id'      => $val['service_id'],
                        'labor_id'        => $val['labor_id'],
                        'work_time'       => time()
                    ];
                }
                $model = new \app\admin\model\WorkRecod; 
                $result = $model->allowField(true)->saveAll($recodList);
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }

            if ($result) {
                $this->success('操作成功');
            }else{
                $this->success('操作失败');
            }
            
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));      
    }

    /**
     * 查看挂钩人员
     */
    public function work_list()
    {
        //当前是否为关联查询
        //$this->relationSearch = true;
        $this->model = new \app\admin\model\ActiveBmlist;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $work_id = $this->request->get("work_id", 0);//显示当前工作所属劳务机构的已签合同的劳务工

            $list = $this->model
                //->with('active')
                ->where(['work_id'=>$work_id,'status'=>3])
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);
               
            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    //解绑
    public function unbind($ids){
        
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            Db::startTrans();
            //$rs = db('active_bmlist')->where('id', 'in', $ids)->update(['status'=>2,'unbind_time'=>time()]);
            $list = db('active_bmlist')->where('id', 'in', $ids)->select();
            try {
                foreach ($list as $key => $val) {
                    $rs = db('active_bmlist')->where('id='.$val['id'])->update(['status'=>2,'unbind_time'=>time()]);
                    $rs2 = db('work_recod')->where(['user_id'=>$val['user_id'],'bm_id'=>$val['id']])->order('id desc')->limit(1)->update(['unbind_time'=>time()]);
                }
                Db::commit();
            } catch (PDOException $e) {
                Db::rollback();
                $this->error($e->getMessage());
            } catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            
            if ($rs) {
                $this->success('操作成功');
            }else{
                $this->error('操作失败');
            }
            
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));      
    }
    

}
