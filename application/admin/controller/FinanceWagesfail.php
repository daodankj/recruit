<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class FinanceWagesfail extends Backend
{
    
    /**
     * FinanceWagesfail模型对象
     * @var \app\admin\model\FinanceWagesfail
     */
    protected $noNeedLogin = '';
    protected $noNeedRight = 'index';
    protected $model = null;
    protected $searchFields = 'name,idcard';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\FinanceWagesfail;

    }

    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->where($where_m)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function import()
    {
        parent::import();
    }

    
    

}
