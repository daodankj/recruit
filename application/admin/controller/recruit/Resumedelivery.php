<?php

namespace app\admin\controller\recruit;

use app\common\controller\Backend;

/**
 * 简历投递管理
 *
 * @icon fa fa-circle-o
 */
class Resumedelivery extends Backend
{

    /**
     * Resumedelivery模型对象
     * @var \app\admin\model\Resumedelivery
     */
    protected $model = null;
    protected $searchFields = 're_name,re_tel';//默认搜索字段

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('Resumedelivery');

    }




    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//除了管理员和总公司外
                $where_m['job.city_id'] = $this->auth->city_id;
            }else if($this->auth->type==4){//企业查看自己的
                $where_m['job.c_id'] = $this->auth->company_id;
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->with(['user', 'job', 'resume'])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['user', 'job', 'resume'])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $row) {
                $row->getRelation('user')->visible(['username', 'nickname']);
                $row->getRelation('job')->visible(['name']);
                $row->getRelation('resume')->visible(['name', 'tel']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    public function detail($ids, $re_id, $re_tel, $job_id, $user_id)
    {
        $row = \app\admin\model\Resume::get(['id' => $re_id]);
        if (!$row) {
            $this->error(__('No Results were found'), '');
            return;
        }

        $this->view->assign("row", $row->toArray());
        return $this->view->fetch();
    }
}
