<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use Exception;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
/**
 * 渠道人员库
 *
 * @icon fa fa-circle-o
 */
class ChannelUser extends Backend
{
    
    /**
     * ChannelUser模型对象
     * @var \app\admin\model\ChannelUser
     */
    protected $model = null;
    protected $searchFields = 'name,phone,idcard';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\ChannelUser;

    }


    /**
     * 导入
     */
    public function import()
    {
        
        $file = $this->request->request('file');
        if (!$file) {
            $this->error(__('Parameter %s can not be empty', 'file'));
        }
        $filePath = ROOT_PATH . DS . 'public' . DS . $file;
        if (!is_file($filePath)) {
            $this->error(__('No results were found'));
        }
        //实例化reader
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        if (!in_array($ext, ['csv', 'xls', 'xlsx'])) {
            $this->error(__('Unknown data format'));
        }
        if ($ext === 'csv') {
            $file = fopen($filePath, 'r');
            $filePath = tempnam(sys_get_temp_dir(), 'import_csv');
            $fp = fopen($filePath, "w");
            $n = 0;
            while ($line = fgets($file)) {
                $line = rtrim($line, "\n\r\0");
                $encoding = mb_detect_encoding($line, ['utf-8', 'gbk', 'latin1', 'big5']);
                if ($encoding != 'utf-8') {
                    $line = mb_convert_encoding($line, 'utf-8', $encoding);
                }
                if ($n == 0 || preg_match('/^".*"$/', $line)) {
                    fwrite($fp, $line . "\n");
                } else {
                    fwrite($fp, '"' . str_replace(['"', ','], ['""', '","'], $line) . "\"\n");
                }
                $n++;
            }
            fclose($file) || fclose($fp);

            $reader = new Csv();
        } elseif ($ext === 'xls') {
            $reader = new Xls();
        } else {
            $reader = new Xlsx();
        }


        //加载文件
        $insert = [];
        try {
            if (!$PHPExcel = $reader->load($filePath)) {
                $this->error(__('Unknown data format'));
            }
            $currentSheet = $PHPExcel->getSheet(0);  //读取文件中的第一个工作表
            $allColumn = $currentSheet->getHighestDataColumn(); //取得最大的列号
            $allRow = $currentSheet->getHighestRow(); //取得一共有多少行
            $maxColumnNumber = Coordinate::columnIndexFromString($allColumn);
            /*$fields = [];
            for ($currentRow = 1; $currentRow <= 1; $currentRow++) {
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $fields[] = $val;
                }
            }*/
            for ($currentRow = 3; $currentRow <= $allRow; $currentRow++) {
                $values = [];
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $values[] = is_null($val) ? '' : $val;
                }
                /*$row = [];
                $temp = array_combine($fields, $values);
                foreach ($temp as $k => $v) {
                    if (isset($fieldArr[$k]) && $k !== '') {
                        $row[$fieldArr[$k]] = $v;
                    }
                }
                if ($row) {
                    $insert[] = $row;
                }*/
                //----
                if ($values) {
                    $insert[] = $values;
                }
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
        if (!$insert) {
            $this->error(__('No rows were updated'));
        }
        //print_r($insert);exit;
        $insertData = [];
        $i = 0;
        try {
            foreach ($insert as $val) {
                if ($val[1] == '') {
                    continue;
                }
                $insertData[$i]['name'] = $val[1]?$val[1]:'';
                $insertData[$i]['sex'] = $val[2]?$val[2]:'';
                $insertData[$i]['phone'] = $val[3]?$val[3]:'';
                $insertData[$i]['idcard'] = $val[4]?$val[4]:'';
                $insertData[$i]['source'] = $val[5]?$val[5]:'';
                $insertData[$i]['link_time'] = $val[6]?$this->formatExcelDate($val[6]):0;
                $insertData[$i]['status'] = $val[7]?$val[7]:'';
                $insertData[$i]['work_company'] = $val[8]?$val[8]:'';
                $insertData[$i]['work_type'] = $val[9]?$val[9]:'';
                $insertData[$i]['induction_time'] = $val[10]?$this->formatExcelDate($val[10]):0;
                $insertData[$i]['leave_time'] = $val[11]?$this->formatExcelDate($val[11]):0;
                $i++;
            }
            $this->model->allowField(true)->saveAll($insertData);
        } catch (PDOException $exception) {
            $msg = $exception->getMessage();
            if (preg_match("/.+Integrity constraint violation: 1062 Duplicate entry '(.+)' for key '(.+)'/is", $msg, $matches)) {
                $msg = "导入失败，包含【{$matches[1]}】的记录已存在";
            };
            $this->error($msg);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success();
    }

    private function formatExcelDate($day){
        if (!$day) {
            return 0;
        }
        //$startDate = strtotime('1900-01-01');
        //$daytime = strtotime('+'.$day.' day',$startDate);
        //excel默认是从1900-01-01作为开始计算日期
        //计算机默认是从1970-01-01 08:00:00 作为开始计算日期
        //两者之间相差70年, 共计25569天, 所以用excel的数字减去25569得到天数,
        $day = intval($day);
        return date('Y-m-d',($day-25569)*24*60*60);
    }

    

}
