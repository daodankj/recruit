<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 团长管理
 *
 * @icon fa fa-circle-o
 */
class RecruitHead extends Backend
{
    
    /**
     * RecruitHead模型对象
     * @var \app\admin\model\RecruitHead
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\RecruitHead;

    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type!=1) {//除了管理员和总公司外
                $where_m['city_id'] = $this->auth->city_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['city_id'] = $this->auth->city_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------

            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            
            $list = $this->model
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function import()
    {
        parent::import();
    }

    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该记录不存在，审核失败'.$id);
            }
            if ($info->status!=0) {
                $this->success('改记录已被审核，不用重新审核');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_admin_id = $this->auth->id;
            $info->check_admin_name = $this->auth->nickname;
            $info->check_time = time();
            if ($info->save()) {
                //消息通知
                //监听审核事件
                $infodata = ['status'=>$status,'type'=>1,'data'=>$info];
                \think\Hook::listen("apply_check", $infodata);
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    

}
