<?php

namespace app\admin\controller;

use app\common\controller\Backend;

use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
/**
 * 劳务机构
 *
 * @icon fa fa-circle-o
 */
class RecruitLabor extends Backend
{
    
    /**
     * RecruitLabor模型对象
     * @var \app\admin\model\RecruitLabor
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\RecruitLabor;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type!=1) {//除了管理员和总公司外
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------

            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            
            $list = $this->model
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 批量更新
     */
    public function multi($ids = "")
    {
        if (!$this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ? $ids : $this->request->post("ids");
        if ($ids) {
            if ($this->request->has('params')) {
                parse_str($this->request->post("params"), $values);
                $values = $this->auth->isSuperAdmin() ? $values : array_intersect_key($values, array_flip(is_array($this->multiFields) ? $this->multiFields : explode(',', $this->multiFields)));
                if ($values) {
                    $adminIds = $this->getDataLimitAdminIds();
                    if (is_array($adminIds)) {
                        $this->model->where($this->dataLimitField, 'in', $adminIds);
                    }
                    $count = 0;
                    Db::startTrans();
                    try {
                        $list = $this->model->where($this->model->getPk(), 'in', $ids)->select();
                        foreach ($list as $index => $item) {
                            $count += $item->allowField(true)->isUpdate(true)->save($values);
                            //关闭和开启对应管理员
                            if ($values['status']==1) {
                                db('admin')->where(['labor_id'=>$item->id])->update(['type'=>3,'status'=>'normal']);
                            }else{
                                db('admin')->where(['labor_id'=>$item->id])->update(['type'=>3,'status'=>'hidden']);
                            }
                        }
                        Db::commit();
                    } catch (PDOException $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    } catch (Exception $e) {
                        Db::rollback();
                        $this->error($e->getMessage());
                    }
                    if ($count) {
                        $this->success();
                    } else {
                        $this->error(__('No rows were updated'));
                    }
                } else {
                    $this->error(__('You have no permission'));
                }
            }
        }
        $this->error(__('Parameter %s can not be empty', 'ids'));
    }
    

}
