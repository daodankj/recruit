<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use tools\TencentEss;
use think\Response;

/**
 * 合同管理
 *
 * @icon fa fa-circle-o
 */
class Contract extends Backend
{
    
    /**
     * Contract模型对象
     * @var \app\admin\model\Contract
     */
    protected $model = null;
    protected $noNeedRight = ['getQrimg'];
    protected $searchFields = 'name,mobile';//默认搜索字段

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Contract;

    }

    /**
     * 合同管理
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->where($where_m)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    public function import()
    {
        parent::import();
    }

    public function show_contract($ids){
        $info = $this->model->where('id='.$ids)->find();
        $data = [
            'flowId' => $info->flow_id,
            'pathType' => 1
        ];
        $status = 1;
        $error = '';
        $url = '';
        $tencentEss = new TencentEss;
        try {
            $res = $tencentEss->createSchemeUrl($data);
            $res = json_decode($res,true);
            $url = base64_encode($res['SchemeUrl']);
        } catch (\Exception $e) {
            $status = 0;
            $error = $e->getMessage();
        }
        $this->view->assign('status',$status);
        $this->view->assign('error',$error);
        $this->view->assign('url',$url);
        return $this->view->fetch();
    }


    public function getQrimg($url){
        $config = get_addon_config('qrcode');
        $params = $this->request->get();
        $params = array_intersect_key($params, array_flip(['text', 'size', 'padding', 'errorlevel', 'foreground', 'background', 'logo', 'logosize', 'logopath', 'label', 'labelfontsize', 'labelalignment']));

        $params['text'] = base64_decode($url);
        $params['label'] = $this->request->get('label', $config['label'], 'trim');

        $qrCode = \addons\qrcode\library\Service::qrcode($params);

        $mimetype = $config['format'] == 'png' ? 'image/png' : 'image/svg+xml';

        $response = Response::create()->header("Content-Type", $mimetype);

        // 直接显示二维码
        header('Content-Type: ' . $qrCode->getContentType());
        $response->content($qrCode->writeString());

        return $response;
    }

}
