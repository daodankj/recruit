<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 打卡签到记录
 *
 * @icon fa fa-circle-o
 */
class Daka extends Backend
{
    
    /**
     * Daka模型对象
     * @var \app\admin\model\Daka
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Daka;

    }

    public function import()
    {
        parent::import();
    }

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['work.service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                //$custom['service_id'] = $this->auth->service_id;
                //$this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['work.labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                //$custom = (array)$this->request->request("custom/a");
                //$custom['labor_id'] = $this->auth->labor_id;
                //$this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->with(['work', 'user'])
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with(['work', 'user'])
                ->where($where)
                ->where($where_m)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            foreach ($list as $row) {
                $row->visible(['id', 'date', 'type','price', 'in_time', 'out_time','image','remark','work_time']);
                $row->visible(['work']);
                $row->getRelation('work')->visible(['name']);
                $row->visible(['user']);
                $row->getRelation('user')->visible(['username', 'nickname']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    

}
