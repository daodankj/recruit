<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use Exception;
use think\exception\PDOException;
use think\exception\ValidateException;
use fast\Random;

/**
 * 招募活动报名
 *
 * @icon fa fa-circle-o
 */
class ActiveBmlist extends Backend
{
    
    /**
     * ActiveBmlist模型对象
     * @var \app\admin\model\ActiveBmlist
     */
    protected $model = null;
    protected $searchFields = 'name,phone';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\ActiveBmlist;

    }

    /**
     * 劳务工档案
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m = [];
            if ($this->auth->type==2) {//服务商
                $where_m['service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->with(['user'])
                ->where($where_m)
                ->where($where)
                ->where(function ($query) {//过滤未入职的,但现实已离职的
                    $query->where('active_bmlist.status', '>', 0)->whereOr('leave_time', '>', 0);
                })
                ->order($sort, $order)
                ->paginate($limit);

            foreach ($list as $row) {
                $row->visible(['id','user_id','name', 'phone','idcard', 'status', 'induction_time', 'contract_time']);
                //$row->visible(['active']);
                //$row->getRelation('active')->visible(['name','labor_id']);
                $row->visible(['user']);
                $row->getRelation('user')->visible(['id','username', 'nickname','mobile','avatar','gender','level','score','status']);
            }

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                if (!$params['user_id']) {//如果没关联用户，用手机号码注册用户
                    $user_id = db('user')->where(['mobile'=>$params['phone']])->value('id');
                    if ($user_id) {
                        $this->error('该手机号码已经注册会员，请在关联用户处选择该用户！');
                    }
                    //注册会员 
                    $salt = Random::alnum();
                    $password = $params['phone'];
                    $udata = [
                        'group_id'   => 1,
                        'username'   => $params['name'],
                        'nickname'   => $params['name'],
                        'mobile'     => $params['phone'],
                        'idcard'     => $params['idcard'],
                        'createtime' => time(),
                        'jointime' => time(),
                        'salt'       => $salt,
                        'password'   => md5(md5($password) . $salt),
                        'status'     => 'normal'
                    ];
                    $rs = db('user')->insert($udata);
                    if (!$rs) {
                        $this->error('会员自动注册失败！');
                    }
                    $params['user_id'] = db('user')->getLastInsID();//insertGetId
                }
                //判断用户是否已经存在档案
                $info = $this->model->where('status>-1 and user_id='.$params['user_id'])->find();
                if ($info) {
                    $this->error('该档案已经存在，不能重复添加');
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 报名查看
     */
    public function worklist()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //--------------数据过滤start-------------
            $where_m['active_id'] = ['>',0];
            if ($this->auth->type==2) {//服务商
                $where_m['active.service_id'] = $this->auth->service_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['active.service_id'] = $this->auth->service_id;
                $this->request->request(['custom'=>$custom]);
            }else if($this->auth->type==3){//机构查看自己的
                $where_m['active.labor_id'] = $this->auth->labor_id;
                //Selectpage过滤
                $custom = (array)$this->request->request("custom/a");
                $custom['active.labor_id'] = $this->auth->labor_id;
                $this->request->request(['custom'=>$custom]);
            }
            //--------------数据过滤end-------------
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $list = $this->model
                ->with('active')
                ->where($where_m)
                ->where($where)
                ->order($sort, $order)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }

    //查看合同
    public function contract($ids){
        $info = $this->model->where('id='.$ids)->find();
        //获取合同模板文件
        $contract_tpl = config('site.contract_tpl');
        $contract_tpl = str_replace("{labor_name}",$info->labor_name,$contract_tpl);
        $contract_tpl = str_replace("{username}",$info->name,$contract_tpl);
        $contract_tpl = str_replace("{phone}",$info->phone,$contract_tpl);

        $this->view->assign('contract_tpl',$contract_tpl);
        $this->view->assign('url',$info->c_url);
        return $this->view->fetch();
    }

    //离职
    public function leave($ids){
        $info = $this->model->where('id='.$ids)->find();
        if (!$info || $info->status==3) {
            $this->error('工作中，无法离职，请先解绑工作');
        }
        $info->status = -1;
        $info->leave_time = time();
        $rs = $info->save();
        if ($rs) {
            $this->success('操作成功');
        }else $this->error('操作失败');
    }

    
    

}
