<?php

namespace app\admin\model;

use think\Model;


class RecruitHead extends Model
{

    

    

    // 表名
    protected $name = 'recruit_head';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'city_name',
        'user_name'
    ];
    

    protected function getCityNameAttr($value,$data){
        $name = db('recruit_opencity')->where(['id'=>$data['city_id']])->value('city');

        return $name?$name:$data['city_id'];
    }

    protected function getUserNameAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('username');

        return $name?$name:$data['user_id'];
    }





}
