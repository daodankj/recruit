<?php

namespace app\admin\model;

use think\Model;


class FinanceWages extends Model
{

    

    

    // 表名
    protected $name = 'finance_wages';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'service_name',
        'labor_name',
        'user_name'
    ];
    

    



    //根据所属劳务机构修改所属城市
    public function setCityIdAttr($value,$data){
        $city_id = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('city_id');

        return $city_id?$city_id:0;
    }
    //根据所属劳务机构修改所属服务商
    public function setServiceIdAttr($value,$data){
        $service_id = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('service_id');

        return $service_id?$service_id:0;
    }

    protected function getServiceNameAttr($value,$data){
        $name = db('recruit_service')->where(['id'=>$data['service_id']])->value('name');

        return $name?$name:$data['service_id'];
    }
    
    protected function getLaborNameAttr($value,$data){
        $name = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('name');

        return $name?$name:$data['labor_id'];
    }

    protected function getUserNameAttr($value,$data){
        $name = db('user')->where('id='.$data['user_id'])->value('username');

        return $name?$name:$data['user_id'];
    }


}
