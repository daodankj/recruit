<?php

namespace app\admin\model;

use think\Model;


class RecruitLabor extends Model
{

    

    

    // 表名
    protected $name = 'recruit_labor';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'city_name',
        'service_name'
    ];

    //根据所属服务商修改所属城市
    public function setCityIdAttr($value,$data){
        $city_id = db('recruit_service')->where(['id'=>$data['service_id']])->value('city_id');

        return $city_id?$city_id:0;
    }
    

    protected function getCityNameAttr($value,$data){
        $name = db('recruit_opencity')->where(['id'=>$data['city_id']])->value('city');

        return $name?$name:$data['city_id'];
    }

    protected function getServiceNameAttr($value,$data){
        $name = db('recruit_service')->where(['id'=>$data['service_id']])->value('name');

        return $name?$name:$data['service_id'];
    }





}
