<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class SxJob extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'sx_job';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'labor_name',
        'bm_num'
    ];
    
    protected function getLaborNameAttr($value,$data){
        $name = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('name');

        return $name?$name:$data['labor_id'];
    }

    protected function getBmNumAttr($value,$data){
        $num  = db('active_bmlist')->where('status>-1 and active_id='.$data['id'])->count();

        return $num;
    }





}
