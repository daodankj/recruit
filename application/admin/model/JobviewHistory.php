<?php

namespace app\admin\model;

use think\Model;


class JobviewHistory extends Model
{

    

    

    // 表名
    protected $name = 'jobview_history';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    
    public function job()
    {
        return $this->belongsTo('Job', 'job_id', 'Id', [], 'LEFT')->setEagerlyType(0);
    }


}
