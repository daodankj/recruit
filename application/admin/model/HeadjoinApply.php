<?php

namespace app\admin\model;

use think\Model;


class HeadjoinApply extends Model
{

    

    

    // 表名
    protected $name = 'headjoin_apply';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'user_name'
    ];
    




    protected function getUserNameAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('username');

        return $name?$name:$data['user_id'];
    }

    public function active()
    {
        return $this->belongsTo('recruitActive', 'active_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
