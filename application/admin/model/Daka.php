<?php

namespace app\admin\model;

use think\Model;


class Daka extends Model
{

    

    

    // 表名
    protected $name = 'daka';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'in_time_text',
        'out_time_text',
        'agent_name'
    ];
    

    



    public function getInTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['in_time']) ? $data['in_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getOutTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['out_time']) ? $data['out_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setInTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setOutTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function getAgentNameAttr($value,$data){
        $name = db('user')->where('id='.$data['agent_user_id'])->value('username');

        return $name?$name:$data['agent_user_id'];
    }

    public function work()
    {
        return $this->belongsTo('recruit_work', 'work_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
