<?php

namespace app\admin\model;

use think\Model;


class WorkRecod extends Model
{

    

    

    // 表名
    protected $name = 'work_recod';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'work_time_text',
        'unbind_time_text',
        'labor_name',
        'company_name',
        'work_name'
    ];
    

    
    protected function getLaborNameAttr($value,$data){
        $name = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('name');

        return $name?$name:$data['labor_id'];
    }

    protected function getCompanyNameAttr($value,$data){
        $name = db('recruit_company')->where(['id'=>$data['company_id']])->value('name');

        return $name?$name:$data['company_id'];
    }

    protected function getWorkNameAttr($value,$data){
        $name = db('recruit_work')->where(['id'=>$data['work_id']])->value('name');

        return $name?$name:$data['work_id'];
    }


    public function getWorkTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['work_time']) ? $data['work_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getUnbindTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['unbind_time']) ? $data['unbind_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setWorkTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setUnbindTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
