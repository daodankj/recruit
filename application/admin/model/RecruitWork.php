<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class RecruitWork extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'recruit_work';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'start_time_text',
        'end_time_text',
        'service_name',
        'labor_name',
        'company_name'
    ];
    

    

    //根据所属劳务机构修改所属城市
    public function setCityIdAttr($value,$data){
        $city_id = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('city_id');

        return $city_id?$city_id:0;
    }
    //根据所属劳务机构修改所属服务商
    public function setServiceIdAttr($value,$data){
        $service_id = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('service_id');

        return $service_id?$service_id:0;
    }

    protected function getServiceNameAttr($value,$data){
        $name = db('recruit_service')->where(['id'=>$data['service_id']])->value('name');

        return $name?$name:$data['service_id'];
    }
    
    protected function getLaborNameAttr($value,$data){
        $name = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('name');

        return $name?$name:$data['labor_id'];
    }

    protected function getCompanyNameAttr($value,$data){
        $name = db('recruit_company')->where(['id'=>$data['company_id']])->value('name');

        return $name?$name:$data['company_id'];
    }

    public function getStartTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['start_time']) ? $data['start_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getEndTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['end_time']) ? $data['end_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setStartTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setEndTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
