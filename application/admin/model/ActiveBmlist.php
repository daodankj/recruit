<?php

namespace app\admin\model;

use think\Model;


class ActiveBmlist extends Model
{

    

    

    // 表名
    protected $name = 'active_bmlist';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'agent_name',
        'service_name',
        'labor_name'
    ];

    //根据所属劳务机构修改所属城市
    public function setCityIdAttr($value,$data){
        $city_id = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('city_id');

        return $city_id?$city_id:0;
    }
    //根据所属劳务机构修改所属服务商
    public function setServiceIdAttr($value,$data){
        $service_id = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('service_id');

        return $service_id?$service_id:0;
    }

    protected function getServiceNameAttr($value,$data){
        $name = db('recruit_service')->where(['id'=>$data['service_id']])->value('name');

        return $name?$name:$data['service_id'];
    }
    
    protected function getLaborNameAttr($value,$data){
        $name = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('name');

        return $name?$name:$data['labor_id'];
    }
    
    protected function getAgentNameAttr($value,$data){
        $name = db('user')->where('id='.$data['agent_user_id'])->value('username');
        $atype = $data['agent_type']==1?'(推广员)':'(团长)';
        return $name?$name.$atype:'无';
    }
    
    public function active()
    {
        return $this->belongsTo('recruitActive', 'active_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

    public function user()
    {
        return $this->belongsTo("User", 'user_id', 'id')->setEagerlyType(0);
    }




}
