<?php

namespace app\admin\model;

use think\Model;
use traits\model\SoftDelete;

class RecruitActive extends Model
{

    use SoftDelete;

    

    // 表名
    protected $name = 'recruit_active';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = 'deletetime';

    // 追加属性
    protected $append = [
        'labor_name',
        'active_status',
        'bm_num'
    ];
    

    protected function getLaborNameAttr($value,$data){
        $name = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('name');

        return $name?$name:$data['labor_id'];
    }

    protected function getActiveStatusAttr($value,$data){
        $start_time = strtotime($data['start_date']);
        $end_time = strtotime($data['end_date']);
        $now_time = time();
        if ($now_time<$start_time) {
            return '<font color="#999;">未开始</font>';
        }elseif($now_time>$end_time){
            return '<font color="red">已结束</font>';
        }else{
            return '<font color="green">进行中</font>';
        }
    }

    protected function getBmNumAttr($value,$data){
        $num  = db('active_bmlist')->where('status>-1 and active_id='.$data['id'])->count();

        return $num;
    }



}
