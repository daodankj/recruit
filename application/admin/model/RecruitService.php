<?php

namespace app\admin\model;

use think\Model;


class RecruitService extends Model
{

    

    

    // 表名
    protected $name = 'recruit_service';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'city_name'
    ];
    

    protected function getCityNameAttr($value,$data){
        $name = db('recruit_opencity')->where(['id'=>$data['city_id']])->value('city');

        return $name?$name:$data['city_id'];
    }







}
