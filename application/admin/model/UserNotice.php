<?php

namespace app\admin\model;

use think\Model;


class UserNotice extends Model
{

    

    

    // 表名
    protected $name = 'user_notice';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_name',
        'createtime_text'
    ];
    
    public function getTypeNameAttr($value,$data){
        $typeName = [
            '1' => '团长申请审核通知',
            '2' => '参团申请审核通知',
            '3' => '企业入驻申请审核通知',
            '4' => '招募活动发布审核通知',
            '5' => '实名认证申请审核通知'
        ];
        return isset($typeName[$data['type']])?$typeName[$data['type']]:'';
    }

    public function getCreatetimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['createtime']) ? $data['createtime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }
    







}
