<?php

namespace app\admin\model;

use think\Model;


class Advance extends Model
{

    

    

    // 表名
    protected $name = 'advance';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'service_name',
        'labor_name',
        'agent_name'
    ];
    
    protected function getServiceNameAttr($value,$data){
        $name = db('recruit_service')->where(['id'=>$data['service_id']])->value('name');

        return $name?$name:$data['service_id'];
    }
    
    protected function getLaborNameAttr($value,$data){
        $name = db('recruit_labor')->where(['id'=>$data['labor_id']])->value('name');

        return $name?$name:$data['labor_id'];
    }
    

    protected function getAgentNameAttr($value,$data){
        $name = db('user')->where('id='.$data['agent_user_id'])->value('username');

        return $name?$name:'无';
    }





}
