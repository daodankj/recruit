<?php

return [
    'Image'       => '图片',
    'Name'        => '培训名称',
    'Address'     => '地址',
    'Start_time'  => '培训时间',
    'Price'       => '报名费用',
    'Intro'       => '内容简介',
    'Content'     => '内容详情',
    'Create_time' => '创建时间'
];
