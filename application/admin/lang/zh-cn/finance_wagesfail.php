<?php

return [
    'City_id'    => '所属城市',
    'Service_id' => '所属区域服务商',
    'Labor_id'   => '所属劳务机构',
    'Day'        => '日期',
    'Name'       => '姓名',
    'Idcard'     => '身份证号码',
    'Remark'     => '备注',
    'Createtime' => '导入时间'
];
