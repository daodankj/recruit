<?php

return [
    'Train_id'    => '技能培训',
    'User_id'     => '所属用户',
    'User_name'   => '姓名',
    'Phone'       => '电话',
    'Status'      => '状态',
    'Create_time' => '报名时间'
];
