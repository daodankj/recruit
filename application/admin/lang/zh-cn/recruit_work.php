<?php

return [
    'Name'       => '工作任务名称',
    'City_id'    => '所属城市',
    'Service_id' => '所属区域服务商',
    'Labor_id'   => '所属劳务机构',
    'Company_id' => '工作企业',
    'Start_time' => '工作开始时间',
    'End_time'   => '工作结束时间',
    'Price'      => '价格',
    'Content'    => '工作内容',
    'Lat'        => '纬度',
    'Lng'        => '经度',
    'Address'    => '打卡地址'
];
