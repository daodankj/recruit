<?php

return [
    'User_id'    => '关联用户',
    'Idcard'     => '身份证号码',
    'City_id'    => '所属城市',
    'Service_id' => '所属服务商',
    'Labor_id'   => '所属劳务机构',
    'Createtime' => '创建时间',
    'Status'     => '状态'
];
