<?php

return [
    'Name'           => '姓名',
    'Sex'            => '性别',
    'Phone'          => '电话',
    'Idcard'         => '身份证',
    'Source'         => '来源',
    'Link_time'      => '联系时间',
    'Status'         => '状态',
    'Work_company'   => '工作单位',
    'Work_type'      => '工种',
    'Induction_time' => '入职时间',
    'Leave_time'     => '离职时间'
];
