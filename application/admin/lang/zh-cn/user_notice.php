<?php

return [
    'User_id'    => '所属用户',
    'Type'       => '类型',
    'Obj_id'     => '对应记录',
    'Notice'     => '消息内容',
    'Is_read'    => '是否已读',
    'Createtime' => '通知时间'
];
