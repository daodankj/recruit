<?php

return [
    'Year'        => '所属年份',
    'Month'       => '所属月份',
    'User_id'     => '所属人',
    'City_id'     => '所属城市',
    'Service_id'  => '所属区域服务商',
    'Labor_id'    => '所属劳务机构',
    'Total_time'  => '总工时',
    'Total_money' => '总工资',
    'Send_money'  => '实发工资',
    'Status'      => '状态',
    'Remark'      => '备注',
    'Createtime'  => '创建时间'
];
