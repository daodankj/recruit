<?php

return [
    'Job_id'      => '职位',
    'User_id'     => '所属用户',
    'User_name'   => '姓名',
    'Phone'       => '电话',
    'Create_time' => '报名时间'
];
