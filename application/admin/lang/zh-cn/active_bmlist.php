<?php

return [
    'Active_id'     => '所属活动',
    'User_id'       => '用户',
    'Agent_user_id' => '推广员',
    'Name'          => '姓名',
    'Phone'         => '联系电话',
    'Createtime'    => '报名时间',
    'Status'        => '状态'
];
