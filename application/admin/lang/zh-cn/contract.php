<?php

return [
    'Flow_id'     => '流程id',
    'Document_id' => '文档id',
    'Bm_id'       => '员工id',
    'Name'        => '姓名',
    'Mobile'      => '电话',
    'Price'       => '白班工时价',
    'Price2'      => '晚班工时价'
];
