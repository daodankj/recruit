<?php

return [
    'Name'            => '活动名称',
    'Type'            => '招募类型',
    'User_num'        => '招募人数',
    'Settlement_type' => '结算方式',
    'Head_open'       => '是否开启团长招募',
    'Content'         => '内容',
    'Createtime'      => '添加时间',
    'Updatetime'      => '更新时间',
    'Status'          => '状态',
    'User_type'       => '发布人类型',
    'User_id'         => '发布推广员',
    'Admin_id'        => '发布管理员'
];
