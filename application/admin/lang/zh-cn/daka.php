<?php

return [
    'Active_id'     => '所属活动',
    'Bm_id'         => '所属合同',
    'User_id'       => '打卡用户',
    'Agent_user_id' => '所属推广员',
    'Price'         => '工时价',
    'Date'          => '打卡日期',
    'In_time'       => '签到时间',
    'Out_time'      => '签退时间',
    'Image'         => '拍照图片',
    'Remark'        => '备注'
];
