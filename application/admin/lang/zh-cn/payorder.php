<?php

return [
    'User_id'      => '用户',
    'Out_trade_no' => '订单号',
    'Amount'       => '金额',
    'Prepay_id'    => '微信预支付id',
    'Status'       => '状态'
];
