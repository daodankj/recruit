<?php

return [
    'User_id'    => '所属用户',
    'Username'   => '用户名称',
    'Idcard'     => '身份证号码',
    'Service_id' => '所属服务商',
    'Labor_id'   => '所属劳务机构',
    'Money'      => '金额',
    'Createtime' => '创建时间',
    'Status'     => '状态'
];
