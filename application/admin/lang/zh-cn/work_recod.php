<?php

return [
    'User_id'       => '用户',
    'Bm_id'         => '合同',
    'Company_id'    => '企业',
    'Agent_user_id' => '推广员',
    'Name'          => '姓名',
    'Phone'         => '联系电话',
    'Work_id'       => '当前工作任务',
    'City_id'       => '所属城市',
    'Service_id'    => '所属区域服务商',
    'Labor_id'      => '所属劳务机构',
    'Work_time'     => '加入工作时间',
    'Unbind_time'   => '解绑时间'
];
