<?php

return [
    'Invite_id'  => '邀请用户',
    'User_id'    => '报名用户',
    'Name'       => '姓名',
    'Phone'      => '电话',
    'Gold'       => '薪资范围',
    'Job'        => '岗位',
    'Createtime' => '上报时间'
];
