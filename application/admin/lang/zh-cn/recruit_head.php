<?php

return [
    'User_id'            => '关联会员',
    'Name'               => '姓名',
    'Idcard'             => '身份证',
    'Phone'              => '手机号码',
    'Address'            => '详细地址',
    'City_id'            => '所属区域',
    'Idcard_img_front'   => '身份证照片正面',
    'Idcard_ing_bankend' => '身份证照片反面',
    'Createtime'         => '创建时间',
    'Status'             => '审核状态'
];
