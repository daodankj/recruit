<?php

return [
    'Name'            => '活动名称',
    'User_num'        => '招募人数',
    'Settlement_type' => '结算方式',
    'Content'         => '内容',
    'Createtime'      => '添加时间',
    'Updatetime'      => '更新时间',
    'Status'          => '状态',
    'Admin_id'        => '发布管理员',
    'City_id'         => '所属城市',
    'Service_id'      => '所属区域服务商',
    'Labor_id'        => '所属劳务机构',
    'Price'           => '工资/小时'
];
