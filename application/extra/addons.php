<?php

return [
    'autoload' => false,
    'hooks' => [
        'sms_send' => [
            'alisms',
        ],
        'sms_notice' => [
            'alisms',
        ],
        'sms_check' => [
            'alisms',
        ],
        'app_init' => [
            'leescore',
            'log',
            'qrcode',
        ],
        'leescorehook' => [
            'leescore',
        ],
        'user_sidenav_after' => [
            'leescore',
        ],
        'admin_login_init' => [
            'loginbg',
        ],
        'config_init' => [
            'nkeditor',
            'third',
        ],
        'testhook' => [
            'recruit',
        ],
    ],
    'route' => [
        '/leescoregoods$' => 'leescore/goods/index',
        '/leescoreorder$' => 'leescore/order/index',
        '/score$' => 'leescore/index/index',
        '/address$' => 'leescore/address/index',
        '/qrcode$' => 'qrcode/index/index',
        '/qrcode/build$' => 'qrcode/index/build',
        '/third$' => 'third/index/index',
        '/third/connect/[:platform]' => 'third/index/connect',
        '/third/callback/[:platform]' => 'third/index/callback',
        '/third/bind/[:platform]' => 'third/index/bind',
        '/third/unbind/[:platform]' => 'third/index/unbind',
    ],
    'priority' => [],
    'domain' => '',
];
