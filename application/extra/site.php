<?php

return array (
  'name' => '八戒求才平台',
  'beian' => '',
  'cdnurl' => '',
  'version' => '1.0.1',
  'timezone' => 'Asia/Shanghai',
  'forbiddenip' => '',
  'languages' => 
  array (
    'backend' => 'zh-cn',
    'frontend' => 'zh-cn',
  ),
  'fixedpage' => 'dashboard',
  'categorytype' => 
  array (
    'default' => 'Default',
    'page' => 'Page',
    'article' => 'Article',
    'test' => 'Test',
  ),
  'configgroup' => 
  array (
    'basic' => 'Basic',
    'email' => 'Email',
    'dictionary' => 'Dictionary',
    'user' => 'User',
    'example' => 'Example',
  ),
  'mail_type' => '1',
  'mail_smtp_host' => 'smtp.qq.com',
  'mail_smtp_port' => '465',
  'mail_smtp_user' => '10000',
  'mail_smtp_pass' => 'password',
  'mail_verify_type' => '2',
  'mail_from' => '10000@qq.com',
  'attachmentcategory' => 
  array (
    'category1' => 'Category1',
    'category2' => 'Category2',
    'custom' => 'Custom',
  ),
  'contract_tpl' => '<p class="MsoNormal" style="text-align:center;"><span style="font-size:21px;">劳务协议</span></p><p class="MsoNormal">甲方（劳务派遣单位）：<u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {labor_name}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</u></p><p class="MsoNormal">统一社会信用代码：<u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {company_no}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</u><u></u></p><p class="MsoNormal">劳务派遣许可证编号：<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><u></u></p><p class="MsoNormal">&nbsp;</p><p class="MsoNormal">乙方（劳动者）：<u>&nbsp; &nbsp;{username}</u><u>&nbsp; &nbsp; &nbsp;&nbsp;</u>身份证号码：<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><u>&nbsp;{idcard}</u><u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</u></p><p class="MsoNormal">户籍地址：<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><u>XXXXXX</u><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span>电话：</span>&nbsp; &nbsp; {phone}</u><u>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</u><u></u></p><p class="MsoNormal">经常居住地（通讯地址）：<u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><u>XXXXXXX</u><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><u></u></p><p class="MsoNormal">紧急联系电话：<u>&nbsp;&nbsp;</u><u>&nbsp;</u><u>XXXXXX</u><u>&nbsp;</u><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u><u></u></p><p class="MsoNormal"><span>根据《中华人民共和国劳动法》《中华人民共和国劳动合同法》等法律法规政策规定</span>,甲乙双方遵循合法、公平、平等自愿、协商一致、诚实信用的原则订立本合同。</p><p class="MsoNormal" style="text-indent:0px;"><span style="text-indent:30pt;">&nbsp; &nbsp; &nbsp; &nbsp;</span>一、劳动合同期限&nbsp;</p><p class="MsoNormal" style="margin-left:21.0000pt;">&nbsp; &nbsp; &nbsp;1.劳务期限：自<u>&nbsp;&nbsp;</u><u>年</u><u>/</u><u>月</u><u>/</u><u>日</u><u>&nbsp;</u><u>&nbsp;</u><u>&nbsp;&nbsp;</u>起至&nbsp;<u>年</u><u>/</u><u>月</u><u>/</u><u>日</u><u>&nbsp;&nbsp;&nbsp;</u><u>&nbsp;&nbsp;</u><u>日</u>或甲方项目到期止。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;二、工作内容和工作地点</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2.&nbsp;乙方同意由甲方派遣到&nbsp;<u> &nbsp; &nbsp; &nbsp; {company_name}</u><u>&nbsp; &nbsp; &nbsp;&nbsp;</u>有限公司 （用工单位名称）工作，工作地点为&nbsp;<u> {company_address}</u><u>&nbsp; &nbsp;</u>。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;3. 乙方同意在用工单位&nbsp;&nbsp;<u>&nbsp;&nbsp;</u><u>普通操作</u><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>岗位工作，属于临时性/辅助性/替代性工作岗位。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;三、劳动报酬和福利待遇</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;经甲方与用工单位商定，甲方采用以下方式向乙方以货币形式支付工资，于每月&nbsp;&nbsp;日前足额支付：</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;4.&nbsp;统一标准时薪：白班：<u>（</u><u>&nbsp;</u><u>x</u><u>&nbsp;</u><u>）</u>元/时 ，夜班：<u>（</u><u>&nbsp;x</u><u>&nbsp;</u><u>）</u>元/时（出勤未满1个月离职（含自己提出离职申请/消极怠工/不服从安排辞退/与同事争吵或纠纷等原因离职）扣减3元/时）。月收入超5000元由个人承担个人所得税；</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;5.&nbsp;入职3天内为双方体验期，未满3天或24小时出勤不管任何原因不结算工资。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;第四条 甲方不得苛扣劳动者的劳动报酬。乙方从甲方获得的工资依法缴纳个人所得税由甲方从其工资中代扣代缴。因甲乙双方属于不定时雇佣关系，甲方不为乙方申报除商业险以外的其他任何社保等其他保险。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;第五条 甲方未能安排乙方工作或被用工单位退回期间，乙方有权自行重新入职新的单位，甲方无权干涉，双方自动解除劳务关系。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;第六条&nbsp;本合同中记载的乙方联系电话、通讯地址为劳动合同期内通知相关事项和送达书面文书的联系方式、送达地址。如发生变化，乙方应当及时告知甲方。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;第七条 双方确认：均已详细阅读并理解本合同内容,清楚各自的权利、义务。本合同未尽事宜，按照有关法律法规和政策规定执行。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;第八条 本劳动合同一式（贰 ）份，双方至少各执（壹）份，自签字（盖章）之日起生效，双方应严格遵照执行。</p><p class="MsoNormal">&nbsp; &nbsp; &nbsp; &nbsp;第九条 &nbsp;其他补充条例可由双方协商补充：</p><table class="table ke-zeroborder" style="width:90%;" cellpadding="0" cellspacing="0" border="0" align="center"><tbody><tr><td style="border-width:0px;border-style:solid;"><span style="text-align:justify;white-space:normal;">甲方（盖章）</span><br /></td><td style="border-width:0px;border-style:solid;"><span style="text-align:justify;white-space:normal;">乙方（签字）</span><br /></td></tr><tr><td style="border-width:0px;border-style:solid;"><span style="text-align:justify;white-space:normal;">法定代表人（主要负责人）</span><br /></td><td style="border-width:0px;border-style:solid;"><br /></td></tr><tr><td style="border-width:0px;border-style:solid;"><span style="text-align:justify;white-space:normal;">年&emsp; 月&emsp; 日</span><br /></td><td style="border-width:0px;border-style:solid;"><span style="text-align:justify;white-space:normal;">年&emsp; 月&emsp; 日</span><br /></td></tr></tbody></table>',
  'headjoin_num' => '3',
);
