<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use tools\TencentEss;

class Test extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';
    protected $tencentEss;

    public function _initialize()
    {
        parent::_initialize();
        $config = [
            'secretId' => 'AKIDfBVcUAdsseCWbU9anlUzFH68xYx98uSl', //AKyDRshUUgyg3d5w7wUJYhdmxX2BXPgiZo
            'secretKey' => '81f4TgeNs8AxYw61jW7WjkaBXfYDdo8M', //SKVr1e3uxXCuCaExYG18GniEqf1odGyiih
            'userId' => 'yDRsdUU1d2yvpUt1KaoRp9wmGhqCQGSs', //yDRsQUUgyg1fepp3Uy0BxDk8aT0L5MPa
        ];
        $this->tencentEss = new TencentEss;
    }

    public function index()
    {   
        
        echo 'ok';
    }

    //创建签署流程
    public function createFlow(){
        $data = [
            'companyInfo' => ['organizationName'=>'八冈信息技术（广东）有限公司','type'=>0,'name'=>'李威','mobile'=>'18588730193'],
            'personInfo' => ['type'=>1,'name'=>'黄应有','mobile'=>'15814568347'],
            'flowName' => '合同测试',
            'deadLine' => time() + 7 * 24 * 3600,  // 请设置合理的时间，否则容易造成合同过期
            'unordered' => true,//true无序  false有序
        ];
        try {
            $res = $this->tencentEss->createFlow($data);
            print_r($res);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    //创建电子文档
    public function createDocument(){
        $data = [
            'flowId' => 'yDRsqUU1ymhvqUPKTxOBE7Xa3aAmJ5vM',
            'templateId' => 'yDRsqUU1ylandUEHiFlBBSwr2A0q91f1',//yDRshUUgyg3dht9mUxVxiTIw8wj58SXE
            'componentNames' => [
                ['name' => '企业全称','value' => '八冈信息技术（广东）有限公司'],
                ['name' => '统一社会信用代码','value' => '91441203MA57B9UB8Q'],
                ['name' => '劳务派遣许可证编号','value' => '1111111'],
                ['name' => '开始日期','value' => '2022-11-29'],
                ['name' => '终止日期','value' => '2022-12-29'],
                ['name' => '公司名称','value' => '测试公司'],
                ['name' => '工作地点','value' => '广州市天河区29号'],
                ['name' => '白班时薪','value' => '15'],
                ['name' => '夜班时薪','value' => '20'],
            ]
        ];
        try {
            $res = $this->tencentEss->createDocument($data);
            print_r($res);//yDRsqUU1ym6upUy3JvJC8LN33GFcPdR1
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    //发起流程
    public function startFlow(){
        $flowId = 'yDRsqUU1ymhvqUPKTxOBE7Xa3aAmJ5vM';
        try {
            $res = $this->tencentEss->startFlow($flowId);
            print_r($res);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    //获取小程序跳转链接
    public function createSchemeUrl(){
        $data = [
            'flowId' => 'yDRsqUU1ymhvqUPKTxOBE7Xa3aAmJ5vM',
            'pathType' => 1
        ];
        try {
            $res = $this->tencentEss->createSchemeUrl($data);
            print_r($res);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    //查询流程摘要
    public function describeFlowBriefs(){
        $flowIds = [
            'yDRsqUU1ymhvqUPKTxOBE7Xa3aAmJ5vM',
        ];
        try {
            $res = $this->tencentEss->describeFlowBriefs($flowIds);
            print_r($res);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    //撤销签署流程
    public function cancelFlow(){
        $flowId = 'yDRsqUU1ymhvqUPKTxOBE7Xa3aAmJ5vM';
        $msg = '测试';
        try {
            $res = $this->tencentEss->cancelFlow($flowId,$msg);
            print_r($res);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

}
