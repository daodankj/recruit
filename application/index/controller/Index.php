<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Cookie;
use think\Hook;
use think\Session;
use EasyWeChat\Factory;
use app\common\library\Menu;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
        $auth = $this->auth;

        //监听注册登录退出的事件
        Hook::add('user_login_successed', function ($user) use ($auth) {
            $expire = input('post.keeplogin') ? 30 * 86400 : 0;
            Cookie::set('uid', $user->id, $expire);
            Cookie::set('token', $auth->getToken(), $expire);
        });
        Hook::add('user_register_successed', function ($user) use ($auth) {
            Cookie::set('uid', $user->id);
            Cookie::set('token', $auth->getToken());
        });
        Hook::add('user_delete_successed', function ($user) use ($auth) {
            Cookie::delete('uid');
            Cookie::delete('token');
        });
        Hook::add('user_logout_successed', function ($user) use ($auth) {
            Cookie::delete('uid');
            Cookie::delete('token');
        });
    }

    public function index()
    {   
        /*$config = get_addon_config('recruit');
        $options = [
            'app_id' => $config['wxappid'],
            'secret' => $config['wxappsecret'],

            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'level' => 'debug',
                'file' => __DIR__.'/wechat.log',
            ],
        ];
        $app = Factory::miniProgram($options);
        $img = $app->app_code->getUnlimit(14, [
            'page'  => 'page/zh_index/index',
            'width' => 418,
            'auto_color' => true,
        ]);
        print_r($img);*/
        return $this->view->fetch();
    }


    //uid自动登入
    public function auto_login_byuid($uid=0){
    	//$uid = db('user')->where(['id'=>$uid])->value('id');
    	$this->auth->direct($uid);
    	/*if($this->auth->direct($uid)){
    		$this->success('登入成功','/score');
    	}else{
    		$this->error('登入失败');
    	}*/
    	$this->redirect('/score');
    }


    public function testm(){
        
    }

}
