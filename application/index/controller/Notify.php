<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use \think\Log;

class Notify extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {   
        $data = 'jAGoVeLV1k3rcgCObU6pP3uqx4ufUV74Kec4Nl1CxW9tbaCy5yMIDH6rH1qRmGB75/UpOQUzhYJj6ct4h6pSeF6NaSaGROUI+6knf0sH8//8jc475ccOdMLyfTLjS2Vt0eLQAzPjUtMLPegSKDAmmTN8wQYnjjpPh4p61nx2Mci6DI4lZ2CDoqLWG42c5tIcjKdOJ7U3y0FCi7YrxljDUjB7Jcl5199l/D+D0yN8YVKNSjS2VPfRWifrPR0WIKPSkbY2RsBkCrIbdywZNgEiPMVF6GMdw5WXzaihA9QskhPh9fvXZLQzvpI3fJdUS/mj';
        $data = openssl_decrypt(base64_decode($data),'AES-256-CBC','D44D5B4DE969489FB3EE5F8BE0D708AB', OPENSSL_RAW_DATA,'D44D5B4DE969489F');
        echo $data;
    }

    public function tencent_notify(){
        $data = file_get_contents('php://input');
        if ($data) {
            //解密
            $data = openssl_decrypt(base64_decode($data),'AES-256-CBC','D44D5B4DE969489FB3EE5F8BE0D708AB', OPENSSL_RAW_DATA,'D44D5B4DE969489F');
            Log::write('电子签回调数据解码后:'.$data,'wxcallback');

            $data = json_decode($data,true);
            $cMod = new \app\admin\model\Contract;
            $bmMod = new \app\admin\model\ActiveBmlist;
            if (isset($data['CallbackType']) && $data['CallbackType']=='sign') {
                $cinfo = $cMod->get(['flow_id'=>$data['FlowId']]);
                if ($cinfo) {
                    $cinfo->status = $data['FlowCallbackStatus'];
                    $cinfo->save();
                    if ($data['FlowCallbackStatus'] == 4) {
                        //已签署修改劳务工状态
                        $binfo = $bmMod->get(['id'=>$cinfo['bm_id']]);
                        if ($binfo['status']==1) {
                            $binfo->status = 3;
                            $binfo->save();
                        }
                    }
                }
                //$cMod->where(['flow_id'=>$data['FlowId']])->update(['status'=>$data['FlowCallbackStatus']]);
            }
        }
        //echo 'ok';
    }

    private function flowCallbackStatus($status){
        $status = ['','待签署','部分签署','已拒签','已签署','已过期','已撤销'];
    }

}
