<?php

namespace tools;

use WeChatPay\Builder;
use WeChatPay\Crypto\Rsa;
use WeChatPay\Util\PemUtil;
use WeChatPay\Crypto\AesGcm;
use WeChatPay\Formatter;
use \think\Log;

/**
 * 微信支付
 */
class Wexinpay
{

    protected $merchantId = '1626988160';
    protected $merchantCertificateSerial = '655F9A697CE29DBEAC5D461F38229FEE1F520966';
    protected $instance;

    public function __construct($options = [])
    {
        // 商户号
        $merchantId = $this->merchantId;
        // 从本地文件中加载「商户API私钥」，「商户API私钥」会用来生成请求的签名
        $merchantPrivateKeyFilePath = 'file://./cert/apiclient_key.pem';
        //$merchantPrivateKeyFilePath = file_get_contents($merchantPrivateKeyFilePath);
        $merchantPrivateKeyInstance = Rsa::from($merchantPrivateKeyFilePath, Rsa::KEY_TYPE_PRIVATE);

        // 「商户API证书」的「证书序列号」
        $merchantCertificateSerial = $this->merchantCertificateSerial;

        // 从本地文件中加载「微信支付平台证书」，用来验证微信支付应答的签名
        $platformCertificateFilePath = 'file://./cert/wechatpay_6504E9F14C29964E34F136EE56B5DCFAB6ADD515.pem';
        //$platformCertificateFilePath = file_get_contents($platformCertificateFilePath);
        $platformPublicKeyInstance = Rsa::from($platformCertificateFilePath, Rsa::KEY_TYPE_PUBLIC);

        // 从「微信支付平台证书」中获取「证书序列号」
        $platformCertificateSerial = PemUtil::parseCertificateSerialNo($platformCertificateFilePath);

        // 构造一个 APIv3 客户端实例
        $this->instance = Builder::factory([
            'mchid'      => $merchantId,
            'serial'     => $merchantCertificateSerial,
            'privateKey' => $merchantPrivateKeyInstance,
            'certs'      => [
                $platformCertificateSerial => $platformPublicKeyInstance,
            ],
        ]);
    }

    public function transfer($option){
        try {
            $resp = $this->instance
            ->chain('v3/transfer/batches')
            ->post(['json' => $option]);
            $info = json_decode($resp->getBody(),true);
        } catch (\Exception $e) {
            // 进行错误处理
            $info = $e->getMessage();
            $info = substr($info, strpos($info, 'response:')+9);
            $info = json_decode(trim($info),true);
            //Log::write('错误日志:'.$e->getMessage(),'createorerError');
        }
        return $info;
    }

    
}
