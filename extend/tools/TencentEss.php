<?php

namespace tools;

use TencentCloud\Ess\V20201111\EssClient;
use TencentCloud\Ess\V20201111\Models\UserInfo;
use TencentCloud\Ess\V20201111\Models\FlowCreateApprover;
use TencentCloud\Ess\V20201111\Models\CreateFlowRequest;
use TencentCloud\Ess\V20201111\Models\FormField;
use TencentCloud\Ess\V20201111\Models\CreateDocumentRequest;
use TencentCloud\Ess\V20201111\Models\StartFlowRequest;
use TencentCloud\Ess\V20201111\Models\CreateSchemeUrlRequest;
use TencentCloud\Ess\V20201111\Models\DescribeFlowBriefsRequest;
use TencentCloud\Ess\V20201111\Models\CancelFlowRequest;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;

/**
 * 腾讯电子签
 */
class TencentEss
{

    protected $secretId = 'AKIDfBVcUAdsseCWbU9anlUzFH68xYx98uSl';//AKyDRshUUgyg3d5w7wUJYhdmxX2BXPgiZo
    protected $secretKey = '81f4TgeNs8AxYw61jW7WjkaBXfYDdo8M';//SKVr1e3uxXCuCaExYG18GniEqf1odGyiih
    protected $userId = 'yDRsdUU1d2yvpUt1KaoRp9wmGhqCQGSs';//yDRsQUUgyg1fepp3Uy0BxDk8aT0L5MPa
    protected $userInfo = null;
    protected $client;

    public function __construct($options = [])
    {
        if (isset($options['secretId'])) {
            $this->secretId = $options['secretId'];
        }
        if (isset($options['secretKey'])) {
            $this->secretKey = $options['secretKey'];
        }
        if (isset($options['userId'])) {
            $this->userId = $options['userId'];
        }
        // 实例化一个证书对象，入参需要传入腾讯云账户secretId，secretKey
        $cred = new Credential($this->secretId, $this->secretKey);

        // 实例化一个http选项，可选的，没有特殊需求可以跳过
        $httpProfile = new HttpProfile();
        $httpProfile->setReqMethod("POST");  // post请求(默认为post请求)
        $httpProfile->setReqTimeout(30);    // 请求超时时间，单位为秒(默认60秒)
        $httpProfile->setEndpoint("ess.tencentcloudapi.com");  // 指定接入地域域名(默认就近接入)

        // 实例化一个client选项，可选的，没有特殊需求可以跳过
        $clientProfile = new ClientProfile();
        $clientProfile->setSignMethod("TC3-HMAC-SHA256");  // 指定签名算法(默认为HmacSHA256)
        $clientProfile->setHttpProfile($httpProfile);

        $this->userInfo = new UserInfo();
        $this->userInfo->setUserId($this->userId);

        $this->client = new EssClient($cred, "ap-guangzhou", $clientProfile);
    }

    //创建签署流程
    public function createFlow($data){
        try {
            $req = new CreateFlowRequest();
            $req->setOperator($this->userInfo);

            // 企业方 静默签署时type为3/非静默签署type为0
            $enterpriseInfo = new FlowCreateApprover();
            $enterpriseInfo->setApproverType($data['companyInfo']['type']);
            $enterpriseInfo->setOrganizationName($data['companyInfo']['organizationName']);
            $enterpriseInfo->setApproverName($data['companyInfo']['name']);
            $enterpriseInfo->setApproverMobile($data['companyInfo']['mobile']);
            $enterpriseInfo->setRequired(true);

            // 客户个人
            $clientInfo = new FlowCreateApprover();
            $clientInfo->setApproverType($data['personInfo']['type']);
            $clientInfo->setApproverName($data['personInfo']['name']);
            $clientInfo->setApproverMobile($data['personInfo']['mobile']);
            $clientInfo->setRequired(true);

            // 企业方2 （当进行B2B场景时，允许指向未注册的企业，签署人可以查看合同并按照指引注册企业）
            $req->Approvers = [];
            array_push($req->Approvers, $enterpriseInfo);
            array_push($req->Approvers, $clientInfo);

            $req->setFlowName($data['flowName']);
            // 请设置合理的时间，否则容易造成合同过期
            $req->setDeadLine($data['deadLine']);
            $req->setUnordered($data['unordered']);

            $resp = $this->client->CreateFlow($req);

            // 输出json格式的字符串回包
            return $resp->toJsonString();
        } catch (TencentCloudSDKException $e) {
            throw new \Exception($e);
        }
    }

    //创建电子文档
    public function createDocument($data){
        try {
            $req = new CreateDocumentRequest();
            $req->setOperator($this->userInfo);

            $req->FileNames = [];
            // 无需关注，传入自定义任意值即可
            array_push($req->FileNames, "filename");

            // 由CreateFlow返回
            $req->setFlowId($data['flowId']);

            // 后台配置后查询获取
            $req->setTemplateId($data['templateId']);
            $req->FormFields = [];
            foreach ($data['componentNames'] as $key => $val) {
                $formField = new FormField();
                // 在模板配置拖入控件的界面可以查询到
                $formField->setComponentName($val['name']);
                $formField->setComponentValue($val['value']);
                array_push($req->FormFields, $formField);
            }     
            $resp = $this->client->CreateDocument($req);

            // 输出json格式的字符串回包
            return $resp->toJsonString();
        } catch (TencentCloudSDKException $e) {
            throw new \Exception($e);
        }
    }

    //发起流程
    public function startFlow($flowId){
        try {
            $req = new StartFlowRequest();
            $req->setOperator($this->userInfo);

            $req->setFlowId($flowId);

            $resp = $this->client->StartFlow($req);

            // 输出json格式的字符串回包
            return $resp->toJsonString();
        } catch (TencentCloudSDKException $e) {
            throw new \Exception($e);
        }
    }

    //获取小程序跳转链接
    public function createSchemeUrl($data){
        try {
            $req = new CreateSchemeUrlRequest();
            $req->setOperator($this->userInfo);

            $req->setFlowId($data['flowId']);
            $req->setPathType($data['pathType']);

            $resp = $this->client->CreateSchemeUrl($req);

            // 输出json格式的字符串回包
            return $resp->toJsonString();
        } catch (TencentCloudSDKException $e) {
            throw new \Exception($e);
        }
    }

    //查询流程摘要
    public function describeFlowBriefs($flowIds){
        try {
            $req = new DescribeFlowBriefsRequest();
            $req->setOperator($this->userInfo);

            $req->FlowIds = [];
            foreach ($flowIds as $val) {
                array_push($req->FlowIds, $val);
            }

            $resp = $this->client->DescribeFlowBriefs($req);

            // 输出json格式的字符串回包
            return $resp->toJsonString();
        } catch (TencentCloudSDKException $e) {
            throw new \Exception($e);
        }
    }

    //撤销签署流程
    public function cancelFlow($flowId,$msg){
        try {
            $req = new CancelFlowRequest();
            $req->setOperator($this->userInfo);

            $req->setFlowId($flowId);
            $req->setCancelMessage($msg);

            $resp = $this->client->CancelFlow($req);

            // 输出json格式的字符串回包
            return $resp->toJsonString();
        } catch (TencentCloudSDKException $e) {
            throw new \Exception($e);
        }
    }
}
