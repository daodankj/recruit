// page/my/login.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },
formSubmit: function (event) {
    var that = this;
    if (event.detail.value.mobile == '') {
      app.error('手机号码不能为空');
      return;
    }
    if (event.detail.value.password == '') {
      app.error('密码不能为空');
      return;
    }
    app.request('/user/login_password', event.detail.value, function (data) {
      app.globalData.userInfo = data.userInfo;
      wx.setStorageSync('token', data.userInfo.token);
      app.success('登入成功!', function () {
        setTimeout(function () {
          //要延时执行的代码
          wx.switchTab({
            url: 'index'
          });
        }, 2000); //延迟时间
      });
    }, function (data, ret) {
      app.error(ret.msg);
    });
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})