<?php

return [
    [
        'name' => 'wxappid',
        'title' => '小程序AppID',
        'type' => 'string',
        'content' => [],
        'value' => 'wx7f30b25fcc0561db',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'wxappsecret',
        'title' => '小程序AppSecret',
        'type' => 'string',
        'content' => [],
        'value' => '4f8c6d4d19119a1364894cbb5a661e68',
        'rule' => 'required',
        'msg' => '',
        'tip' => '',
        'ok' => '',
        'extend' => '',
    ],
    [
        'name' => 'apikey',
        'title' => 'ApiKey',
        'type' => 'string',
        'content' => [],
        'value' => '',
        'rule' => '',
        'msg' => '',
        'tip' => '用于调用API接口时写入数据权限控制<br>可以为空',
        'ok' => '',
        'extend' => '',
    ],
];
