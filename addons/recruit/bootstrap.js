require.config({
    paths: {
        'slider': '../addons/recruit/js/bootstrap-slider.min'
    },
    shim: {
        'slider': ['css!../addons/recruit/css/bootstrap-slider.min.css'],
    }
});
require(['form', 'upload'], function (Form, Upload) {
    var _bindevent = Form.events.bindevent;
    Form.events.bindevent = function (form) {
        _bindevent.apply(this, [form]);
        try {
            if ($(".slider_double", form).size() > 0) {
                require(['slider'], function () {
                });
            }
        } catch (e) {

        }

    };
});
