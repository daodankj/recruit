<?php

namespace addons\recruit\controller;
use app\admin\model\User;

/**
 * 打卡签到
 */
class Clock extends Base
{

    protected $noNeedLogin = [];

    public function _initialize()
    {
        parent::_initialize();
    }



    //获取打卡信息,判断是否能打卡
    public function index(){
        $lat = $this->request->post('lat');
        $lng = $this->request->post('lng');

        $user_id = $this->auth->id;

        $status = 0;
        $companyName = '您还未分配到工作任务，无法打卡！';
        $diff = 0;
        $dkinfo = [];
        //获取用户是否有签合同
        $bminfo = db('active_bmlist')->where(['user_id'=>$user_id,'status'=>3])->find();
        if($bminfo){
            $status = 1;
            //获取任务信息
            $ainfo = db('recruit_work')->field('id,name,lat,lng,company_id,price,error_range')->where(['id'=>$bminfo['work_id']])->find();
            if ($ainfo) {
                $companyName = db('recruit_company')->where(['id'=>$ainfo['company_id']])->value('name');
                //计算是否在打卡范围
                $diff = distance($ainfo['lat'],$ainfo['lng'],$lat,$lng);//公里数
                $diff = $diff*1000;//转换成米
                if ($diff>$ainfo['error_range']) {
                    $status = -1;
                    $companyName = '距离打卡地太远，无法打卡';
                }
            }else{
                $status = -1;
                $companyName = '未查询到任务信息，无法打卡';
            }
            //获取打卡记录信息
            $dkinfo = db('daka')->where(['user_id'=>$user_id,'date'=>date('Y-m-d')])->find();
            if ($dkinfo && $dkinfo['in_time']>0) {
                $dkinfo['in_time_text'] = date('Y-m-d H:i:s',$dkinfo['in_time']);
                $status==1&&$status = 2;
            }
            if ($dkinfo && $dkinfo['out_time']>0) {
                $dkinfo['out_time_text'] = date('Y-m-d H:i:s',$dkinfo['out_time']);
                $status==2&&$status = 3;
            }
            if (!$dkinfo) {//今天未打卡，判断是否是夜班，昨天晚上签到的
                //获取最后一次打卡记录
                $dkinfo = db('daka')->where(['user_id'=>$user_id])->order('id desc')->find();
                if ($dkinfo&&$dkinfo['out_time']==0) {//未签退的,说明是晚班，今天要先签退
                    $dkinfo['in_time_text'] = date('Y-m-d H:i:s',$dkinfo['in_time']);
                    $status==1&&$status = 2;
                }
            }
        }
        
        $data = ['status'=>$status,'companyName'=>$companyName,'diff'=>$diff,'dkinfo'=>$dkinfo];
        $this->success('',$data);
    }

    public function daka(){
        $type = $this->request->post('type',1);//1签到  2签退
        $image = $this->request->post('image','');
        $remark = $this->request->post('remark','');
        $user_id = $this->auth->id;
        //获取用户是否有分配工作
        $bminfo = db('active_bmlist')->where(['user_id'=>$user_id,'status'=>3])->find();
        if(!$bminfo){
            $this->error('您还未分配到工作任务，无法打卡！');
        }

        $workinfo = db('recruit_work')->where('id='.$bminfo['work_id'])->field('price,price2,score')->find();

        $dakatype = 1;//打卡班次，白班和夜班，晚上7点后打卡的算夜班
        //签到判断是夜班还是白班,晚上7点后打卡的算夜班
        $hour = date('H');
        if ($hour>19) {
            $dakatype = 2;
            $bminfo['price'] = $workinfo['price2'];//db('recruit_work')->where('id='.$bminfo['work_id'])->value('price2');//夜班工价
        }else{
            $bminfo['price'] = $workinfo['price'];//db('recruit_work')->where('id='.$bminfo['work_id'])->value('price');
        }
        //获取打卡记录信息
        $dkinfo = db('daka')->where(['user_id'=>$user_id,'date'=>date('Y-m-d')])->find();
        if ($dkinfo && $dkinfo['in_time']>0 && $type==1) {
            $this->error('今天已签到，无需再次签到');
        }
        if ($dkinfo && $dkinfo['out_time']>0 && $type==2) {
            $this->error('今天已签退，无需再次签退');
        }
        //夜班签退判断，应为夜班是第二天签退
        if (!$dkinfo && $type==2) {//夜班，第二天签退
            //$this->error('今天还未签到，无法签退');
            //获取最后一次打卡记录
            $dkinfo = db('daka')->where(['user_id'=>$user_id])->order('id desc')->find();
            if ($dkinfo&&$dkinfo['out_time']==0) {//未签退的
                //签退，计算工作时间，先精确到分钟
                $diff_time = time() - $dkinfo['in_time'];
                $work_time = round($diff_time/3600,2);
                $money = $work_time * $dkinfo['price'];
                $rs = db('daka')->where(['id'=>$dkinfo['id']])->update(['out_time'=>time(),'work_time'=>$work_time,'money'=>$money]);
                if ($rs) {
                    $this->success('操作成功');
                }else{
                    $this->error('操作失败');
                }
            }else{
                $this->error('无打卡记录，无法签退');
            }
        }
        //--------------夜班签退end---------------
        if (!$dkinfo) {
            $data = [
                'type'          => $dakatype,
                'work_id'       => $bminfo['work_id'],
                'bm_id'         => $bminfo['id'],
                'user_id'       => $user_id,
                'agent_user_id' => $bminfo['agent_user_id'],
                'agent_type'    => $bminfo['agent_type'],
                'price'         => $bminfo['price'],
                'date'          => date('Y-m-d'),
                'in_time'       => time(),
                'image'         => $image,
                'remark'        => $remark
            ];
            $rs = db('daka')->insert($data);
        }else{
            //签退，计算工作时间，先精确到分钟
            $diff_time = time() - $dkinfo['in_time'];
            $work_time = round($diff_time/3600,2);
            $money = $work_time * $dkinfo['price'];
            $rs = db('daka')->where(['id'=>$dkinfo['id']])->update(['out_time'=>time(),'work_time'=>$work_time,'money'=>$money]);
        }
        if ($rs) {
            //打卡获得积分
            $u_mod = new User();
            $u_mod->addScore($user_id,$workinfo['score'],'打卡获得积分');
            $this->success('操作成功');
        }else{
            $this->error('操作失败');
        }
        
    }
}
