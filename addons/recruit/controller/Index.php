<?php

namespace addons\recruit\controller;

//use think\addons\Controller;
//use app\common\model\Addon;

use addons\recruit\model\News;

use app\admin\model\Job;
use app\admin\model\Company;
use app\admin\model\RecruitActive;
use app\admin\model\SxJob;

class Index extends Base
{
    protected $noNeedLogin = '*';

    public function _initialize()
    {
        parent::_initialize();
    }

    public function index()
    {
        $this->error("当前插件暂无前台页面");
    }

    public function get_index_all_data()
    {
        $city_id = $this->request->post('city_id');

        $bannerList = [];
        $list = News::getBannerList(['type' => 'focus', 'status' => 'normal', 'row' => 5, 'cache' => 0]);
        foreach ($list as $index => $item) {
            $bannerList[] = ['image' => cdnurl($item['image'], true), 'url' => '/', 'title' => $item['title'], 'name' => $item['name'], 'id' => $item['id']];
        }

        //最新10条招募互动数据
        $now_date = date('Y-m-d');
        $activeList = [];
        $active_m = new RecruitActive();
        if ($city_id == 'no' || $city_id == 'undefined') {
            $where_m['city_id'] = $city_id;
        } else {
            $where_m = [];
        }
        $activeList = $active_m->where('start_date<="'.$now_date.'" and end_date>="'.$now_date.'"')
                ->where($where_m)
                ->field('id,name,user_num,settlement_type,address,company_name,price,worktime,start_date,end_date,labor_id')
                ->order('updatetime', 'desc')
                ->limit(10)
                ->select();
        foreach ($activeList as $key => &$val) {
            $val['settlement_type'] = $val['settlement_type']==1?"月结":($val['settlement_type']==2?"周结":"日结");
        }
        //最新5条时薪职位列表
        $sxList = [];
        $sxjob_m = new SxJob();
        if ($city_id == 'no' || $city_id == 'undefined') {
            $sxList = $sxjob_m->where('status=1')->limit(5)
                ->order('updatetime', 'desc')
                ->select();
        } else {
            $sxList = $sxjob_m->where('status=1 and city_id='.$city_id)->limit(5)
                ->order('updatetime', 'desc')
                ->select();
        }
        foreach ($sxList as $key => &$val) {
            $val['settlement_type'] = $val['settlement_type']==1?"月结":($val['settlement_type']==2?"周结":"日结");
        }
        //这里把最新的10条职位列表拖出来
        $JobsList = [];
        $JobsList_m = new Job();
        if ($city_id == 'no' || $city_id == 'undefined') {
            $JobsList = $JobsList_m->with('recruitcompany')->limit(10)
                ->order('toptime', 'desc')
                ->select();
        } else {
            $JobsList = $JobsList_m->with('recruitcompany')->where('recruitcompany.city_id', $city_id)->limit(10)
                ->order('toptime', 'desc')
                ->select();
        }

        $ZhusD = ['不提供住宿', '提供住宿', '提供夫妻房'];
        $FoodD = ['不提供伙食', '提供午饭', '提供三餐', '有餐补'];
        $SafeD = ['不提供社保', '缴纳三险', '缴纳五险', '缴纳五险一金'];
        $EducationD = ['无学历要求', '小学', '初中', '高中', '大专', '本科', '研究生及以上'];
        $comxinzn = ['国有企业', '私营企业', '中外合作企业', '中外合资企业', '外商独资企业'];
        foreach ($JobsList as $key => &$item) {
            if ($item['recruitcompany']['xinzhi'] == null) {
                unset($JobsList[$key]);
                continue;
            }
            $tempd = $item['recruitcompany']['xinzhi'];
            $item['XinZhiname'] = $comxinzn[$tempd];

            //这里格式化一下 住宿、社保、学历
            $item['zhusuname'] = $ZhusD[$item['stay']];
            $item['Safename'] = $SafeD[$item['safe']];
            $item['Educationname'] = $EducationD[$item['education']];

            //这里需要格式化一下 工资薪水
            if ($item['gold1'] == $item['gold2']) {
                $item['goldtext'] = ($item['gold1'] / 1000) . "K";
                if ($item['gold1'] == 3000) {
                    $item['goldtext'] .= "以下";
                }
                if ($item['gold1'] == 10000) {
                    $item['goldtext'] .= "以上";
                }
            } else {
                $item['goldtext'] = ($item['gold1'] / 1000) . "K-" . ($item['gold2'] / 1000) . "K";
            }
        }

        //拖取企业数据
        $companyList = [];//知名企业
        $companyList2 = [];//最行企业
        $companyList3 = [];//高兴急聘
        if ($city_id == 'no' || $city_id == 'undefined') {
            $cwhere['city_id'] = $city_id;
        } else {
            $cwhere = [];
        }
        $companyList = db('recruit_company')->where('type=1')->where($cwhere)->field('id,name,xinzhi')->order('id desc')->limit(4)->select();
        $companyList2 = db('recruit_company')->where('type=2')->where($cwhere)->field('id,name,xinzhi')->order('id desc')->limit(4)->select();
        $companyList3 = db('recruit_company')->where('type=3')->where($cwhere)->field('id,name,xinzhi')->order('id desc')->limit(4)->select();
        foreach ($companyList as $key => &$val) {
            $val['Id']         = $val['id'];
            $val['XinZhiname'] = $comxinzn[$val['xinzhi']];
            $val['jobCount']   = Job::where('c_id', $val['id'])->count();
        }
        foreach ($companyList2 as $key => &$val) {
            $val['Id']         = $val['id'];
            $val['XinZhiname'] = $comxinzn[$val['xinzhi']];
            $val['jobCount']   = Job::where('c_id', $val['id'])->count();
        }
        foreach ($companyList3 as $key => &$val) {
            $val['Id']         = $val['id'];
            $val['XinZhiname'] = $comxinzn[$val['xinzhi']];
            $val['jobCount']   = Job::where('c_id', $val['id'])->count();
        }
        /*foreach ($JobsList as $keyJ => $itemJ) {
            if (count($companyList) >= 8) {
                break;
            }

            $HasT = false;
            foreach ($companyList as $keyC => $itemC) {
                if ($itemC['name'] == $itemJ['recruitcompany']['name']) {
                    $HasT = true;
                    break;
                }
            }
            if (!$HasT) {
                $tempd = $itemJ['recruitcompany']['xinzhi'];
                $itemJ['recruitcompany']['XinZhiname'] = $comxinzn[$tempd];

                $itemJ['recruitcompany']['jobCount'] = Job::where('c_id', $itemJ['recruitcompany']['Id'])->count();
                array_push($companyList, $itemJ['recruitcompany']);
            }
        }*/
        //现在判断是否已经确认过隐私协议
        $user_id = $this->request->post("user_id");
        $third = \addons\third\model\Third::where('user_id',$user_id)->find();
        if ($third && $third->is_read==1) {//已读
            $xyinfo = '';
        }else{//未读才返回协议内容
            $xyinfo = db('recruit_news')->where('id=8')->value('content');
        }

        $data = [
            'bannerList' => $bannerList,
            'companyList' => $companyList,
            'companyList2' => $companyList2,
            'companyList3' => $companyList3,
            'JobsList' => $JobsList,
            'activeList' => $activeList,
            'sxList'     => $sxList,
            'xyinfo'     => $xyinfo
        ];
        $this->success('', $data);
    }

    //确认协议
    public function sure_xy(){
        $user_id = $this->request->post("user_id");
        if (!$user_id) {
            $this->error('请先登入');
        }
        $third = \addons\third\model\Third::where('user_id',$user_id)->find();
        if (!$third) {
            $this->error('操作异常');
        }
        $third->is_read = 1;
        $rs = $third->save();
        if ($rs) {
            $this->success('');
        }else{
            $this->error('操作失败');
        }
    }
}
