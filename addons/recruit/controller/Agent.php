<?php

namespace addons\recruit\controller;
use app\admin\model\User;
use Exception;
use think\Db;
use think\exception\PDOException;
use think\exception\ValidateException;
use tools\TencentEss;
use fast\Random;
/**
 * 推广员
 */
class Agent extends Base
{

    protected $noNeedLogin = [];
    protected $tencentEss;

    public function _initialize()
    {
        parent::_initialize();
    }



    //获取报名用户列表
    public function bm_list(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\ActiveBmlist;
        //处理失效报名,7天之前的
        $day_befortime = time() - 7*24*3600;
        $bmMod->where(['status'=>0,'createtime'=>['<',$day_befortime]])->update(['status'=>-1]);
        //计算总数
        $noNum = $bmMod->where(['status'=>0,'agent_user_id'=>$user_id])->count();
        $yesNum = $bmMod->where(['status'=>['>',-1],'agent_user_id'=>$user_id])->count();

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['status'=>['>',-1],'agent_user_id'=>$user_id])->order('status asc')->limit($limit)->select();
        foreach ($list as $key => &$val) {
            $val['createtime'] = date('Y-m-d',$val['createtime']);
        }
        $data = ['noNum'=>$noNum,'yesNum'=>$yesNum,'list'=>$list];
        $this->success('',$data);
    }

    //工作中的员工
    public function worker_list(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');
        $agent_type = $this->request->post('agent_type',1);

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\ActiveBmlist;
        
        //计算总数
        $bandNum = $bmMod->where(['status'=>3,'agent_user_id'=>$user_id,'agent_type'=>$agent_type])->count();

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['status'=>3,'agent_user_id'=>$user_id,'agent_type'=>$agent_type])->limit($limit)->select();
        foreach ($list as $key => &$val) {
            $val['statusName'] = '工作中';
        }
        //本月提成
        $y = date("Y", time()); //年 
        $m = date("m", time()); //月 
        $start_month = mktime(0, 0, 0, $m, 1, $y); // 创建本月开始时间 
        $total_hour = db('daka')->where(['agent_type'=>$agent_type,'agent_user_id'=>$user_id,'in_time'=>['>',$start_month]])->sum('work_time');
        if ($agent_type==1) {
            //获取推广员提成点
            $commission = db('agent')->where('user_id',$user_id)->value('commission');
            $money = $total_hour * $commission;
        }else{
            $money = 0;
        }
        

        $data = ['bandNum'=>$bandNum,'money'=>$money,'list'=>$list];
        $this->success('',$data);
    }
    //绑定列表
    public function bind_list(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\ActiveBmlist;
        
        //计算总数
        $bandNum = $bmMod->where(['status'=>['>',-1],'agent_user_id'=>$user_id])->count();

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['status'=>['>',-1],'agent_user_id'=>$user_id])->limit($limit)->select();
        foreach ($list as $key => &$val) {
            if ($val['status']==0) {
                $val['statusName'] = '待入职';
            }else if($val['status']==1){
                $val['statusName'] = '待签合同';
            }else if($val['status']==2){
                $val['statusName'] = '已签合同';
            }else if($val['status']==3){
                $val['statusName'] = '工作中';
            }
            
        }
        $data = ['bandNum'=>$bandNum,'money'=>0.00,'list'=>$list];
        $this->success('',$data);
    }
    
    //确认入职
    public function sure_induction(){
        $id = $this->request->post('id');

        $bmMod = new \app\admin\model\ActiveBmlist;
        $info = $bmMod->get($id);
        if (!$info || $info->status!=0) {
            $this->error('已入职，无需重新操作');
        }
        $info->status = 1;
        $info->induction_time = time();
        $rs = $info->save();
        if ($rs) {
            $this->success('操作成功');
        }else $this->error('操作失败');
    }

    //确认签合同
    public function sure_contract(){
        $id = $this->request->post('id');

        $bmMod = new \app\admin\model\ActiveBmlist;
        $info = $bmMod->get($id);
        if (!$info || $info->status!=1) {
            $this->error('已签合同，无需重新操作');
        }
        $info->status = 2;
        $info->contract_time = time();
        $rs = $info->save();
        if ($rs) {
            $this->success('操作成功');
        }else $this->error('操作失败');
    }

    //解绑
    public function bind_del(){
        $id = $this->request->post('id');

        $bmMod = new \app\admin\model\ActiveBmlist;
        $info = $bmMod->get($id);
        if (!$info || $info->status!=3) {
            $this->error('已解绑，无需重新操作');
        }
        $info->status = 2;
        $info->unbind_time = time();
        $rs = $info->save();
        //工作记录
        $rs2 = db('work_recod')->where(['user_id'=>$info->user_id,'bm_id'=>$info->id])->order('id desc')->limit(1)->update(['unbind_time'=>time()]);
        if ($rs) {
            $this->success('操作成功');
        }else $this->error('操作失败');
    }

    //离职
    public function sure_leave(){
        $id = $this->request->post('id');

        $bmMod = new \app\admin\model\ActiveBmlist;
        $info = $bmMod->get($id);
        if (!$info || $info->status==3) {
            $this->error('工作中，无法离职，请先解绑工作');
        }
        $info->status = -1;
        $info->leave_time = time();
        $rs = $info->save();
        if ($rs) {
            $this->success('操作成功');
        }else $this->error('操作失败');
    }


    //预支申请审核列表
    public function get_advance(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\Advance;
        

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['agent_user_id'=>$user_id,'status'=>0])->limit($limit)->select();
        foreach ($list as $key => &$val) {
            if ($val['status']==0) {
                $val['statusName'] = "待初审";
            }else if($val['status']==1){
                $val['statusName'] = "待复审";
            }else if($val['status']==3){
                $val['statusName'] = "审核通过";
            }else if($val['status']==2){
                $val['statusName'] = "审核未通过";
            }
            $val['createtime'] = date('Y-m-d',$val['createtime']);
        }
        
        $data['list'] = $list;
        $this->success('',$data);
    }

    //审批操作
    public function advance_check(){
        $id = $this->request->post('id');
        $status = $this->request->post('status',0);
        $remark = $this->request->post('remark','');

        $model = new \app\admin\model\Advance;
        $info = $model->get($id);
        if (!$info || $info->status!=0) {
            $this->error('该申请已被审核，不能重复审核');
        }
        $info->status = $status;
        $info->result = $remark;
        $rs = $info->save();
        if ($rs) {
            $this->success('操作成功');
        }else $this->error('操作失败');
    }

    //扫描邀请码上报信息
    public function invite_add(){
        $row = $this->request->post();
        $user_id = $this->auth->id;
        //查看详情
        $info = \app\admin\model\InviteList::get(['user_id'=>$user_id]);
        if ($info) {
            $this->error('已上报，无需重复上报');
        }
        
        $row['user_id'] = $user_id;
        $row['createtime'] = time();
        $active_m = new \app\admin\model\InviteList;
        $rs = $active_m->allowField(true)->save($row);
        if ($rs) {
            //加积分
            $u_mod = new User();
            $u_mod->addScore($user_id,10,'扫码上报个人信息');
            $this->success('上报成功');
        }else $this->error('上报失败');
    }

    //工作任务
    public function work_list(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $agentInfo = \app\admin\model\Agent::get(['user_id'=>$user_id]);
        if (!$agentInfo) {
            $this->error('用户信息获取失败');
        }
        $wMod = new \app\admin\model\RecruitWork;
        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $wMod->field('id,city_id,service_id,labor_id,company_id,name,price,price2,address,start_time,end_time')->where(['labor_id'=>$agentInfo['labor_id']])->limit($limit)->order('id desc')->select();
        foreach ($list as $key => &$val) {
            $val['start_date'] = date('Y-m-d',$val['start_time']);
            $val['end_date'] = date('Y-m-d',$val['end_time']);
        }

        $data['list'] = $list;
        $this->success('',$data);
    }

    //添加人员选择列表
    public function select_user_list(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');
        $Re_input = $this->request->post('Re_input');

        $user_id = $this->auth->id;
        $agentInfo = \app\admin\model\Agent::get(['user_id'=>$user_id]);
        if (!$agentInfo) {
            $this->error('用户信息获取失败');
        }
        if ($Re_input != '') {
            $Re_input = " (name like '%$Re_input%' or phone like '%$Re_input%') ";
        } 
        $bMod = new \app\admin\model\ActiveBmlist;
        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bMod->where(['status'=>2,'labor_id'=>$agentInfo['labor_id']])->where($Re_input)->limit($limit)->order('id desc')->select();
        
        $this->success('',$list);
    }

    //确认选择
    public function sure_select(){
        $work_id = $this->request->post('work_id');
        $ids = $this->request->post('bm_ids');
        if (!intval($work_id)) {
            $this->error('请选择对应工作任务'); 
        }
        if (!$ids) {
           $this->error('请选择人员'); 
        }
        $ids = explode(',', $ids);
        $company_id = db('recruit_work')->where('id='.$work_id)->value('company_id');
        $result = false;
        Db::startTrans();
        $list = db('active_bmlist')->where('status=2')->where('id', 'in', $ids)->select();
        $recodList = [];
        try {
            foreach ($list as $key => $val) {
                $result = db('active_bmlist')->where('id='.$val['id'])->update(['status'=>3,'work_id'=>$work_id,'work_time'=>time()]);
                $recodList[$key] = [
                    'user_id'         => $val['user_id'],
                    'bm_id'           => $val['id'],
                    'agent_user_id'   => $val['agent_user_id'],
                    'company_id'      => $company_id,
                    'name'            => $val['name'],
                    'phone'           => $val['phone'],
                    'work_id'         => $work_id,
                    'city_id'         => $val['city_id'],
                    'service_id'      => $val['service_id'],
                    'labor_id'        => $val['labor_id'],
                    'work_time'       => time()
                ];
            }
            $model = new \app\admin\model\WorkRecod; 
            $result = $model->allowField(true)->saveAll($recodList);
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }

        if ($result) {
            $this->success('操作成功');
        }else{
            $this->error('操作失败');
        }
              
    }

    //查看挂钩人员
    public function work_user_list(){
        $work_id = $this->request->post('work_id');
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');
        $Re_input = $this->request->post('Re_input');
        if (!intval($work_id)) {
            $this->error('请选择对应工作任务'); 
        }
        if ($Re_input != '') {
            $Re_input = " (name like '%$Re_input%' or phone like '%$Re_input%') ";
        }
        $bMod = new \app\admin\model\ActiveBmlist;
        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bMod->where(['status'=>3,'work_id'=>$work_id])->where($Re_input)->limit($limit)->order('work_time desc')->select();
        
        $this->success('',$list);
    }

    //解绑
    public function unbind(){
        $ids = $this->request->post('bm_ids');
        if (!$ids) {
           $this->error('请选择人员'); 
        }
        $ids = explode(',', $ids);
        Db::startTrans();
        $list = db('active_bmlist')->where('status=3')->where('id', 'in', $ids)->select();
        $rs = [];
        try {
            foreach ($list as $key => $val) {
                $rs = db('active_bmlist')->where('id='.$val['id'])->update(['status'=>2,'unbind_time'=>time()]);
                $rs2 = db('work_recod')->where(['user_id'=>$val['user_id'],'bm_id'=>$val['id']])->order('id desc')->limit(1)->update(['unbind_time'=>time()]);
            }
            Db::commit();
        } catch (PDOException $e) {
            Db::rollback();
            $this->error($e->getMessage());
        } catch (Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        
        if ($rs) {
            $this->success('操作成功');
        }else{
            $this->error('操作失败');
        }    
    }
    //查看合同状态
    public function getContract(){
        $bm_id = $this->request->post('bm_id');
        if (!$bm_id) {
            $this->error('参数错误');
        }
        $cMod = new \app\admin\model\Contract;
        $info = $cMod->where(['bm_id'=>$bm_id])->order('id desc')->find();
        if ($info) {
            $status = ['待签署','待签署','部分签署','已拒签','已签署','已过期','已撤销'];
            $info['statusName'] = isset($status[$info['status']])?$status[$info['status']]:'待签署';
        }
        $this->success('',$info);
    }
    //创建电子合同
    public function createContract(){
        $cdata = $this->request->post();
        if (!$cdata['bm_id']) {
            $this->error('参数错误');
        }
        if (!$cdata['company_name']) {
            $this->error('公司名称不能为空');
        }
        if (!$cdata['address']) {
            $this->error('公司名称不能为空');
        }
        if (!$cdata['start_date']) {
            $this->error('开始不能为空');
        }
        if (!$cdata['end_date']) {
            $this->error('结束不能为空');
        }
        $bmMod = new \app\admin\model\ActiveBmlist;
        $info = $bmMod->get($cdata['bm_id']);
        if (!$info || $info->status!=1) {
            $this->error('已签合同，无需重新操作');
        }
        $cMod = new \app\admin\model\Contract;
        //创建流程对象
        $config = [
            'secretId' => 'AKIDfBVcUAdsseCWbU9anlUzFH68xYx98uSl', 
            'secretKey' => '81f4TgeNs8AxYw61jW7WjkaBXfYDdo8M',
            'userId' => 'yDRsdUU1d2yvpUt1KaoRp9wmGhqCQGSs',
        ];
        $this->tencentEss = new TencentEss;
        //创建签署流程
        $data = [
            'companyInfo' => ['organizationName'=>'八冈信息技术（广东）有限公司','type'=>0,'name'=>'李威','mobile'=>'18588730193'],
            'personInfo' => ['type'=>1,'name'=>$info['name'],'mobile'=>$info['phone']],
            'flowName' => $info['name'].'的合同',
            'deadLine' => time() + 7 * 24 * 3600,  // 请设置合理的时间，否则容易造成合同过期
            'unordered' => true,//true无序  false有序
        ];
        try {
            $res = $this->tencentEss->createFlow($data);
            $res = json_decode($res,true);
            $flowId = $res['FlowId'];
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        //创建电子文档
        $data = [
            'flowId' => $flowId,
            'templateId' => 'yDRsqUU1ylandUEHiFlBBSwr2A0q91f1',
            'componentNames' => [
                //['name' => '企业全称','value' => '八冈信息技术（广东）有限公司'],
                //['name' => '统一社会信用代码','value' => '91441203MA57B9UB8Q'],
                ['name' => '劳务派遣许可证编号','value' => '1111111'],
                ['name' => '开始日期','value' => $cdata['start_date']],
                ['name' => '终止日期','value' => $cdata['end_date']],
                ['name' => '公司名称','value' => $cdata['company_name']],
                ['name' => '工作地点','value' => $cdata['address']],
                ['name' => '白班时薪','value' => $cdata['price']],
                ['name' => '夜班时薪','value' => $cdata['price2']],
            ]
        ];
        try {
            $res = $this->tencentEss->createDocument($data);
            $res = json_decode($res,true);
            $documentId = $res['DocumentId'];
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        //发起流程
        //文档创建是异步的，确保发起流程前已经创建完成这里睡眠1秒
        sleep(1);
        try {
            $res = $this->tencentEss->startFlow($flowId);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        //发起成功保持到数据库
        $cdata['flow_id'] = $flowId;
        $cdata['document_id'] = $documentId;
        $cdata['name'] = $info['name'];
        $cdata['mobile'] = $info['phone'];
        $cMod->allowField(true)->save($cdata);
        $this->success('合同发起成功');
    }
    //撤销合同
    public function cancelFlow(){
        $flow_id = $this->request->post('flow_id');
        if (!$flow_id) {
            $this->error('参数错误');
        }
        $this->tencentEss = new TencentEss;
        try {
            $res = $this->tencentEss->cancelFlow($flow_id,'撤销合同');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('撤销成功');
    }
    //添加劳务工
    public function add_worker(){
        $data = $this->request->post();
        if (!$data['name']) {
            $this->error('姓名必须');
        }
        if (!$data['phone']) {
            $this->error('电话必须');
        }
        if (!$data['idcard']) {
            $this->error('身份证必须');
        }
        if (!$data['birthday'] || $data['birthday']=='请选择出生日期') {
            $this->error('请选择出生日期');
        }
        if (!$data['gender']) {
            $this->error('请选择性别');
        }
        if (!$data['mz']) {
            $this->error('请输入民族');
        }
        if (!$data['idcard_img_front']) {
            $this->error('请上传身份证正面照片');
        }
        if (!$data['idcard_img_backend']) {
            $this->error('请上传身份证反面照片');
        }
        $user_id = db('user')->where(['mobile'=>$data['phone']])->value('id');
        if (!$user_id) {
           //注册会员 
            $salt = Random::alnum();
            $password = $data['phone'];
            $udata = [
                'group_id'   => 1,
                'username'   => $data['name'],
                'nickname'   => $data['name'],
                'mobile'     => $data['phone'],
                'idcard'     => $data['idcard'],
                'createtime' => time(),
                'jointime' => time(),
                'salt'       => $salt,
                'password'   => md5(md5($password) . $salt),
                'status'     => 'normal',
                'birthday' => $data['birthday'],
                'gender' => $data['gender'],
                'mz' => $data['mz'],
                'idcard_img_front' => $data['idcard_img_front'],
                'idcard_img_backend' => $data['idcard_img_backend'],
                'identify_status' => 1,//已审核
            ];
            $rs = db('user')->insert($udata);
            if (!$rs) {
                $this->error('会员自动注册失败！');
            }
            $user_id = db('user')->getLastInsID();//insertGetId
        }
        $data['labor_id'] = db('agent')->where(['user_id'=>$this->auth->id])->value('labor_id');
        $data['status'] = 1;
        $data['agent_user_id'] = $this->auth->id;
        $data['user_id'] = $user_id;
        $data['city_id'] = 0;
        $data['service_id'] = 0;
        //判断用户是否已经存在档案
        $bMmodel = new \app\admin\model\ActiveBmlist;
        $info = $bMmodel->where('status>-1 and user_id='.$data['user_id'])->find();
        if ($info) {
            $this->error('该档案已经存在，不能重复添加');
        }
        $result = $bMmodel->allowField(true)->save($data);
        if ($result) {
            $this->success('添加成功');
        }else{
            $this->error('添加失败！');
        }
    }
}
