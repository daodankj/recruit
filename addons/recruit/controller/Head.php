<?php

namespace addons\recruit\controller;


/**
 * 招募活动
 */
class Head extends Base
{

    protected $noNeedLogin = [];

    public function _initialize()
    {
        parent::_initialize();
    }



    //获取自己申请信息
    public function apply_info(){
        $user_id = $this->auth->id;
        $info = \app\admin\model\RecruitHead::get(['user_id'=>$user_id]);
        if ($info) {
            if ($info['idcard_img_front']) {
                $info['idcard_img_front'] = cdnurl($info['idcard_img_front'],true);
            }
            if ($info['idcard_img_backend']) {
                $info['idcard_img_backend'] = cdnurl($info['idcard_img_backend'],true);
            }
        }

        $this->success('',$info);
    }
    /**
     * 团长申请
     */
    public function apply()
    {
        //首先判断 是否已经申请
        $user_id = $this->auth->id;
        $info = \app\admin\model\RecruitHead::get(['user_id'=>$user_id]);
        if($info){
            $this->error('您已经申请，不能重复申请');
        }
        //判断是不是推广员，推广员不能申请
        //暂时只有推广员才能申请
        $isgent = db('agent')->where('user_id='.$user_id)->value('id');
        if(!$isgent){
            $this->error('您不是推广员，不能申请团长');
        }
        $row = $this->request->post();
        $head = new \app\admin\model\RecruitHead;
        $rs = $head->allowField(true)->save($row);
        if ($rs) {
            $this->success('申请成功');
        }else $this->error('申请失败');
        
    }

    //团长获取招募活动
    public function get_active(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');
        $user_id = $this->auth->id;

        $active_m = new \app\admin\model\RecruitActive;
        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $now_date = date('Y-m-d');
        $activeList = $active_m->field('id,name,user_num,settlement_type,address,company_name,price,worktime,start_date,end_date,labor_id')
                    ->where('start_date<="'.$now_date.'" and end_date>="'.$now_date.'"')
                    ->where(['head_open'=>1])
                    ->order('id desc')
                    ->limit($limit)
                    ->select();
        foreach ($activeList as $key => &$val) {
            $val['settlement_type'] = $val['settlement_type']==1?"月结":($val['settlement_type']==2?"周结":"日结");
            //判断是否参团
            $join_info = db('headjoin_apply')->where(['active_id'=>$val['id'],'user_id'=>$user_id])->find();
            if ($join_info) {
                $val['join_status']= $join_info['status'] ;
            }else{
                $val['join_status']= -1 ;
            }
        }

        $this->success('', $activeList);
    }

    //活动参团申请
    public function headjoin_apply(){
        $active_id = $this->request->post('active_id');
        //首先判断 是否已经申请
        $user_id = $this->auth->id;

        $headjoin_m = new \app\admin\model\HeadjoinApply;

        $info = $headjoin_m->get(['active_id'=>$active_id,'user_id'=>$user_id]);
        if($info){
            $this->error('您已经申请，不能重复申请');
        }
        //只能同时申请3个团，进行中的
        $now_date = date('Y-m-d');//获取还没结束的参团个数
        $num = $headjoin_m->with('active')->where('headjoin_apply.user_id='.$user_id.' and active.end_date>'.$now_date)->count();
        //获取最大参团个数
        $max_num = config('site.headjoin_num');
        if ($num>=$max_num) {
            $this->error('您当前已有'.$num.'个进行中的参团，不能再参团');
        }
        $row = $this->request->post();
        
        $rs = $headjoin_m->allowField(true)->save($row);
        if ($rs) {
            $this->success('申请成功');
        }else $this->error('申请失败');
    }
}
