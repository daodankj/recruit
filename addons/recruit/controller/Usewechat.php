<?php

namespace addons\recruit\controller;

use EasyWeChat\Factory;
use addons\third\model\Third;

/**
 * 首页
 */
class Usewechat extends Base
{
    protected $noNeedLogin = '*';

    protected $miniProgram = null;
    protected $wxapp = 'wxapp';//标记来自哪个小程序,默认八戒求才

    public function _initialize()
    {
        parent::_initialize();

        $config = get_addon_config('recruit');
        //新增了一个小程序，后台没的配置，这里单独配置
        $this->wxapp = $this->request->param('wxapp','wxapp');
        if ($this->wxapp == 'wxapp2') {//八戒求才服务端
            $config['wxappid'] = 'wxb52b3d7b2144373c';
            $config['wxappsecret'] = 'a36a96f7d8729682baa720fa7335fad4';
        }
        $options = [
            'app_id' => $config['wxappid'],
            'secret' => $config['wxappsecret'],

            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',

            'log' => [
                'level' => 'debug',
                'file' => __DIR__.'/wechat.log',
            ],
        ];

        $app = Factory::miniProgram($options);
        $this->miniProgram = $app;
    }

    public function index()
    {
        $uid = $this->request->param('uid',0);
        $env_version = $this->request->param('env_version','release');//正式版为 release，体验版为 trial，开发版为 develop
        //$img = $this->miniProgram->qrcode->appCodeUnlimit(14, 'page/zh_index/index', 418, true, null, true);
        //return response($img, 418)->contentType("image/jpg");
        $img = $this->miniProgram->app_code->getUnlimit($uid, [
            'page'  => 'page/zh_index/index',
            'width' => 418,
            'auto_color' => true,
            'env_version' => $env_version
        ]);
        return response($img, 418)->contentType("image/jpg");
    }

    public function get_resume_QrPng()
    {
        $id = $this->request->param()['id'];
        //$img = $this->miniProgram->qrcode->appCodeUnlimit($id, 'page/zh_resume/ShowOneResume', 418, true, null, true);
        //return response($img, 418)->contentType("image/jpg");
        $img = $this->miniProgram->app_code->getUnlimit($id, [
            'page'  => 'page/zh_resume/ShowOneResume',
            'width' => 418,
            'auto_color' => true,
        ]);
        return response($img, 418)->contentType("image/jpg");
    }

    public function get_Job_QrPng()
    {
        $id = $this->request->param()['id'];
        //$img = $this->miniProgram->qrcode->appCodeUnlimit($id, 'page/zh_Jobs/ShowOneJob', 418, true, null, true);
        //return response($img, 418)->contentType("image/jpg");
        $img = $this->miniProgram->app_code->getUnlimit($id, [
            'page'  => 'page/zh_Jobs/ShowOneJob',
            'width' => 418,
            'auto_color' => true,
        ]);
        return response($img, 418)->contentType("image/jpg");
    }

    public function get_News_QrPng()
    {
        $id = $this->request->param()['id'];
        //$img = $this->miniProgram->qrcode->appCodeUnlimit($id, 'page/zh_news/index', 418, true, null, true);
        $img = $this->miniProgram->app_code->getUnlimit($id, [
            'page'  => 'page/zh_news/index',
            'width' => 418,
            'auto_color' => true,
        ]);
        return response($img, 418)->contentType("image/jpg");
    }

    public function get_PhoneNum()
    {
        $encryptedData = $this->request->post("encryptedData");
        $iv = $this->request->post("iv");

        $third = Third::where(['user_id' => $this->auth->id, 'platform' => $this->wxapp])->find();
        $sessionKey = $third['access_token'];

        //$this->success('', $this->miniProgram->encryptor->decryptData($sessionKey, $iv, $encryptedData));
        $this->success('', $this->miniProgram->encryptor->decryptData($sessionKey, $iv, $encryptedData));
    }

    


}