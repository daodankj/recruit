<?php

namespace addons\recruit\controller;

use addons\recruit\model\News;
/**
 * 技能培训
 */
class Train extends Base
{

    protected $noNeedLogin = [];

    public function _initialize()
    {
        parent::_initialize();
    }



    //获取自己申请信息
    public function get_index(){
        $bannerList = [];

        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');
        $Re_input = $this->request->post('Re_input');

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        //第一页才加载，防止翻页无用加载，让费资源
        if ($page==1) {
            $list = News::getBannerList(['type' => 'train','status' => 'normal', 'row' => 5, 'cache' => 0]);
            foreach ($list as $index => $item) {
                $bannerList[] = ['image' => cdnurl($item['image'], true), 'url' => '/', 'title' => $item['title'], 'name' => $item['name'], 'id' => $item['id']];
            }
        }  

        if ($Re_input != '') {
            $Re_input = " (name like '%$Re_input%'or intro like '%$Re_input%') ";
        }          

        $train_m = new \app\admin\model\Train;
        $tlist = $train_m->field('id,image,name,address,start_time,intro')
            ->where($Re_input)
            ->order('id desc')->limit($limit)->select();
        foreach ($tlist as $key => &$val) {
            $val['image'] = cdnurl($val['image'], true);
        }

        $this->success('',['bannerList'=>$bannerList,'list'=>$tlist]);
    }

    //获取活动详情
    public function info(){
        $id = $this->request->post('id');

        $user_id = $this->auth->id;
        $activeInfo = \app\admin\model\Train::get($id);
        if(empty($activeInfo)){
            $this->error('该培训活动不存在');
        }
        //判断 是否报名
        $baoming = \app\admin\model\TrainBm::where(['train_id'=>$id,'user_id'=>$user_id])->find();
        $activeInfo['isBaoming'] = empty($baoming)?0:1;
        if ($activeInfo['image']) {
            $activeInfo['image'] = cdnurl($activeInfo['image'], true);
        }
        //详情内容中的图片地址加上域名
        $activeInfo['content'] = preg_replace_callback('/<[img|IMG].*?src=[\'| \"](?![http|https])(.*?(?:[\.gif|\.jpg]))[\'|\"].*?[\/]?>/', function ($r) {
                    $str = 'https://'.$_SERVER['HTTP_HOST'].$r[1];
                    return str_replace($r[1], $str, $r[0]);
                }, $activeInfo['content']);
        
        
        $this->success('', $activeInfo);
    }

    //活动报名
    public function add_baoming(){
        $row = $this->request->post();
        $user_id = $this->auth->id;
        //查看详情
        $info = \app\admin\model\Train::get($row['train_id']);
        if (!$info) {
            $this->error('该培训不存在');
        }
        //判断 是否报名
        $is_baoming = \app\admin\model\TrainBm::where(['train_id'=>$row['train_id'],'user_id'=>$user_id])->count();
        if ($is_baoming) {
            $this->error('您已经报过名，不能重复报名');
        }
        //判断是否需要支付金额,如果不需要状态默认为已支付
        if ($info->price==0) {
            $row['status'] = 1;
        }
        $row['user_name'] = $row['name'];
        $row['create_time'] = time();
        $active_m = new \app\admin\model\TrainBm;
        $rs = $active_m->allowField(true)->save($row);
        if ($rs) {
            $this->success('报名成功');
        }else $this->error('报名失败');

        $this->success('');
    }
}
