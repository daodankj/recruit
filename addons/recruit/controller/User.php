<?php

namespace addons\recruit\controller;

use addons\recruit\library\Service;
use addons\third\model\Third;
use app\common\library\Auth;
use fast\Http;
use think\Config;
use think\Validate;
use \think\Log;

/**
 * 会员
 */
class User extends Base
{

    protected $noNeedLogin = ['index', 'login', 'login_hawk', 'Updata_user_hawk', 'login_debug','login_password','login_hawk2','getXy','sure_xy','bindMobile2','getCode'];

    protected $token = '';
    protected $wxapp = 'wxapp';//标记来自哪个小程序,默认八戒求才

    public function _initialize()
    {
        //新增了一个小程序，后台没的配置，这里单独配置
        $this->wxapp = $this->request->param('wxapp','wxapp');
        $this->token = $this->request->post('token');
        if ($this->request->action() == 'login' && $this->token) {
            $this->request->post(['token' => '']);
        }
        parent::_initialize();

        if (!Config::get('fastadmin.usercenter')) {
            $this->error(__('User center already closed'));
        }

        $ucenter = get_addon_info('ucenter');
        if ($ucenter && $ucenter['state']) {
            include ADDON_PATH . 'ucenter' . DS . 'uc.php';
        }

    }
    //授权绑定手机号码
    public function bind_PhoneNum(){
        $mobile = $this->request->post("phone");
        if (!$mobile || !$this->token) {
            $this->error('手机号有误');
        }
        $this->auth->init($this->token);
        //检测是否登录
        if ($this->auth->isLogin()) {
            $user = $this->auth->getUser();
            $fields['mobile'] = $mobile;
            $user->save($fields);
            $this->success("绑定成功", ['userInfo' => $this->auth->getUserinfo()]);
        } else {
            $this->error("未登录状态");
        }
    }

    public function Updata_user_hawk()
    {
        $userInfo = $this->request->post("userInfo", '', 'trim');
        $mobile = $this->request->post("mobile");
        if (!$userInfo || !$this->token) {
            $this->error("参数不正确");
        }

        $this->auth->init($this->token);
        //检测是否登录
        if ($this->auth->isLogin()) {
            $user = $this->auth->getUser();
            $fields = [];
            $userInfo = json_decode($userInfo, true);
            $fields['avatar'] = $userInfo['avatarUrl'];
            $fields['nickname'] = $userInfo['nickName'];
            $fields['mobile'] = $mobile;
            $user->save($fields);
            //
            $third = Third::where(['user_id' => $this->auth->id,'platform' => $this->wxapp])->find();
            $third->openname = $userInfo['nickName'];
            $third->save();
            $this->success("已经登录", ['userInfo' => $this->auth->getUserinfo()]);
        } else {
            $this->error("未登录状态");
        }
    }

    public function login_hawk()
    {
        $config = get_addon_config('recruit');
        $code = $this->request->post("code");
        $scene = $this->request->post("scene",0);//推广人id
        if (!$code) {
            $this->error("参数不正确");
        }
        if ($this->wxapp == 'wxapp2') {//八戒求才服务端
            $config['wxappid'] = 'wxb52b3d7b2144373c';
            $config['wxappsecret'] = 'a36a96f7d8729682baa720fa7335fad4';
        }

        $params = [
            'appid' => $config['wxappid'],
            'secret' => $config['wxappsecret'],
            'js_code' => $code,
            'grant_type' => 'authorization_code'
        ];
        $result = Http::sendRequest("https://api.weixin.qq.com/sns/jscode2session", $params, 'GET');
        if ($result['ret']) {
            $json = (array)json_decode($result['msg'], true);
            //$json = ['openid' => 'test', 'session_key' => 'test'];
            if (isset($json['openid'])) {
                //如果有传Token
                if ($this->token) {
                    $this->auth->init($this->token);
                    //检测是否登录
                    if ($this->auth->isLogin()) {
                        $third = Third::where(['openid' => $json['openid'], 'platform' => $this->wxapp])->find();
                        if ($third && $third['user_id'] == $this->auth->id) {
                            //把最新的 session_key 保存到 第三方表的 access_token 里
                            $third['access_token'] = $json['session_key'];
                            $third->save();
                            $this->success("登录成功", $this->Format_avatarUrl($this->auth->getUserinfo()));
                        }
                    }
                }

                $platform = $this->wxapp;
                $result = [
                    'openid' => $json['openid'],
                    'userinfo' => [
                        'nickname' => '游客未登录',
                    ],
                    'access_token' => $json['session_key'],
                    'refresh_token' => '',
                    'expires_in' => isset($json['expires_in']) ? $json['expires_in'] : 0,
                    'invite_id' => intval($scene)
                ];
                $extend = ['mobile' => 'NoLoginData', 'gender' => '0', 'nickname' => '游客未登录', 'avatar' => '/assets/img/avatar.png'];
                $ret = Service::connect_hawk($platform, $result, $extend);
                if ($ret) {
                    $auth = Auth::instance();
                    $this->success("登录成功", $this->Format_avatarUrl($this->auth->getUserinfo()));
                } else {
                    $this->error("连接失败");
                }
            } else {
                $this->error("登录失败", $json);
            }
        }

        return;
    }

    /**
     * 登录
     */
    public function login()
    {
        $config = get_addon_config('recruit');
        $code = $this->request->post("code");
        $rawData = $this->request->post("rawData");
        if (!$code || !$rawData) {
            $this->error("参数不正确");
        }
        if ($this->wxapp == 'wxapp2') {//八戒求才服务端
            $config['wxappid'] = 'wxb52b3d7b2144373c';
            $config['wxappsecret'] = 'a36a96f7d8729682baa720fa7335fad4';
        }
        $userInfo = (array)json_decode($rawData, true);

        $params = [
            'appid' => $config['wxappid'],
            'secret' => $config['wxappsecret'],
            'js_code' => $code,
            'grant_type' => 'authorization_code'
        ];
        $result = Http::sendRequest("https://api.weixin.qq.com/sns/jscode2session", $params, 'GET');
        if ($result['ret']) {
            $json = (array)json_decode($result['msg'], true);
            if (isset($json['openid'])) {
                //如果有传Token
                if ($this->token) {
                    $this->auth->init($this->token);
                    //检测是否登录
                    if ($this->auth->isLogin()) {
                        $third = Third::where(['openid' => $json['openid'], 'platform' => $this->wxapp])->find();
                        if ($third && $third['user_id'] == $this->auth->id) {
                            $this->success("登录成功", ['userInfo' => $this->auth->getUserinfo()]);
                        }
                    }
                }

                $platform = $this->wxapp;
                $result = [
                    'openid' => $json['openid'],
                    'userinfo' => [
                        'nickname' => $userInfo['nickName'],
                    ],
                    'access_token' => $json['session_key'],
                    'refresh_token' => '',
                    'expires_in' => isset($json['expires_in']) ? $json['expires_in'] : 0,
                ];
                $extend = ['gender' => $userInfo['gender'], 'nickname' => $userInfo['nickName'], 'avatar' => $userInfo['avatarUrl']];
                $ret = Service::connect($platform, $result, $extend);
                if ($ret) {
                    $auth = Auth::instance();
                    $this->success("登录成功", ['userInfo' => $auth->getUserinfo()]);
                } else {
                    $this->error("连接失败");
                }
            } else {
                $this->error("登录失败");
            }
        }

        return;
    }

    //账号密码登入
    public function login_password(){
        $mobile = $this->request->post("mobile");
        $password = $this->request->post("password");
        if (!$mobile || !$password) {
            $this->error("手机号码和密码不能为空");
        }
        $auth = \app\common\library\Auth::instance();
        $ret = $auth->login($mobile, $password);
        if ($ret) {
            $data = ['userInfo' => $auth->getUserinfo()];
            $this->success(__('Logged in successful'), $data);
        } else {
            $this->error($auth->getError());
        }
    }

    /**
     * 绑定账号
     */
    public function bind()
    {
        $account = $this->request->post("account");
        $password = $this->request->post("password");
        if (!$account || !$password) {
            $this->error("参数不正确");
        }

        $account = $this->request->post('account');
        $password = $this->request->post('password');
        $rule = [
            'account' => 'require|length:3,50',
            'password' => 'require|length:6,30',
        ];

        $msg = [
            'account.require' => 'Account can not be empty',
            'account.length' => 'Account must be 3 to 50 characters',
            'password.require' => 'Password can not be empty',
            'password.length' => 'Password must be 6 to 30 characters',
        ];
        $data = [
            'account' => $account,
            'password' => $password,
        ];
        $validate = new Validate($rule, $msg);
        $result = $validate->check($data);
        if (!$result) {
            $this->error(__($validate->getError()));
            return FALSE;
        }
        $field = Validate::is($account, 'email') ? 'email' : (Validate::regex($account, '/^1\d{10}$/') ? 'mobile' : 'username');
        $user = \app\common\model\User::get([$field => $account]);
        if (!$user) {
            $this->error('账号未找到');
        }
        $third = Third::where(['user_id' => $user->id, 'platform' => $this->wxapp])->find();
        if ($third) {
            $this->error('账号已经绑定其他小程序账号');
        }

        $third = Third::where(['user_id' => $this->auth->id, 'platform' => $this->wxapp])->find();
        if (!$third) {
            $this->error('未找到登录信息');
        }

        if ($this->auth->login($account, $password)) {
            $third->user_id = $this->auth->id;
            $third->save();
            $this->success("绑定成功", ['userInfo' => $this->auth->getUserinfo()]);
        } else {
            $this->error($this->auth->getError());
        }
    }

    /**
     * 个人资料
     */
    public function profile()
    {
        $user = $this->auth->getUser();
        $username = $this->request->request('username');
        $nickname = $this->request->request('nickname');
        $bio = $this->request->request('bio');
        $avatar = $this->request->request('avatar');
        if (!$username || !$nickname) {
            $this->error("用户名和昵称不能为空");
        }
        $exists = \app\common\model\User::where('username', $username)->where('id', '<>', $this->auth->id)->find();
        if ($exists) {
            $this->error(__('Username already exists'));
        }
        $avatar = str_replace(Config::get('upload.cdnurl'), '', $avatar);
        $user->username = $username;
        $user->nickname = $nickname;
        $user->bio = $bio;
        $user->avatar = $avatar;
        $user->save();
        $this->success('', ['userInfo' => $this->auth->getUserinfo()]);
    }

    /*
    更新用户组别
    */
    public function group()
    {
        $user = $this->auth->getUser();
        $group = $this->request->request('type');
        //切换成推广员和团长判断是否是推广员和团长
        if ($group==3) {//推广员
            $agent = db('agent')->where('status=1 and user_id='.$user->id)->value('id');
            if (!$agent) {
                $this->error("您还不是推广员，无法切换");
            }
        }else if($group==4){//团长
            $head = db('recruit_head')->where('status=1 and user_id='.$user->id)->value('id');
            if (!$head) {
                $this->error("您还不是团长，无法切换");
            }
        }
        $user->group_id = $group;
        $user->save();
        $this->success('');
    }


    /**
     * 保存头像
     */
    public function avatar()
    {
        $user = $this->auth->getUser();
        $avatar = $this->request->request('avatar');
        if (!$avatar) {
            $this->error("头像不能为空");
        }
        $avatar = str_replace(Config::get('upload.cdnurl'), '', $avatar);
        $user->avatar = $avatar;
        $user->save();
        $this->success('', $this->Format_avatarUrl($this->auth->getUserinfo()));
    }

    private function startsWith($str, $prefix)
    {
        for ($i = 0; $i < strlen($prefix); ++$i) {
            if ($prefix[$i] !== $str[$i]) {
                return false;
            }
        }
        return true;
    }

    private function Format_avatarUrl($userInfo)
    {
        $avatar = $userInfo['avatar'];
        if ($this->startsWith($avatar, "/uploads/")) {
            $userInfo['avatar'] = cdnurl($avatar, true);
        }
        //角色分开
        if ($this->wxapp == 'wxapp2') {
            if(!in_array($userInfo['group_id'], [1,3])){
                $userInfo['group_id'] = 1;
            }
        }else{
            if(!in_array($userInfo['group_id'], [1,2])){
                $userInfo['group_id'] = 1;
            }
        }
        return ['userInfo' => $userInfo];
    }

    public function getCode(){
        $sms = new \app\api\controller\Sms;
        $sms->send();
    }

    public function bindMobile(){
        $mobile = $this->request->post("mobile");
        $event = $this->request->post("event");
        $event = $event ? $event : 'register';
        $captcha = $this->request->post("captcha");
        $smslib = new \app\common\library\Sms;
        $ret = $smslib::check($mobile, $captcha, $event);
        if ($ret) {
            if ($this->auth->isLogin()) {
                $user = $this->auth->getUser();
                //检查手机号码是否注册，已注册直接更改绑定
                $this->bindUser($mobile,$user,$smslib,$event);
                $user->mobile = $mobile;
                $user->save();
                $smslib::flush($mobile, $event);
                //$this->success(__('成功'));
                $this->success("绑定成功", ['userInfo' => $this->auth->getUserinfo()]);
            } else {
                $this->error("未登录状态");
            }
        } else {
            $this->error(__('验证码不正确'));
        }
    }

    //绑定手机号码判断手机号码是否注册，已注册这绑定到新会员
    private function bindUser($mobile,$user,$smslib,$event){
        $userid = db('user')->where(['mobile'=>$mobile])->value('id');
        if (!$userid) {
            return false;
        }
        $third = db('third')->where(['user_id'=>$user->id,'platform' => $this->wxapp])->update(['user_id'=>$userid]);//变更绑定
        if ($third) {//登入新用户信息，更新下用户昵称头像信息
            $auth = Auth::instance();
            $auth->direct($userid);
            //久的昵称和头像
            $avatar = $user->avatar;
            $user = $this->auth->getUser();//获取新用户
            $user->avatar = $avatar;
            $user->save();
            $smslib::flush($mobile, $event);
            $this->success("绑定成功", ['userInfo' => $this->auth->getUserinfo()]);
        }else{
            $this->error('绑定失败');
        }
        exit;
    }

    //实名认证
    public function identify(){
        $row = $this->request->post();
        if ($this->auth->isLogin()) {
            $user = $this->auth->getUser();
            if ($user->mobile==''||$user->mobile=='NoLoginData') {
                $this->error("请先绑定手机号码");
                exit;
            }
            $row['identify_status'] = -1;//待审核状态
            $user->allowField(true)->save($row);
            $this->success("提交成功", ['userInfo' => $this->auth->getUserinfo()]);
        } else {
            $this->error("未登录状态");
        }
    }

    //积分排行榜
    public function score_top(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;

        $model = new \app\admin\model\User;
        $list = $model->field('nickname,score,avatar,prevtime,logintime,jointime')->order('score desc')->limit($limit)->select();
        foreach ($list as $key => &$val) {
            $val['avatar'] = cdnurl($val['avatar'],true);
        }

        $this->success('',['list'=>$list]);
    }

    //--------------------------美丽的分割线-----后端管理小程序独立接口
    //自动登入
    public function login_hawk2()
    {
        $config = get_addon_config('recruit');
        $code = $this->request->post("code");
        $scene = $this->request->post("scene",0);//推广人id
        if (!$code) {
            $this->error("参数不正确");
        }
        if ($this->wxapp == 'wxapp2') {//八戒求才服务端
            $config['wxappid'] = 'wxb52b3d7b2144373c';
            $config['wxappsecret'] = 'a36a96f7d8729682baa720fa7335fad4';
        }

        $params = [
            'appid' => $config['wxappid'],
            'secret' => $config['wxappsecret'],
            'js_code' => $code,
            'grant_type' => 'authorization_code'
        ];
        $openid = '';
        $result = Http::sendRequest("https://api.weixin.qq.com/sns/jscode2session", $params, 'GET');
        if ($result['ret']) {
            $json = (array)json_decode($result['msg'], true);
            //$json = ['openid' => 'test', 'session_key' => 'test'];
            if (isset($json['openid'])) {
                //如果有传Token
                if ($this->token) {
                    $this->auth->init($this->token);
                    //检测是否登录
                    if ($this->auth->isLogin()) {
                        $third = Third::where(['openid' => $json['openid'], 'platform' => $this->wxapp])->find();
                        if ($third && $third['user_id'] == $this->auth->id) {
                            //把最新的 session_key 保存到 第三方表的 access_token 里
                            $third['access_token'] = $json['session_key'];
                            $third->save();
                            $this->success("登录成功", $this->Format_avatarUrl($this->auth->getUserinfo()));
                        }
                    }
                }else{
                    $third = Third::where(['openid' => $json['openid'], 'platform' => $this->wxapp])->find();
                    if ($third && $third['user_id']) {//如果之前绑定过
                        $third['access_token'] = $json['session_key'];
                        $third->save();
                        $auth = Auth::instance();
                        $auth->direct($third['user_id']);
                        $this->success("登录成功", $this->Format_avatarUrl($this->auth->getUserinfo()));
                    }
                }

                $platform = $this->wxapp;
                $time = time();
                $expires_in = isset($json['expires_in']) ? $json['expires_in'] : 0;
                $values = [
                    'platform' => $platform,
                    'openid' => $json['openid'],
                    'openname' => '游客未登录',
                    'access_token' => $json['session_key'],
                    'refresh_token' => '',
                    'expires_in' => $expires_in,
                    'logintime' => $time,
                    'expiretime' => $time + $expires_in,
                ];
                $third = Third::get(['platform' => $platform, 'openid' => $json['openid']]);
                if ($third) {
                    $third->save($values);
                }else{
                    Third::create($values);
                }
                $openid = $json['openid'];
            } 
        }
        $this->error("请先登入", ['openid'=>$openid]);
    }
    //获取隐私协议
    public function getXy(){
        $openid = $this->request->post("openid");
        $user_id = $this->request->post("user_id");
        $work = [];
        if ($user_id) {
            $third = \addons\third\model\Third::where(['user_id' => $user_id,'platform' => $this->wxapp])->find();
            //获取工作任务
            $model = new \app\admin\model\WorkRecod;
            $work = $model->where('unbind_time=0 and user_id='.$this->auth->id)->order('id desc')->find();
        }else{
            $third = \addons\third\model\Third::where(['openid' => $openid,'platform' => $this->wxapp])->find();
        }
        if ($third && $third->is_read==1) {//已读
            $xyinfo = '';
        }else{//未读才返回协议内容
            $xyinfo = db('recruit_news')->where('id=8')->value('content');
        }

        $data = [
            'xyinfo'     => $xyinfo,
            'work' => $work
        ];
        $this->success('', $data);
    }

    //确认协议
    public function sure_xy(){
        $openid = $this->request->post("openid");
        if (!$openid && !$this->auth->id) {
            $this->error('请先登入');
        }
        if ($openid) {
            $third = \addons\third\model\Third::where(['openid' => $openid,'platform' => $this->wxapp])->find();
        }else{
            $third = \addons\third\model\Third::where(['user_id' => $this->auth->id,'platform' => $this->wxapp])->find();
        }
        
        if (!$third) {
            $this->error('操作异常');
        }
        $third->is_read = 1;
        $rs = $third->save();
        if ($rs) {
            $this->success('');
        }else{
            $this->error('操作失败');
        }
    }
    public function bindMobile2(){
        $openid = $this->request->post("openid");
        if (!$openid) {
            $this->error('未获取到您的微信信息，无法绑定！');
        }
        $mobile = $this->request->post("mobile");
        $event = $this->request->post("event");
        $event = $event ? $event : 'register';
        $captcha = $this->request->post("captcha");
        $smslib = new \app\common\library\Sms;
        $ret = $smslib::check($mobile, $captcha, $event);
        if ($ret||($mobile=='18588730193'&&$captcha=='6688')) {//供微信审核人员登入
            $userInfo = db('user')->where(['mobile'=>$mobile])->field('id,nickname')->find();
            if (!$userInfo) {
                $this->error('该用户不存在');
            }
            //判断是否是劳务工
            $workerInfo = db('active_bmlist')->where(['user_id'=>$userInfo['id'],'status'=>['>',1]])->find();
            //判断是否是推广员
            $agent = db('agent')->where(['user_id'=>$userInfo['id'],'status'=>1])->find();
            if (!$workerInfo && !$agent) {
                $this->error('您无权登入');
            }
            $third = db('third')->where(['openid'=>$openid,'platform' => $this->wxapp])->update(['user_id'=>$userInfo['id'],'openname'=>$userInfo['nickname']]);//绑定
            if ($third) {//登入新用户信息，更新下用户昵称头像信息
                $auth = Auth::instance();
                $auth->direct($userInfo['id']);
                $smslib::flush($mobile, $event);
                $this->success("绑定成功", ['userInfo' => $this->auth->getUserinfo()]);
            }else{
                $this->error('绑定失败');
            }
        } else {
            $this->error(__('验证码不正确'));
        }
    }
}
