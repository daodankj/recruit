<?php

namespace addons\recruit\controller;

use app\admin\model\Company;
use app\admin\model\Job;
//use think\Controller;
use app\common\model\Area;
use app\common\model\Version;
use fast\Random;
use think\Config;

use app\common\model\Attachment;


class Companyjob extends Base
{

    protected $noNeedLogin = ['get_c_job', 'get_companyById', 'get_JobList_resrch'];
    protected $noNeedRight = '*';

    private $db_pf = '';
    protected $comxinzn = ['国有企业', '私营企业', '中外合作企业', '中外合资企业', '外商独资企业'];
    protected $ZhusD = ['不提供住宿', '提供住宿', '提供夫妻房'];
    protected $FoodD = ['不提供伙食', '提供午饭', '提供三餐', '有餐补'];
    protected $SafeD = ['不提供社保', '缴纳三险', '缴纳五险', '缴纳五险一金'];
    protected $EducationD = ['无学历要求', '小学', '初中', '高中', '大专', '本科', '研究生及以上'];
    protected $AgeD = ['无要求', '18-30岁', '30-45岁', '45-50岁', '其他'];

    public function _initialize()
    {
        parent::_initialize();
        $this->db_pf = Config::get('database.prefix');
    }

    public function get_companyById()
    {
        $id = $this->request->post('id');
        $CompanyD = Company::get($id);
        if (!$CompanyD) {
            $this->error('不存在企业');
            return;
        }

        $comxinzn = $this->comxinzn;
        $CompanyD['XinZhiname'] = $comxinzn[$CompanyD['xinzhi']];

        //这里把企业对应的 职位全部列表展示出来
        $JobsList = Job::where('c_id', $id)->select();
        $ZhusD = $this->ZhusD;
        $SafeD = $this->SafeD;
        $EducationD = $this->EducationD;
        foreach ($JobsList as $key => &$item) {

            //这里格式化一下 住宿、社保、学历
            $item['zhusuname'] = $ZhusD[$item['stay']];
            $item['Safename'] = $SafeD[$item['safe']];
            $item['Educationname'] = $EducationD[$item['education']];

            //这里需要格式化一下 工资薪水
            if ($item['gold1'] == $item['gold2']) {
                $item['goldtext'] = ($item['gold1'] / 1000) . "K";
                if ($item['gold1'] == 3000) {
                    $item['goldtext'] .= "以下";
                }
                if ($item['gold1'] == 10000) {
                    $item['goldtext'] .= "以上";
                }
            } else {
                $item['goldtext'] = ($item['gold1'] / 1000) . "K-" . ($item['gold2'] / 1000) . "K";
            }
        }
        if (count($JobsList) == 0) {
            $JobsList = null;
        }
        $CompanyD['JobsList'] = $JobsList;


        $this->success('', $CompanyD);
    }

    public function get_my_company()
    {
        $my_id = $this->auth->id;
        $CompanyD = Company::get(['user_id' => $my_id]);
        if (!$CompanyD) {
            $this->error('不存在企业');
            return;
        }
        $comxinzn = $this->comxinzn;
        $CompanyD['XinZhiname'] = $comxinzn[$CompanyD['xinzhi']];
        $CompanyD['cityName'] = db('recruit_opencity')->where(['id'=>$CompanyD['city_id']])->value('city');
        $this->success('', $CompanyD);
    }

    public function get_my_companyName_jobsnum()
    {
        $my_id = $this->auth->id;
        $CompanyD = Company::get(['user_id' => $my_id]);
        if (!$CompanyD) {
            $CompanyD['Id'] = 0;
            $CompanyD['name'] = '';
            $CompanyD['jobCount'] = 0;
            $CompanyD['showrc'] = 0;//审核通过才显示找人才
        } else {
            $CompanyD['jobCount'] = Job::where('c_id', $CompanyD['Id'])->count();
            $CompanyD['showrc'] = $CompanyD['status']==1?1:0;//审核通过才显示找人才
        }
        //这里拖取 resume的简历数据
        $CompanyD['resum_num'] = \app\admin\model\Resume::where(['user_id' => $my_id])->count();

        //这里拖取技能培训报名的次数
        //$JobfairCount = \app\admin\model\Jobfair::where('user_id', $my_id)->count();
        $JobfairCount = \app\admin\model\TrainBm::where('user_id', $my_id)->count();
        $CompanyD['JobfairCount'] = $JobfairCount;
        //这里获取推广员发布的招募活动数
        $activeCount = \app\admin\model\RecruitActive::where('user_id', $my_id)->count();
        $CompanyD['activeCount'] = $activeCount;

        //新增红点标记
        //1工资确认
        $CompanyD['finance_wages'] = db('finance_wages')->where(['user_id'=>$my_id,'status'=>0])->count();
        //2是否有工作任务
        $CompanyD['work_num'] = db('active_bmlist')->where(['user_id'=>$my_id,'status'=>3])->count();
        //3待入职数量
        //处理失效报名,7天之前的
        $bmMod = new \app\admin\model\ActiveBmlist;
        $day_befortime = time() - 7*24*3600;
        $bmMod->where(['status'=>0,'createtime'=>['<',$day_befortime]])->update(['status'=>-1]);
        //计算总数
        $CompanyD['drz_num'] = $bmMod->where(['status'=>0,'agent_user_id'=>$my_id])->count();
        //4团长获取招募活动数量
        $active_m = new \app\admin\model\RecruitActive;
        $now_date = date('Y-m-d');
        $CompanyD['active_num'] = $active_m->where('start_date<="'.$now_date.'" and end_date>="'.$now_date.'"')->where(['head_open'=>1])->count();
        //5未读消息
        $CompanyD['notice_num'] = db('user_notice')->where(['is_read'=>0,'user_id'=>$my_id])->count();

        $this->success('', $CompanyD);
    }

    public function get_JobList_resrch()
    {
        $page = intval($this->request->post('page'));
        $page_block = intval($this->request->post('page_block'));

        $YueXin = $this->request->post('YueXin');
        $XueLi_num = $this->request->post('XueLi_num');
        $ZhuSu_num = $this->request->post('ZhuSu_num');
        $ZhiWei_num = $this->request->post('ZhiWei_num');

        $Re_input = $this->request->post('Re_input');

        $ShowCity_id = $this->request->post('ShowCity_id');
        //格式化 搜索职位和公司名称
        if ($Re_input != '') {
            $Re_input = " (job.name like '%$Re_input%'or recruitcompany.name like '%$Re_input%') ";
        }

        $tmp_m = new Job();

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;

        $cache = false;
        if ($YueXin == 0 && $XueLi_num == 0 && $ZhuSu_num == 0 && $ZhiWei_num==0) {
            $JobsList = $tmp_m->with(['recruitcompany', 'recruitopencity'])->where($Re_input)->where('recruitcompany.city_id', $ShowCity_id)->order($this->db_pf . 'recruit_job.toptime', 'desc')->limit($limit)->cache($cache)->select();
        } else {
            //format 住宿
            $ZhuSu_num = $ZhuSu_num == 0 ? '' : 'stay = ' . $ZhuSu_num;

            $XueLi_num--;
            //新增职位字典查询
            $ZhiWei_num = $ZhiWei_num == 0 ? '' : 'jd_id = ' . $ZhiWei_num;
          

            $JobsList = $tmp_m->with(['recruitcompany', 'recruitopencity'])
                ->where('gold2', '>=', $YueXin)
                ->where('recruitcompany.city_id', $ShowCity_id)
                //->where('education','>=',$XueLi_num)
                ->where('education', ['>=', $XueLi_num], ['=', 0], 'or')
                ->where($ZhuSu_num)
                ->where($ZhiWei_num)
                ->where($Re_input)
                ->order($this->db_pf . 'recruit_job.toptime', 'desc')->limit($limit)->cache($cache)->select();
        }

        $ZhusD = $this->ZhusD;
        $FoodD = $this->FoodD;
        $SafeD = $this->SafeD;
        $EducationD = $this->EducationD;
        $AgeD = $this->AgeD;
        $comxinzn = $this->comxinzn;

        foreach ($JobsList as $key => &$row) {
            if ($row['recruitcompany']['xinzhi'] == null) {
                unset($JobsList[$key]);
                continue;
            }
            $row['zhusuname'] = $ZhusD[$row['stay']];
            $row['Safename'] = $SafeD[$row['safe']];
            $row['FoodDname'] = $FoodD[$row['food']];
            $row['Agename'] = $AgeD[$row['age']];
            $row['Educationname'] = $EducationD[$row['education']];

            if ($row['gold1'] == $row['gold2']) {
                $row['goldtext'] = ($row['gold1'] / 1000) . "K";
                if ($row['gold1'] == 3000) {
                    $row['goldtext'] .= "以下";
                }
                if ($row['gold1'] == 10000) {
                    $row['goldtext'] .= "以上";
                }
            } else {
                $row['goldtext'] = ($row['gold1'] / 1000) . "K-" . ($row['gold2'] / 1000) . "K";
            }

            $row['XinZhiname'] = $comxinzn[$row['recruitcompany']['xinzhi']];
        }

        $this->success('', $JobsList);
    }

    public function get_all_myjob()
    {
        $my_id = $this->auth->id;
        //首先查看是否有 企业，若没有则返回没有企业
        $CompanyD = Company::get(['user_id' => $my_id]);
        if (!$CompanyD) {
            $this->error('不存在企业');
            return;
        }

        $tmp_m = new Job();
        $JobsList = $tmp_m->with(['recruitcompany', 'recruitopencity'])->where('recruitcompany.user_id', $my_id)->order($this->db_pf . 'recruit_job.updatetime', 'desc')->select();

        $ZhusD = $this->ZhusD;
        $FoodD = $this->FoodD;
        $SafeD = $this->SafeD;
        $EducationD = $this->EducationD;
        $AgeD = $this->AgeD;
        $comxinzn = $this->comxinzn;
        foreach ($JobsList as $key => &$row) {
            if ($row['recruitcompany']['xinzhi'] == null) {
                unset($JobsList[$key]);
                continue;
            }
            $row['zhusuname'] = $ZhusD[$row['stay']];
            $row['Safename'] = $SafeD[$row['safe']];
            $row['FoodDname'] = $FoodD[$row['food']];
            $row['Agename'] = $AgeD[$row['age']];
            $row['Educationname'] = $EducationD[$row['education']];

            if ($row['gold1'] == $row['gold2']) {
                $row['goldtext'] = ($row['gold1'] / 1000) . "K";
                if ($row['gold1'] == 3000) {
                    $row['goldtext'] .= "以下";
                }
                if ($row['gold1'] == 10000) {
                    $row['goldtext'] .= "以上";
                }
            } else {
                $row['goldtext'] = ($row['gold1'] / 1000) . "K-" . ($row['gold2'] / 1000) . "K";
            }

            $row['XinZhiname'] = $comxinzn[$row['recruitcompany']['xinzhi']];
        }

        $outData = [];
        $outData['JobsList'] = $JobsList;
        $outData['Company'] = $CompanyD;

        $this->success('', $outData);

    }

    public function get_c_job()
    {
        $id = $this->request->post('id');
        $tmp_m = new Job();
        $row = $tmp_m->with(['recruitcompany', 'recruitopencity'])->where($this->db_pf . 'recruit_job.Id', $id)->limit(1)->select();
        if (!$row)
            $this->error(__('No Results were found'));
        //加入浏览记录
        $user_id = $this->auth->id;
        if ($user_id) {
            $hdata = ['job_id'=>$id,'user_id'=>$user_id,'createtime'=>time()];
            $hid = db('jobview_history')->where(['job_id'=>$id,'user_id'=>$user_id])->value('id');
            if ($hid) {
                db('jobview_history')->where('id',$hid)->update($hdata);
            }else{
                db('jobview_history')->insert($hdata);
            }
        }

        $row = $row[0];
        $row['createtime'] = date('Y-m-d',$row['createtime']);
        //格式化数据
        $ZhusD = $this->ZhusD;
        $FoodD = $this->FoodD;
        $SafeD = $this->SafeD;

        $row['zhusuname'] = $ZhusD[$row['stay']];
        $row['Safename'] = $SafeD[$row['safe']];
        $row['FoodDname'] = $FoodD[$row['food']];

        $AgeD = $this->AgeD;
        $row['Agename'] = $AgeD[$row['age']];

        $EducationD = $this->EducationD;
        $row['Educationname'] = $EducationD[$row['education']];

        //这里需要格式化一下 工资薪水
        if ($row['gold1'] == $row['gold2']) {
            $row['goldtext'] = $row['gold1'];
            if ($row['gold1'] == 3000) {
                $row['goldtext'] .= "以下";
            }
            if ($row['gold1'] == 10000) {
                $row['goldtext'] .= "以上";
            }
        } else {
            $row['goldtext'] = $row['gold1'] . " - " . $row['gold2'];
        }

        $comxinzn = $this->comxinzn;
        $row['XinZhiname'] = $comxinzn[$row['recruitcompany']['xinzhi']];
        //企业总共职位数量
        $row['jobnum'] = $tmp_m->where(['c_id'=>$row['c_id']])->count();

        $this->success('', $row);
    }

    public function del_my_company()
    {
        $id = $this->request->post('Id');
        $Companyd = Company::get(['Id' => $id]);
        $Companyd->delete();

        $this->success('', $id);
    }

    public function del_my_job()
    {
        $id = $this->request->post('Id');
        $Companyd = Job::get(['Id' => $id]);
        $Companyd->delete();

        $this->success('', $id);
    }

    public function edit_job()
    {
        $row = $this->request->post();
        //增加登录人的信息
        $row['user_id'] = $this->auth->id;

        $job = Job::get(['Id' => $row['id']]);

        //增加登录人的信息
        $row['user_id'] = $this->auth->id;

        $job->user_id = $row['user_id'];
        $job->name = '';
        $job->neednum = $row['neednum'];
        $job->age = $row['age'];
        $job->city_id = $row['city_id'];
        $job->content = $row['content'];

        $job->education = $row['education'];
        $job->food = $row['food'];
        $job->gold1 = $row['gold1'];
        $job->gold2 = $row['gold2'];
        $job->safe = $row['safe'];
        $job->stay = $row['stay'];
        $job->save();

        $this->success('', $row);
    }

    public function add_job()
    {
        $row = $this->request->post();
        //判断是否有发布次数
        $job_count = db('recruit_company')->where(['id'=>$row['c_id']])->value('job_count');
        if ($job_count<1) {
            $this->error('本月可发布次数已用完，本次发布失败');
        }
        //增加登录人的信息
        $row['user_id'] = $this->auth->id;

        $job = new Job;
        $job->user_id = $row['user_id'];
        $job->name = '';
        $job->neednum = $row['neednum'];
        $job->age = $row['age'];
        $job->c_id = $row['c_id'];
        $job->jd_id = $row['jd_id'];
        $job->city_id = $row['city_id'];
        $job->content = $row['content'];

        $job->education = $row['education'];
        $job->food = $row['food'];
        $job->gold1 = $row['gold1'];
        $job->gold2 = $row['gold2'];
        $job->safe = $row['safe'];
        $job->stay = $row['stay'];
        $job->save();
        //修改剩余次数
        db('recruit_company')->where(['id'=>$row['c_id']])->setDec('job_count');

        $this->success('', $row);


    }

    public function add_com()
    {
        $row = $this->request->post();

        //增加登录人的信息
        $row['user_id'] = $this->auth->id;

        //如果发现已经有公司了 则不予增加
        if (Company::get(['user_id' => $row['user_id']])) {
            $this->error('已有管理公司');
            return;
        }

        $Company = new Company;
        $Company->status = 0;//待审核
        $Company->name = $row['name'];
        $Company->tel = $row['tel'];
        $Company->no = $row['no'];
        $Company->xinzhi = $row['xinzhi'];
        $Company->adress = $row['adress'];
        $Company->content = $row['content'];
        $Company->user_id = $row['user_id'];

        $Company->cimage = !isset($row['cimage']) ? '' : $row['cimage'];
        $Company->cimages = !isset($row['cimages']) ? '' : $row['cimages'];

        $Company->zz_img = isset($row['zz_img']) ? $row['zz_img'] : '';
        $Company->fr_name = isset($row['fr_name']) ? $row['fr_name'] : '';
        $Company->fr_phone = isset($row['fr_phone']) ? $row['fr_phone'] : '';
        $Company->save();


        $this->success('', $row);
    }

    public function edit_com()
    {
        $row = $this->request->post();

        //增加登录人的信息
        $row['user_id'] = $this->auth->id;

        $Company = Company::get(['user_id' => $row['user_id']]);

        if (!$Company) {
            $this->error('关联公司错误');
            return;
        }

        $Company->name = isset($row['name']) ? $row['name'] : $Company->name;
        $Company->tel = isset($row['tel']) ? $row['tel'] : $Company->tel;
        $Company->no = isset($row['no']) ? $row['no'] : $Company->no;
        $Company->xinzhi = isset($row['xinzhi']) ? $row['xinzhi'] : $Company->xinzhi;
        $Company->adress = isset($row['adress']) ? $row['adress'] : $Company->adress;
        $Company->content = isset($row['content']) ? $row['content'] : $Company->content;
        $Company->user_id = isset($row['user_id']) ? $row['user_id'] : $Company->user_id;
        $Company->cimage = isset($row['cimage']) ? $row['cimage'] : $Company->cimage;
        $Company->cimages = isset($row['cimages']) ? $row['cimages'] : $Company->cimages;

        $Company->zz_img = isset($row['zz_img']) ? $row['zz_img'] : $Company->zz_img;
        $Company->fr_name = isset($row['fr_name']) ? $row['fr_name'] : $Company->fr_name;
        $Company->fr_phone = isset($row['fr_phone']) ? $row['fr_phone'] : $Company->fr_phone;

        $Company->save();
        $this->success('', $row);
    }

    public function upload()
    {
        $file = $this->request->file('files');
        if (empty($file)) {
            $this->error(__('No file upload or server upload limit exceeded'));
        }

        //判断是否已经存在附件
        $sha1 = $file->hash();

        $upload = Config::get('upload');

        preg_match('/(\d+)(\w+)/', $upload['maxsize'], $matches);
        $type = strtolower($matches[2]);
        $typeDict = ['b' => 0, 'k' => 1, 'kb' => 1, 'm' => 2, 'mb' => 2, 'gb' => 3, 'g' => 3];
        $size = (int)$upload['maxsize'] * pow(1024, isset($typeDict[$type]) ? $typeDict[$type] : 0);
        $fileInfo = $file->getInfo();
        $suffix = strtolower(pathinfo($fileInfo['name'], PATHINFO_EXTENSION));
        $suffix = $suffix ? $suffix : 'file';

        $mimetypeArr = explode(',', strtolower($upload['mimetype']));
        $typeArr = explode('/', $fileInfo['type']);

        //验证文件后缀
        /*if ($upload['mimetype'] !== '*' &&
            (
                !in_array($suffix, $mimetypeArr)
                || (stripos($typeArr[0] . '/', $upload['mimetype']) !== false && (!in_array($fileInfo['type'], $mimetypeArr) && !in_array($typeArr[0] . '/*', $mimetypeArr)))
            )
        ) {
            $this->error(__('Uploaded file format is limited'));
        }*/
        $fileInfo['suffix'] = $suffix;
        //禁止上传PHP和HTML文件
        if (in_array($fileInfo['type'], ['text/x-php', 'text/html']) || in_array($fileInfo['suffix'], ['php', 'html', 'htm', 'phar', 'phtml']) || preg_match("/^php(.*)/i", $fileInfo['suffix'])) {
            $this->error(__('上传格式限制'));
        }
        //Mimetype值不正确
        if (stripos($fileInfo['type'], '/') === false) {
            $this->error(__('上传格式限制'));
        }
        //验证文件后缀
        if (!($upload['mimetype'] === '*'
            || in_array($fileInfo['suffix'], $mimetypeArr) || in_array('.' . $fileInfo['suffix'], $mimetypeArr)
            || in_array($typeArr[0] . "/*", $mimetypeArr) || (in_array($fileInfo['type'], $mimetypeArr) && stripos($fileInfo['type'], '/') !== false))) {
            $this->error(__('上传格式限制'));
        }
        $replaceArr = [
            '{year}' => date("Y"),
            '{mon}' => date("m"),
            '{day}' => date("d"),
            '{hour}' => date("H"),
            '{min}' => date("i"),
            '{sec}' => date("s"),
            '{random}' => Random::alnum(16),
            '{random32}' => Random::alnum(32),
            '{filename}' => $suffix ? substr($fileInfo['name'], 0, strripos($fileInfo['name'], '.')) : $fileInfo['name'],
            '{suffix}' => $suffix,
            '{.suffix}' => $suffix ? '.' . $suffix : '',
            '{filemd5}' => md5_file($fileInfo['tmp_name']),
        ];
        $savekey = $upload['savekey'];
        $savekey = str_replace(array_keys($replaceArr), array_values($replaceArr), $savekey);

        $uploadDir = substr($savekey, 0, strripos($savekey, '/') + 1);
        $fileName = substr($savekey, strripos($savekey, '/') + 1);
        //
        $splInfo = $file->validate(['size' => $size])->move(ROOT_PATH . '/public' . $uploadDir, $fileName);
        if ($splInfo) {
            $imagewidth = $imageheight = 0;
            if (in_array($suffix, ['gif', 'jpg', 'jpeg', 'bmp', 'png', 'swf'])) {
                $imgInfo = getimagesize($splInfo->getPathname());
                $imagewidth = isset($imgInfo[0]) ? $imgInfo[0] : $imagewidth;
                $imageheight = isset($imgInfo[1]) ? $imgInfo[1] : $imageheight;
            }
            $params = array(
                'admin_id' => 0,
                'user_id' => (int)$this->auth->id,
                'filesize' => $fileInfo['size'],
                'imagewidth' => $imagewidth,
                'imageheight' => $imageheight,
                'imagetype' => $suffix,
                'imageframes' => 0,
                'mimetype' => $fileInfo['type'],
                'url' => $uploadDir . $splInfo->getSaveName(),
                'uploadtime' => time(),
                'storage' => 'local',
                'sha1' => $sha1,
            );
            //$attachment = model("attachment");
            $attachment = new Attachment();
            $attachment->data(array_filter($params));
            $attachment->save();
            \think\Hook::listen("upload_after", $attachment);
            $this->success(__('Upload successful'), [
                'url' => $uploadDir . $splInfo->getSaveName()
            ]);
        } else {
            // 上传失败获取错误信息
            $this->error($file->getError());
        }
    }

    //获取职位字典列表
    public function get_job_type(){
        $model = new \app\admin\model\JobDictionaries;
        $list = $model->where('status=1')->field('id,name')->limit(200)->select();

        $this->success('', $list);
    }


    //招人才，获取简历列表
    public function get_resumelist(){
        $Re_input = $this->request->post('Re_input');
        $ShowCity_id = $this->request->post('ShowCity_id');
        $YueXin = $this->request->post('YueXin');
        $XueLi_num = $this->request->post('XueLi_num');

        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');


        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;

        $Resume = new \app\admin\model\Resume;
        $XueLi_num--;
        $list = $Resume->field('*')
                ->where('work_city', $ShowCity_id)
                ->where('gold2', '>=', $YueXin)
                ->where('education', ['>=', $XueLi_num], ['=', 0], 'or')
                ->where(" (last_company_job like '%$Re_input%'or hope_work like '%$Re_input%')")
                ->order('updatetime desc')->limit($limit)->select();
        foreach ($list as $key => &$row) {
            if ($row['gold1'] == $row['gold2']) {
                $row['goldtext'] = ($row['gold1'] / 1000) . "K";
                if ($row['gold1'] == 3000) {
                    $row['goldtext'] .= "以下";
                }
                if ($row['gold1'] == 10000) {
                    $row['goldtext'] .= "以上";
                }
            } else {
                $row['goldtext'] = ($row['gold1'] / 1000) . "K-" . ($row['gold2'] / 1000) . "K";
            }
        }

        $this->success('',['list'=>$list]);

    }

    //获取职位浏览记录
    public function get_jobhistory(){
        $job_id = $this->request->post('job_id');
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        

        $tmp_m = new \app\admin\model\JobviewHistory();

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;

        $JobsList = $tmp_m->with(['job'])
            ->where('jobview_history.user_id',$this->auth->id)
            ->order('jobview_history.createtime', 'desc')->limit($limit)->select();

        $ZhusD = $this->ZhusD;
        $FoodD = $this->FoodD;
        $SafeD = $this->SafeD;
        $EducationD = $this->EducationD;
        $AgeD = $this->AgeD;
        $comxinzn = $this->comxinzn;

        //$cmodel = new Company();
        foreach ($JobsList as $key => &$row) {
            $row['recruitcompany'] = Company::get($row['job']['c_id']);
            if (!$row['recruitcompany'] || $row['recruitcompany']['xinzhi'] == null) {
                unset($JobsList[$key]);
                continue;
            }
            $row['zhusuname'] = $ZhusD[$row['job']['stay']];
            $row['Safename'] = $SafeD[$row['job']['safe']];
            $row['FoodDname'] = $FoodD[$row['job']['food']];
            $row['Agename'] = $AgeD[$row['job']['age']];
            $row['Educationname'] = $EducationD[$row['job']['education']];

            if ($row['job']['gold1'] == $row['job']['gold2']) {
                $row['goldtext'] = ($row['job']['gold1'] / 1000) . "K";
                if ($row['job']['gold1'] == 3000) {
                    $row['goldtext'] .= "以下";
                }
                if ($row['job']['gold1'] == 10000) {
                    $row['goldtext'] .= "以上";
                }
            } else {
                $row['goldtext'] = ($row['job']['gold1'] / 1000) . "K-" . ($row['job']['gold2'] / 1000) . "K";
            }

            $row['XinZhiname'] = $comxinzn[$row['recruitcompany']['xinzhi']];
            if ($row['job']['Id']==$job_id) {
                $row['checked'] = true;
            }else $row['checked'] = false;
            
        }

        $this->success('', $JobsList);
    }

    //获取职位简历投递记录
    public function get_job_resume(){
        $job_name = $this->request->post('Re_input');
        $c_id = $this->request->post('c_id');
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        

        $tmp_m = new \app\admin\model\Resumedelivery();

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;

        $List = $tmp_m->with(['job'])
            ->where('job.c_id',$c_id)
            ->where("job.name like '%$job_name%'")
            ->order('createtime', 'desc')->limit($limit)->select();
        foreach ($List as $key => &$val) {
            $val['createtime'] = date("Y-m-d H:i:s",$val['createtime']);
        }
        $this->success('', ['list'=>$List]);
    }

    //分享后职位发布次数加一
    public function add_job_count(){
        $c_id = $this->request->post('c_id');

        db('recruit_company')->where(['id'=>$c_id])->setInc('job_count');
        $this->success('职位发布次数+1');
    }

}
