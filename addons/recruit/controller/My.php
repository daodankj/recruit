<?php

namespace addons\recruit\controller;

/**
 * 我的
 */
class My extends Base
{

    protected $noNeedLogin = ['aboutus'];

    public function _initialize()
    {
        parent::_initialize();
    }

    /**
     * 提交报名
     */
    public function add_baoming()
    {
        $row = $this->request->post();
        //增加登录人的信息
        $row['user_id'] = $this->auth->id;

        $Jobfair = new \app\admin\model\Jobfair;
        $Jobfair->user_id = $row['user_id'];
        $Jobfair->block_id = $row['block_id'];
        $Jobfair->block_title = $row['block_title'];

        $Jobfair->tname = $row['tname'];
        $Jobfair->ttel = $row['ttel'];

        $Jobfair->save();

        $this->success('', $row);
    }


    /**
     * 这里拖取 本人的简历投递情况 和 简历是否存在的情况
     */
    public function Job_re_stat()
    {
        $id = $this->request->post('id');
        //首先判断 是否有简历
        $user_id = $this->auth->id;
        //$Resume = \app\admin\model\Resume::get(['user_id' => $user_id]);
        //$ResumeNum = count($Resume);
        $ResumeNum = \app\admin\model\Resume::where(['user_id' => $user_id])->count();

        //投递过简历信息
        $red_D = \app\admin\model\Resumedelivery::get(['user_id' => $user_id, 'job_id' => $id]);
        $row = array();
        $row['ResumeNum'] = $ResumeNum;
        $row['red_D'] = $red_D;

        $this->success('', $row);
    }

    /**
     * 投递简历
     */
    public function add_resume_resumedelivery()
    {
        $newData = new \app\admin\model\Resumedelivery();

        $user_id = $this->auth->id;
        $newData['user_id'] = $user_id;
        $Resume = \app\admin\model\Resume::get(['user_id' => $user_id]);

        $newData['re_id'] = $Resume['id'];
        $newData['re_name'] = $Resume['name'];
        $newData['re_tel'] = $Resume['tel'];

        $newData['job_id'] = $this->request->post('id');
        $newData['com_name'] = $this->request->post('com_name');
        $newData['job_name'] = $this->request->post('job_name');

        $newData->save();

        $this->success('', $newData);
    }

    /**
     * 投递简历
     */
    public function My_resumedelivery()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //登录人的信息
        $ResumedeliveryList = \app\admin\model\Resumedelivery::with(['job'])->where(['resumedelivery.user_id' => $this->auth->id])
            ->order('Id desc')
            ->limit(30)
            ->select();

        $ZhusD = ['不提供住宿', '提供住宿', '提供夫妻房'];
        $SafeD = ['不提供社保', '缴纳三险', '缴纳五险', '缴纳五险一金'];
        $EducationD = ['无学历要求', '小学', '初中', '高中', '大专', '本科', '研究生及以上'];

        foreach ($ResumedeliveryList as $key => &$item) {
            //这里格式化一下 住宿、社保、学历
            $item['job']['zhusuname'] = $ZhusD[$item['job']['stay']];
            $item['job']['Safename'] = $SafeD[$item['job']['safe']];
            $item['job']['Educationname'] = $EducationD[$item['job']['education']];
            //这里需要格式化一下 工资薪水
            if ($item['job']['gold1'] == $item['job']['gold2']) {
                $item['job']['goldtext'] = ($item['job']['gold1'] / 1000) . "K";
                if ($item['job']['gold1'] == 3000) {
                    $item['job']['goldtext'] .= "以下";
                }
                if ($item['job']['gold1'] == 10000) {
                    $item['job']['goldtext'] .= "以上";
                }
            } else {
                $item['job']['goldtext'] = ($item['job']['gold1'] / 1000) . "K-" . ($item['job']['gold2'] / 1000) . "K";
            }
        }


        $row = array();
        $row['ResumedeliveryList'] = $ResumedeliveryList;
        $this->success('', $row);
    }

    //预支申请记录
    public function get_advance(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\Advance;
        

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['user_id'=>$user_id])->limit($limit)->select();
        foreach ($list as $key => &$val) {
            if ($val['status']==0) {
                $val['statusName'] = "待初审";
            }else if($val['status']==1){
                $val['statusName'] = "待复审";
            }else if($val['status']==3){
                $val['statusName'] = "审核通过";
            }else if($val['status']==2){
                $val['statusName'] = "审核未通过";
            }
            $val['createtime'] = date('Y-m-d',$val['createtime']);
        }
        
        $data['list'] = $list;
        $this->success('',$data);
    }
    //预支申请
    public function advance_apply(){

        $money = $this->request->post('money');
        $user_id = $this->auth->id;
        if (!is_numeric($money)) {
            $this->error('金额格式错误');
        }
        //查看是否有签合同的劳动公司
        $info = db('active_bmlist')->where(['user_id'=>$user_id,'status'=>['>',1]])->find();//已签合同的
        if (!$info) {
            $this->error('您还未入职任何劳务公司，无法申请');
        }
        $data = [
            'user_id'   => $user_id,
            'username'  => $info['name'],
            'phone'  => $info['phone'],
            'idcard'    => $this->auth->idcard,
            'agent_user_id'=> $info['agent_user_id'],
            'service_id'=> $info['service_id'],
            'labor_id'  => $info['labor_id'],
            'money'     => $money,
            'createtime'=> time(),
            'status'    => $info['agent_user_id']?0:1
        ];
        $rs = db('advance')->insert($data);
        if ($rs) {
            $this->success('申请成功');
        }else $this->error('申请失败');
    }

    //我的工作记录
    public function my_work(){
        $model = new \app\admin\model\WorkRecod;
        $list = $model->where('user_id='.$this->auth->id)->order('id desc')->limit(20)->select();
        if ($list && $list[0]['unbind_time']==0) {//是否有工作中的
            $workinfo = $list[0];
        }else{
            $workinfo = [];
        }

        $this->success('',['list'=>$list,'workinfo'=>$workinfo]);
    }
    //我的打卡记录
    public function get_daka(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\Daka;
        

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->with(['work'])->where(['user_id'=>$user_id])->limit($limit)->order('id desc')->select();
        foreach ($list as $row) {
            $row->visible(['id', 'date', 'price', 'in_time', 'out_time','image','remark','work_time']);
            $row->visible(['work']);
            $row->getRelation('work')->visible(['name']);
        }
        $list = collection($list)->toArray();
        $data['list'] = $list;
        $this->success('',$data);
    }

    //我的合同
    public function my_contract(){
        $model = new \app\admin\model\ActiveBmlist;
        $info = $model->where('status>-1 and user_id='.$this->auth->id)->order('id desc')->find();
        //查看合同信息
        if ($info) {
            $cMod = new \app\admin\model\Contract;
            $cinfo = $cMod->where(['bm_id'=>$info['id']])->order('id desc')->find();
            if ($cinfo) {
                $status = ['待签署','待签署','部分签署','已拒签','已签署','已过期','已撤销'];
                $cinfo['statusName'] = isset($status[$cinfo['status']])?$status[$cinfo['status']]:'待签署';
            }
            $info['contract'] = $cinfo;
        }
        //获取合同模板文件
        $contract_tpl = config('site.contract_tpl');
        if ($info) {
            $contract_tpl = str_replace("{labor_name}",$info->labor_name,$contract_tpl);
            $contract_tpl = str_replace("{username}",$info->name,$contract_tpl);
            $contract_tpl = str_replace("{phone}",$info->phone,$contract_tpl);
        }
          

        $data = ['contract_tpl'=>$contract_tpl,'info'=>$info];

        $this->success('',$data);
    }
    //保存签名
    public function save_contract(){
        $id = $this->request->post('id');
        $c_url = $this->request->post('c_url');
        if (!$id || !$c_url) {
            $this->error('参数有误');
        }
        $model = new \app\admin\model\ActiveBmlist;
        $info = $model->get($id);
        if ($info->status!=1) {
            $this->error('您当前还不可签合同');
        }
        $info->c_url = $c_url;
        $info->contract_time = time();
        $info->status = 2;
        $info->save();

        $this->success('签名成功');
    }

    //工作结算记录列表
    public function get_finance_wages(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\FinanceWages;
        

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['user_id'=>$user_id])->limit($limit)->order('id desc')->select();

        $list = collection($list)->toArray();
        $data['list'] = $list;
        $this->success('',$data);
    }
    //确认结算金额
    public function check_finance_wages(){
        $id = $this->request->post('id');
        $status = $this->request->post('status');
        $remark = $this->request->post('remark','');

        $bmMod = new \app\admin\model\FinanceWages;
        $info = $bmMod->get($id);
        if (!$info || $info->status!=0) {
            $this->error('已确认，无需重新操作');
        }
        $info->status = $status;
        if ($remark) {
            $info->remark = $remark;
        }
        //$info->check_time = time();
        $rs = $info->save();
        if ($rs) {
            $this->success('操作成功');
        }else $this->error('操作失败');
    }

    //获取我的消息
    public function get_msglist(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\admin\model\UserNotice;
        

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['user_id'=>$user_id])->limit($limit)->order('id desc')->select();

        $list = collection($list)->toArray();
        $data['list'] = $list;
        $this->success('',$data);
    }

    //资金日志
    public function moneylog(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $user_id = $this->auth->id;
        $bmMod = new \app\common\model\MoneyLog;

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $list = $bmMod->where(['user_id'=>$user_id])->order('id desc')->limit($limit)->select();
        foreach ($list as $key => &$val) {
            $val['createtime'] = date('Y-m-d',$val['createtime']);
        }
        
        $data['list'] = $list;
        $this->success('',$data);
    }
}
