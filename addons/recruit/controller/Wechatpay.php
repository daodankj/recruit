<?php

namespace addons\recruit\controller;

use WeChatPay\Builder;
use WeChatPay\Crypto\Rsa;
use WeChatPay\Util\PemUtil;
use WeChatPay\Crypto\AesGcm;
use WeChatPay\Formatter;
use \think\Log;
use addons\third\model\Third;
use app\admin\model\User;
/**
 * 微信支付
 */
class Wechatpay extends Base
{

    protected $noNeedLogin = ['notifyUrl','index'];
    protected $merchantId = '1626988160';
    protected $merchantCertificateSerial = '655F9A697CE29DBEAC5D461F38229FEE1F520966';
    protected $appid = 'wx7f30b25fcc0561db';
    protected $instance;
    protected $wxapp = 'wxapp';//标记来自哪个小程序,默认八戒求才

    public function _initialize()
    {
        parent::_initialize();
        $this->init();
        //新增了一个小程序，后台没的配置，这里单独配置
        $this->wxapp = $this->request->param('wxapp','wxapp');
        if ($this->wxapp == 'wxapp2') {//八戒求才服务端
            $this->appid = 'wxb52b3d7b2144373c';
        }
    }

    public function init(){
        // 商户号
        $merchantId = $this->merchantId;
        // 从本地文件中加载「商户API私钥」，「商户API私钥」会用来生成请求的签名
        $merchantPrivateKeyFilePath = 'file://./cert/apiclient_key.pem';
        //$merchantPrivateKeyFilePath = file_get_contents($merchantPrivateKeyFilePath);
        $merchantPrivateKeyInstance = Rsa::from($merchantPrivateKeyFilePath, Rsa::KEY_TYPE_PRIVATE);

        // 「商户API证书」的「证书序列号」
        $merchantCertificateSerial = $this->merchantCertificateSerial;

        // 从本地文件中加载「微信支付平台证书」，用来验证微信支付应答的签名
        $platformCertificateFilePath = 'file://./cert/wechatpay_6504E9F14C29964E34F136EE56B5DCFAB6ADD515.pem';
        //$platformCertificateFilePath = file_get_contents($platformCertificateFilePath);
        $platformPublicKeyInstance = Rsa::from($platformCertificateFilePath, Rsa::KEY_TYPE_PUBLIC);

        // 从「微信支付平台证书」中获取「证书序列号」
        $platformCertificateSerial = PemUtil::parseCertificateSerialNo($platformCertificateFilePath);

        // 构造一个 APIv3 客户端实例
        $this->instance = Builder::factory([
            'mchid'      => $merchantId,
            'serial'     => $merchantCertificateSerial,
            'privateKey' => $merchantPrivateKeyInstance,
            'certs'      => [
                $platformCertificateSerial => $platformPublicKeyInstance,
            ],
        ]);
    }

    public function index(){
        //$info = ['out_trade_no'=>'order20220904161952609','trade_state'=>'SUCCESS'];
        //$this->notifyProcess($info);
        //Log::write('测试日志信息，这是警告级别','createorerError');
        $this->error("当前插件暂无前台页面");
    }

    //微信支付平台证书下载
    public function download(){
        // 发送请求
        $resp = $this->instance->chain('v3/certificates')->get(
            ['debug' => true] // 调试模式，https://docs.guzzlephp.org/en/stable/request-options.html#debug
        );
        echo $resp->getBody(), PHP_EOL;
    }

    public function createOrder(){
        $amount = $this->request->post('money');
        $type = $this->request->post('type',1);
        $job_id = $this->request->post('job_id',0);//职位置顶
        if ($type==2&&$job_id>0) {
            $amount = 1;//置顶暂写死1元 
            $toptime = db('recruit_job')->where(['Id'=>$job_id])->value('toptime');
            if ($toptime>1) {
                $this->error("该职位已置顶，无需重复置顶");
            }
        }
        if ($amount<=0) {
            $this->error("充值金额必须大于0元");
        }
        $code = 0;
        $user_id = $this->auth->id;
        $third = Third::where(['user_id' => $user_id, 'platform' => $this->wxapp])->find();
        if (!$third || !$third['openid']) {
            $this->error('账号未绑定小程序');
        }
        $payMode = new \app\admin\model\Payorder;
        $payMode->user_id = $user_id;
        $payMode->out_trade_no = 'order'.date('YmdHis').rand(100,999);
        $payMode->amount = $amount;
        $payMode->openid = $third['openid'];
        $payMode->remark = '微信充值';
        $payMode->type = $type;
        $payMode->job_id = $job_id;
        $payMode->createtime = time();
        $rs = $payMode->save();
        if (!$rs) {
            $this->error("创建订单失败");
        }
        try {
            $resp = $this->instance
            ->chain('v3/pay/transactions/jsapi')
            ->post(['json' => [
                'appid'     => $this->appid,
                'mchid'     => $this->merchantId,
                'out_trade_no' => $payMode->out_trade_no,
                'description'  => $payMode->remark,
                'notify_url'   => $this->request->domain().'/addons/recruit/Wechatpay/notifyUrl',
                'amount'       => [
                    'total'    => $payMode->amount*100,
                    'currency' => 'CNY'
                ],
                'payer'        => [
                    'openid'=> $payMode->openid
                ]
            ]]);

            $info = json_decode($resp->getBody(),true);
            $data = $this->getParams($info['prepay_id']);
            $code = $resp->getStatusCode();
            $payMode->prepay_id = $info['prepay_id'];
            $payMode->save();
        } catch (\Exception $e) {
            // 进行错误处理
            $info = $e->getMessage();
            Log::write('错误日志:'.$e->getMessage(),'createorerError');
            /*if ($e instanceof \GuzzleHttp\Exception\RequestException && $e->hasResponse()) {
                $r = $e->getResponse();
                echo $r->getStatusCode() . ' ' . $r->getReasonPhrase(), PHP_EOL;
                echo $r->getBody(), PHP_EOL, PHP_EOL, PHP_EOL;
            }
            echo $e->getTraceAsString(), PHP_EOL;*/
        }
        if($code == 200){
            $this->success('',$data);
        }else{
            $this->error('操作失败',$info);
        }
    }

    public function getParams($prepay_id){
        $merchantPrivateKeyFilePath = 'file://./cert/apiclient_key.pem';
        $merchantPrivateKeyInstance = Rsa::from($merchantPrivateKeyFilePath);

        $params = [
            'appId'     => $this->appid,
            'timeStamp' => (string)Formatter::timestamp(),
            'nonceStr'  => Formatter::nonce(),
            'package'   => 'prepay_id='.$prepay_id,
        ];
        $params += ['paySign' => Rsa::sign(
            Formatter::joinedByLineFeed(...array_values($params)),
            $merchantPrivateKeyInstance
        ), 'signType' => 'RSA'];
        return $params;
    }

    public function notifyUrl(){
        $inWechatpaySignature = $this->request->header('wechatpay-signature');// 请根据实际情况获取
        $inWechatpayTimestamp = $this->request->header('wechatpay-timestamp');// 请根据实际情况获取
        $inWechatpaySerial = $this->request->header('wechatpay-serial');// 请根据实际情况获取
        $inWechatpayNonce = $this->request->header('wechatpay-nonce');// 请根据实际情况获取
        $inBody = file_get_contents('php://input');
        //Log::write('回调数据body:'.$inBody,'wxcallback');
        $apiv3Key = 'fe3fa1e70a0f13b8e547cea92c5781b8';// 在商户平台上设置的APIv3密钥

        // 根据通知的平台证书序列号，查询本地平台证书文件，
        // 假定为 `/path/to/wechatpay/inWechatpaySerial.pem`
        $platformPublicKeyInstance = Rsa::from('file://./cert/wechatpay_6504E9F14C29964E34F136EE56B5DCFAB6ADD515.pem', Rsa::KEY_TYPE_PUBLIC);

        // 检查通知时间偏移量，允许5分钟之内的偏移
        $timeOffsetStatus = 300 >= abs(Formatter::timestamp() - (int)$inWechatpayTimestamp);
        $verifiedStatus = Rsa::verify(
            // 构造验签名串
            Formatter::joinedByLineFeed($inWechatpayTimestamp, $inWechatpayNonce, $inBody),
            $inWechatpaySignature,
            $platformPublicKeyInstance
        );
        if ($timeOffsetStatus && $verifiedStatus) {
            // 转换通知的JSON文本消息为PHP Array数组
            $inBodyArray = (array)json_decode($inBody, true);
            // 使用PHP7的数据解构语法，从Array中解构并赋值变量
            ['resource' => [
                'ciphertext'      => $ciphertext,
                'nonce'           => $nonce,
                'associated_data' => $aad
            ]] = $inBodyArray;
            // 加密文本消息解密
            $inBodyResource = AesGcm::decrypt($ciphertext, $apiv3Key, $nonce, $aad);
            // 把解密后的文本转换为PHP Array数组
            $inBodyResourceArray = (array)json_decode($inBodyResource, true);
            // print_r($inBodyResourceArray);// 打印解密后的结果
            Log::write('回调数据解码:'.$inBodyResource,'wxcallback');
            $this->notifyProcess($inBodyResourceArray);
        }else{
            Log::write('签名校验错误:'.$timeOffsetStatus,'wxcallback');
        }
    }

    //回调处理业务
    private function notifyProcess($data){
        $payMode = new \app\admin\model\Payorder;
        $payOrder = $payMode->where(['out_trade_no'=>$data['out_trade_no']])->find();
        if (!$payOrder || $payOrder->status!=0) {
            return;
        }
        if ($data['trade_state'] == 'SUCCESS') {
            $payOrder->status = 1;
            $payOrder->save();
            if ($payOrder['type']==2) {
                //职位置顶
                db('recruit_job')->where(['Id'=>$payOrder['job_id']])->update(['toptime'=>time()]);
            }else{
                //增加余额
                $u_mod = new User();
                $uinfo = $u_mod->where(['id'=>$payOrder->user_id])->find();
                if (!$uinfo) {
                    return;
                }
                $uinfo->remark = $payOrder['remark'];
                $uinfo->money += $payOrder['amount'];
                $uinfo->allowField(true)->save();
            }
        }
    }
}
