<?php

namespace addons\recruit\controller;


/**
 * 招募活动
 */
class ZmActive extends Base
{

    protected $noNeedLogin = ['info'];

    public function _initialize()
    {
        parent::_initialize();
        //处理失效报名,7天之前的
        $bmMod = new \app\admin\model\ActiveBmlist;       
        $day_befortime = time() - 7*24*3600;
        $bmMod->where(['status'=>0,'createtime'=>['<',$day_befortime]])->update(['status'=>-1]);
    }



    /**
     * 这里拖取 本人是否报名
     */
    public function info()
    {
        $id = $this->request->post('id');
        $agentId = $this->request->post('agentId');//团长分享的详情,查看团长手机号码

        $user_id = $this->auth->id;
        $activeInfo = \app\admin\model\RecruitActive::get($id);
        if(empty($activeInfo)){
            $this->error('该活动不存在');
        }
        $activeInfo['settlement_type'] = $activeInfo['settlement_type']==1?"月结":($activeInfo['settlement_type']==2?"周结":"日结");
        $activeInfo['createtime'] = date('Y-m-d',$activeInfo['createtime']);
        //判断 是否报名
        $is_baoming = \app\admin\model\ActiveBmlist::where(['status'=>['>',-1],'user_id'=>$user_id])->count();
        $row['activeInfo'] = $activeInfo;
        $row['isBaoming'] = $is_baoming?1:0;
        //判断是否是推广员，设置分享链接
        //$row['isAgent'] = $activeInfo['user_id'];//推广员发布的活动，报名信息归属推广员（团长分享除外）
        $isAgent = db('agent')->where(['user_id'=>$user_id])->value('id');
        if ($isAgent) {
            $row['isAgent'] = 1;
        }else{
            $row['isAgent'] = 0;
        }
        //获取推广员电话或团长电话
        if ($agentId) {//团长
            $linkInfo = db('recruit_head')->where('status=1 and user_id='.$agentId)->field('name,phone')->find();
            $row['link_phone'] = empty($linkInfo)?'':$linkInfo['phone'];
        }elseif($activeInfo['user_id']){
            $linkInfo = db('user')->where('id='.$activeInfo['user_id'])->field('username,mobile')->find();
            $row['link_phone'] = empty($linkInfo)?'':$linkInfo['mobile'];
        }else{
            $row['link_phone'] = '';
        }
        
        $this->success('', $row);
    }
    //活动报名
    public function add_baoming(){
        $row = $this->request->post();
        $user_id = $this->auth->id;
        //判断 是否报名
        $is_baoming = \app\admin\model\ActiveBmlist::where(['status'=>['>',-1],'user_id'=>$user_id])->count();
        if ($is_baoming) {
            $this->error('您已经报过名，不能重复报名');
        }
        //获取活动信息,补充归属信息
        $ainfo = db('recruit_active')->where('id='.$row['active_id'])->field('city_id,service_id,labor_id,user_id,start_date,end_date')->find();
        $now_date = date('Y-m-d');
        if ($now_date<$ainfo['start_date']) {
            $this->error('活动还未开始');
        }elseif ($now_date>$ainfo['end_date']) {
            $this->error('活动已结束');
        }
        $row['city_id'] = $ainfo['city_id'];
        $row['service_id'] = $ainfo['service_id'];
        $row['labor_id'] = $ainfo['labor_id'];
        if (!$row['agent_user_id']) {//如果不是团长分享的，归属推广员
            $row['agent_user_id'] = $ainfo['user_id'];
            $row['agent_type'] = 1;
        }else{
            $row['agent_type'] = 2;//标记是团长分享报名的
        }

        $active_m = new \app\admin\model\ActiveBmlist;
        $rs = $active_m->allowField(true)->save($row);
        if ($rs) {
            $this->success('报名成功');
        }else $this->error('报名失败');

        //$this->success('');
    }
    //招募活动列表
    public function get_active(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $Re_input = $this->request->post('Re_input');
        $ShowCity_id = $this->request->post('ShowCity_id');
        $status = $this->request->post('status',1);
        if ($Re_input != '') {
            $Re_input = " (name like '%$Re_input%'or company_name like '%$Re_input%') ";
        }
        if ($ShowCity_id) {
            $ShowCity_id = " city_id = $ShowCity_id";
        }

        $active_m = new \app\admin\model\RecruitActive;
        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $now_date = date('Y-m-d');
        if ($status==2) {//已结束
            $where1 = 'end_date<="'.$now_date.'"';
        }else{//进行中
            $where1 = 'start_date<="'.$now_date.'" and end_date>="'.$now_date.'"';
        }
        $activeList = $active_m->field('id,name,user_num,settlement_type,address,company_name,price,worktime,start_date,end_date,labor_id')
            ->where($where1)
            ->where($Re_input)
            ->where($ShowCity_id)
            ->order('id desc')->limit($limit)->select();
        foreach ($activeList as $key => &$val) {
            $val['settlement_type'] = $val['settlement_type']==1?"月结":($val['settlement_type']==2?"周结":"日结");
        }

        $this->success('', $activeList);
    }

    //推广员获取招募活动
    public function get_my_active(){
        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');

        $Re_input = $this->request->post('Re_input');
        $ShowCity_id = $this->request->post('ShowCity_id');
        if ($Re_input != '') {
            $Re_input = " (name like '%$Re_input%'or company_name like '%$Re_input%') ";
        }
        if ($ShowCity_id) {
            $ShowCity_id = " city_id = $ShowCity_id";
        }

        $active_m = new \app\admin\model\RecruitActive;
        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        $now_date = date('Y-m-d');
        $activeList = $active_m->field('id,name,user_num,settlement_type,address,company_name,price,worktime,start_date,end_date,labor_id')
            ->where('user_id='.$this->auth->id.' and start_date<="'.$now_date.'" and end_date>="'.$now_date.'"')
            ->where($Re_input)
            ->where($ShowCity_id)
            ->order('id desc')->limit($limit)->select();
        foreach ($activeList as $key => &$val) {
            $val['settlement_type'] = $val['settlement_type']==1?"月结":($val['settlement_type']==2?"周结":"日结");
        }

        $this->success('', $activeList);
    }


    //推广员新增招募活动
    public function add_active(){
        $user_id = $this->auth->id;
        $ainfo = \app\admin\model\Agent::get(['user_id'=>$user_id]);
        if(!$ainfo || $ainfo->status!=1){
            $this->error('您不是推广员，不能新增活动');
        }
        $row = $this->request->post();
        $row['head_open'] = $row['head_open']?1:0;
        if ( $row['head_open'] && $row['head_price']<=0 ) {
            $this->error('请输入团长提成');
        }
        $row['status'] = 0;
        $row['user_type'] = 1;
        $row['city_id'] = $ainfo->city_id;
        $row['service_id'] = $ainfo->service_id;
        $row['labor_id'] = $ainfo->labor_id;
        $row['worktime'] = $row['start_date']."至".$row['end_date'];
        $active_m = new \app\admin\model\RecruitActive;
        $rs = $active_m->allowField(true)->save($row);
        if ($rs) {
            $this->success('添加成功');
        }else $this->error('添加失败');
    }
}
