<?php

namespace addons\recruit\controller;

use addons\recruit\model\News;
/**
 * 技能培训
 */
class Sxjob extends Base
{

    protected $noNeedLogin = [];

    public function _initialize()
    {
        parent::_initialize();
    }



    //获取自己申请信息
    public function get_index(){
        $bannerList = [];

        $page = $this->request->post('page');
        $page_block = $this->request->post('page_block');
        $Re_input = $this->request->post('Re_input');

        $page = max(1, $page);
        $limit = ($page - 1) * $page_block . ',' . $page_block;
        //第一页才加载，防止翻页无用加载，让费资源
        /*if ($page==1) {
            $list = News::getBannerList(['type' => 'train','status' => 'normal', 'row' => 5, 'cache' => 0]);
            foreach ($list as $index => $item) {
                $bannerList[] = ['image' => cdnurl($item['image'], true), 'url' => '/', 'title' => $item['title'], 'name' => $item['name'], 'id' => $item['id']];
            }
        }*/  

        if ($Re_input != '') {
            $Re_input = " (name like '%$Re_input%'or intro like '%$Re_input%') ";
        }          

        $train_m = new \app\admin\model\SxJob;
        $tlist = $train_m->field('id,name,settlement_type,price,labor_id')
            ->where($Re_input)
            ->order('id desc')->limit($limit)->select();
        foreach ($tlist as $key => &$val) {
            $val['settlement_type'] = $val['settlement_type']==1?"月结":($val['settlement_type']==2?"周结":"日结");
        }

        $this->success('',['bannerList'=>$bannerList,'list'=>$tlist]);
    }

    //获取活动详情
    public function info(){
        $id = $this->request->post('id');

        $user_id = $this->auth->id;
        $activeInfo = \app\admin\model\SxJob::get($id);
        if(empty($activeInfo)){
            $this->error('该招聘不存在');
        }
        $activeInfo['settlement_type'] = $activeInfo['settlement_type']==1?"月结":($activeInfo['settlement_type']==2?"周结":"日结");
        $activeInfo['createtime'] = date('Y-m-d',$activeInfo['createtime']);
        //判断 是否报名
        $baoming = \app\admin\model\SxjobBm::where(['job_id'=>$id,'user_id'=>$user_id])->find();
        $activeInfo['isBaoming'] = empty($baoming)?0:1;
        
        
        
        $this->success('', $activeInfo);
    }

    //活动报名
    public function add_baoming(){
        $row = $this->request->post();
        $user_id = $this->auth->id;
        //查看详情
        $info = \app\admin\model\SxJob::get($row['job_id']);
        if (!$info) {
            $this->error('该职位不存在');
        }
        //判断 是否报名
        $is_baoming = \app\admin\model\SxjobBm::where(['job_id'=>$row['job_id'],'user_id'=>$user_id])->count();
        if ($is_baoming) {
            $this->error('您已经报过名，不能重复报名');
        }
  
        $row['user_name'] = $row['name'];
        $row['create_time'] = time();
        $active_m = new \app\admin\model\SxjobBm;
        $rs = $active_m->allowField(true)->save($row);
        if ($rs) {
            $this->success('报名成功');
        }else $this->error('报名失败');

        $this->success('');
    }
}
