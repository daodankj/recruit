<?php

return array(
    0 =>
        array(
            'name'    => 'enterimg',
            'title'   => '入口图片',
            'type'    => 'image',
            'content' =>
                array(),
            'value'   => '',
            'rule'    => '',
            'msg'     => '',
            'tip'     => '',
            'ok'      => '',
            'extend'  => '',
        ),
    8 =>
        array(
            'name'    => 'logo',
            'title'   => '商城LOGO',
            'type'    => 'image',
            'content' =>
                array(),
            'value'   => '',
            'rule'    => '',
            'msg'     => '',
            'tip'     => '',
            'ok'      => '',
            'extend'  => '',
        ),
    1 =>
        array(
            'name'    => 'scorestatus',
            'title'   => '开启积分商城',
            'type'    => 'radio',
            'content' =>
                array(
                    1 => '启用',
                    0 => '关闭',
                ),
            'value'   => '1',
            'rule'    => 'required',
            'msg'     => '',
            'tip'     => '',
            'ok'      => '',
            'extend'  => '',
        ),
    2 =>
        array(
            'name'    => 'open_clear',
            'title'   => '清理订单',
            'type'    => 'radio',
            'content' =>
                array(
                    1 => '启用',
                    0 => '关闭',
                ),
            'value'   => '1',
            'rule'    => 'required',
            'msg'     => '',
            'tip'     => '',
            'ok'      => '',
            'extend'  => '',
        ),
    3 =>
        array(
            'name'    => 'open_clear_time',
            'title'   => '清理间隔（分钟）',
            'type'    => 'number',
            'content' => '',
            'value'   => '1',
            'rule'    => 'required',
            'msg'     => '',
            'tip'     => '24小时内未完成付款将会关闭订单',
            'ok'      => '',
            'extend'  => '',
        ),
    4 =>
        array(
            'name'    => 'order_out_time',
            'title'   => '订单删除',
            'type'    => 'number',
            'content' => '',
            'value'   => '1',
            'rule'    => '',
            'msg'     => '',
            'tip'     => '未付款订单x天后直接删除。',
            'ok'      => '',
            'extend'  => '',
        ),
    5 =>
        array(
            'name'    => 'order_prefix',
            'title'   => '订单前缀',
            'type'    => 'string',
            'content' => '',
            'value'   => 'SN',
            'rule'    => 'required',
            'msg'     => '',
            'tip'     => '',
            'ok'      => '',
            'extend'  => '',
        ),
    6 =>
        array(
            'name'    => 'domain',
            'title'   => '绑定二级域名前缀',
            'type'    => 'string',
            'content' =>
                array(),
            'value'   => '',
            'rule'    => '',
            'msg'     => '',
            'tip'     => '',
            'ok'      => '',
            'extend'  => '',
        ),
    7 =>
        array(
            'name'    => 'rewrite',
            'title'   => '伪静态',
            'type'    => 'array',
            'content' =>
                array(),
            'value'   =>
                array(
                    'goods/index'   => '/leescoregoods$',
                    'order/index'   => '/leescoreorder$',
                    'index/index'   => '/score$',
                    'address/index' => '/address$',
                ),
            'rule'    => '',
            'msg'     => '',
            'tip'     => '',
            'ok'      => '',
            'extend'  => '',
        ),
);
