
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for __PREFIX__leescore_address
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `zip` varchar(60) DEFAULT NULL COMMENT '邮编',
  `mobile` varchar(20) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `region` varchar(200) DEFAULT NULL COMMENT '省 / 区',
  `city` varchar(200) DEFAULT NULL COMMENT '城市',
  `xian` varchar(200) DEFAULT NULL COMMENT '县 / 区',
  `address` text,
  `status` int(11) DEFAULT NULL COMMENT '默认收货地址',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `firstname` varchar(255) DEFAULT NULL COMMENT '姓氏',
  `lastname` varchar(255) DEFAULT NULL COMMENT '名字',
  `isdel` int(11) DEFAULT '0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='地址表';

-- ----------------------------
-- Records of __PREFIX__leescore_address
-- ----------------------------
INSERT INTO `__PREFIX__leescore_address` VALUES ('1', '2', '531000', '123123123', '中国', '广西壮族自治区/南宁市/兴宁区', null, null, '123231', '0', '1527133237', '123123', '123123', '0');
INSERT INTO `__PREFIX__leescore_address` VALUES ('2', '3', '12323', '12345678912', '中国', '广西壮族自治区/南宁市/西乡塘区', null, null, '123', '1', '1527727978', '123', '123', '0');

-- ----------------------------
-- Table structure for __PREFIX__leescore_order_goods
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_order_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `buy_num` int(11) DEFAULT NULL COMMENT '购买数量',
  `score` decimal(10,0) DEFAULT NULL COMMENT '积分价格',
  `money` varchar(255) DEFAULT NULL COMMENT '货币价格',
  `goods_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `goods_thumb` varchar(255) DEFAULT NULL COMMENT '商品缩略图',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8 COMMENT='订单-商品表';

-- ----------------------------
-- Table structure for __PREFIX__leescore_ads
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '广告标题',
  `thumb` char(255) DEFAULT NULL COMMENT '广告图片',
  `open_mode` enum('0','1') DEFAULT '0' COMMENT '打开方式:0=原网页,1=新开页面',
  `path_url` varchar(255) DEFAULT '#' COMMENT '跳转地址',
  `position` enum('slider','activicy','other') DEFAULT 'other' COMMENT '广告位:slider=轮播处,activicy=热门活动,other=其它位置',
  `starttime` int(11) DEFAULT NULL COMMENT '起始时间',
  `endtime` int(11) DEFAULT NULL COMMENT '截止时间',
  `weigh` int(11) DEFAULT '50' COMMENT '排序',
  `status` enum('0','1') DEFAULT '0' COMMENT '状态:0=未启用,1=启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='广告位管理';

-- ----------------------------
-- Records of __PREFIX__leescore_ads
-- ----------------------------
INSERT INTO `__PREFIX__leescore_ads` VALUES ('1', '轮播图一', '/uploads/20181128/bccf36b9b061921ba0250c02d6164ff4.jpg', '0', 'https://www.fastadmin.net/', 'slider', '1524538292', '1733021492', '50', '1');
INSERT INTO `__PREFIX__leescore_ads` VALUES ('2', '轮播图二', '/uploads/20181128/2f927bc54a834b3868b34ba7bfab4023.jpg', '1', 'https://www.fastadmin.net/', 'slider', '1524538372', '1733021572', '50', '1');
INSERT INTO `__PREFIX__leescore_ads` VALUES ('4', '轮播图三', '/uploads/20181128/257db14af1ffe05c7d376d40fb886e0c.jpg', '1', 'https://www.fastadmin.net/', 'slider', '1540194154', '1733038954', '50', '1');
-- ----------------------------
-- Table structure for __PREFIX__leescore_category
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名',
  `topid` int(11) DEFAULT '0' COMMENT '顶级父类ID',
  `category_id` int(11) DEFAULT '0' COMMENT '上级菜单',
  `path` varchar(255) DEFAULT NULL COMMENT '完整路径',
  `weigh` int(11) DEFAULT '50' COMMENT '权重排序',
  `status` varchar(30) DEFAULT 'normal' COMMENT '状态',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='商品分类';

-- ----------------------------
-- Records of __PREFIX__leescore_category
-- ----------------------------
INSERT INTO `__PREFIX__leescore_category` VALUES ('20', '话费&QQ', '0', '0', null, '20', 'normal', '1545716736');
INSERT INTO `__PREFIX__leescore_category` VALUES ('21', '话费兑换', '20', '20', null, '21', 'normal', '1545716759');
INSERT INTO `__PREFIX__leescore_category` VALUES ('22', 'QQ业务', '20', '20', null, '22', 'normal', '1545716769');
INSERT INTO `__PREFIX__leescore_category` VALUES ('23', '爱生活', '0', '0', null, '23', 'normal', '1545716781');
INSERT INTO `__PREFIX__leescore_category` VALUES ('24', '享品质', '0', '0', null, '24', 'normal', '1545716791');
INSERT INTO `__PREFIX__leescore_category` VALUES ('26', '品时尚', '0', '0', null, '26', 'normal', '1545716856');
INSERT INTO `__PREFIX__leescore_category` VALUES ('27', '家居日用', '23', '23', null, '27', 'normal', '1545716951');
INSERT INTO `__PREFIX__leescore_category` VALUES ('28', '五谷杂粮', '23', '23', null, '28', 'normal', '1545716968');
INSERT INTO `__PREFIX__leescore_category` VALUES ('29', '数码科技', '24', '24', null, '29', 'normal', '1545716993');
INSERT INTO `__PREFIX__leescore_category` VALUES ('30', '厨房电器', '24', '24', null, '30', 'normal', '1545717046');
INSERT INTO `__PREFIX__leescore_category` VALUES ('33', '时尚衣饰', '26', '26', null, '33', 'normal', '1545717186');
INSERT INTO `__PREFIX__leescore_category` VALUES ('34', '户外运动', '26', '26', null, '34', 'normal', '1545717281');


-- ----------------------------
-- Table structure for __PREFIX__leescore_cart
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `number` int(11) DEFAULT NULL,
  `goods_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `createtime` int(11) DEFAULT NULL COMMENT '加入时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='购物车';

-- ----------------------------
-- Table structure for __PREFIX__leescore_exchangelog
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_exchangelog` (
  `uid` int(11) DEFAULT NULL COMMENT '兑换用户',
  `goods_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `order_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `createtime` int(11) DEFAULT NULL COMMENT '兑换时间',
  `ip` varchar(60) DEFAULT NULL COMMENT '客户端Ip'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品购买日志';



-- ----------------------------
-- Table structure for __PREFIX__leescore_goods
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `category_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `name` varchar(255) DEFAULT NULL COMMENT '商品标题',
  `paytype` enum('0','1','2') DEFAULT '0' COMMENT '出售模式:0=积分模式,1=货币模式,2=金钱+货币模式',
  `type` enum('0','2','1') DEFAULT '0' COMMENT '商品类型: 0=实物商品,1=虚拟商品,2=积分',
  `status` enum('0','1','2') DEFAULT '2' COMMENT '商品状态:0=删除,1=仓库,2=上架',
  `createtime` int(11) DEFAULT NULL COMMENT '商品发布时间',
  `body` text COMMENT '商品详情',
  `rule` text COMMENT '兑换规则',
  `thumb` varchar(255) DEFAULT NULL COMMENT '图片缩略图',
  `pics` text COMMENT '商品图集',
  `weigh` int(11) DEFAULT '50' COMMENT '权重排序',
  `updatetime` int(11) DEFAULT NULL COMMENT '更新时间',
  `stock` int(11) DEFAULT '0' COMMENT '商品库存',
  `scoreprice` int(11) DEFAULT '0' COMMENT '所需积分',
  `firsttime` int(11) DEFAULT NULL COMMENT '开放时间',
  `lasttime` int(11) DEFAULT NULL COMMENT '结束时间',
  `money` float(11,2) DEFAULT '0.00' COMMENT '价格',
  `usenum` int(11) DEFAULT '0' COMMENT '已兑换',
  `flag` set('index','hot','recommend','new') DEFAULT '' COMMENT '推荐:index=首页,hot=热门,recommend=推荐,new=最新',
  `max_buy_number` int(11) DEFAULT '-1' COMMENT '限购数量',
  `fenxiao_status` enum('0','1') DEFAULT '0' COMMENT '商品分销: 0=不开启,1=开启',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='推广类型';

-- ----------------------------
-- Records of __PREFIX__leescore_goods
-- ----------------------------
INSERT INTO `__PREFIX__leescore_goods` VALUES ('22', '21', '移动30元话费', '0', '1', '2', '1545717404', '每个用户限购一次', '每个用户限购一次', '/uploads/20181225/f3ec64b481d0fc88105a262dc1dc0a39.jpg', '', '22', '1545717685', '999', '3000', '1545717287', '1577253287', '0.00', '0', 'index,hot,recommend,new', '1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('23', '21', '移动50元话费', '0', '1', '2', '1545717450', '每个用户限购一次', '每个用户限购一次', '/uploads/20181225/e21be3d66e0d261d11d57f2964a49aef.jpg', '', '23', '1545717685', '999', '5000', '1545717405', '1577253405', '0.00', '0', 'index,hot,recommend,new', '1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('24', '21', '联通30元话费', '0', '1', '2', '1545717507', '联通30元话费每个用户限购一次', '联通30元话费每个用户限购一次', '/uploads/20181225/38df34c650d3376a39096dcd777e5b0d.jpg', '', '24', '1545717685', '999', '3000', '1545717452', '1577253452', '0.00', '0', 'index,hot,recommend,new', '1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('25', '21', '联通50元话费', '0', '1', '2', '1545717544', '每个用户限购一次', '每个用户限购一次', '/uploads/20181225/9ef628a4b03e6244241399588ad26258.jpg', '', '25', '1545717685', '999', '5000', '1545717508', '1577253508', '0.00', '0', 'index,hot,recommend,new', '1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('26', '22', '10Q币', '0', '1', '2', '1545717606', '10Q币每个用户限购一次', '10Q币每个用户限购一次', '/uploads/20181225/ba16b91ae5af24efe4dbc35cd22c82fe.jpg', '', '26', '1545717699', '999', '1000', '1545717550', '1577253550', '0.00', '0', '', '1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('27', '22', '30Q币', '0', '1', '2', '1545717659', '每个用户限购一次', '每个用户限购一次', '/uploads/20181225/e58d2f86b120ed6049783294bc067fcc.jpg', '', '27', '1545717699', '999', '3000', '1545717608', '1577253608', '0.00', '0', '', '1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('28', '22', '1个月QQ会员', '0', '1', '2', '1545717771', '1个月QQ会员', '1个月QQ会员', '/uploads/20181225/479e3d7724eeb95918b117610ec15069.jpg', '', '28', '1545717771', '999', '1000', '1545717700', '1577253700', '0.00', '0', '', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('29', '22', '3个月QQ会员', '0', '1', '2', '1545717803', '3个月QQ会员', '3个月QQ会员', '/uploads/20181225/848fdb720e6eb3c1de97d7194bdfedb8.jpg', '', '29', '1545717803', '0', '3000', '1545717773', '1577253773', '0.00', '0', '', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('30', '27', '洗发水一瓶', '2', '0', '2', '1545717868', '洗发水一瓶', '洗发水一瓶', '/uploads/20181225/2a10abd0a2f509567c24e74913e15577.jpg', '', '30', '1545717868', '999', '100', '1545717810', '1577253810', '30.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('31', '27', '木梳子一把', '0', '0', '2', '1545717917', '木梳子一把', '木梳子一把', '/uploads/20181225/da1c2e3b0c375e312ad2e8f314a5a202.jpg', '', '31', '1545717917', '999', '1000', '1545717873', '1577253873', '0.00', '0', '', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('32', '28', '5Kg大米一袋', '0', '0', '2', '1545717974', '5Kg大米一袋', '5Kg大米一袋', '/uploads/20181225/c60c54cdea80880008118a02ba0c06d5.jpg', '', '32', '1545718041', '999', '100', '1545717921', '1577253921', '15.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('33', '27', '护目镜一副', '0', '0', '2', '1545718035', '护目镜一副', '护目镜一副', '/uploads/20181225/f0fd6b72d9de6d7607fec656931e1814.jpg', '', '33', '1545718129', '999', '4800', '1545717975', '1577253975', '5.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('34', '28', '食用油一罐', '0', '0', '2', '1545718124', '食用油一罐', '食用油一罐', '/uploads/20181225/e4d80b086ce5c57dda467d6333a14526.jpg', '', '34', '1545718124', '999', '10', '1545718042', '1577254042', '20.00', '0', '', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('35', '29', 'xxx热水壶', '0', '0', '2', '1545718203', '热水壶热水壶热水壶热水壶', '热水壶', '/uploads/20181225/22a57872698b40ad206ae19ee7d57b4c.jpg', '', '35', '1545718203', '999', '350', '1545718148', '1577254148', '30.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('36', '29', '家用望远镜', '0', '0', '2', '1545718250', '家用望远镜', '家用望远镜', '/uploads/20181225/849f5e518a30e1f6f63bcb32257bc509.jpg', '', '36', '1545718250', '999', '100', '1545718205', '1577254205', '490.00', '0', '', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('37', '30', '电动煮蛋器', '0', '0', '2', '1545718310', '电动煮蛋器', '电动煮蛋器', '/uploads/20181225/30b36ea4c03b51a25b849f198a3c6324.jpg', '', '37', '1545718373', '9999', '2000', '1545718251', '1577254251', '2.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('38', '30', '电吹风机', '0', '0', '2', '1545718366', '电吹风机', '电吹风机', '/uploads/20181225/756433054d36400f9e8fcd3f4ed01e85.jpg', '', '38', '1545718366', '988', '10', '1545718316', '1577254316', '10.00', '0', '', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('39', '29', '多接口数据线 ', '0', '0', '2', '1545718446', '多接口数据线', '多接口数据线', '/uploads/20181225/3304e6bc0fa0fa583289ac35a6ad7f3d.jpg', '', '39', '1545718446', '999', '100', '1545718377', '1577254377', '5.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('40', '29', '移动充电宝', '0', '0', '2', '1545718491', '移动充电宝', '移动充电宝', '/uploads/20181225/4cb535f4bf00cd1626b48568cec1ed96.jpg', '', '40', '1545718491', '888', '5900', '1545718447', '1577254447', '0.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('41', '33', '高端商务表', '0', '0', '2', '1545718581', '高端商务表', '高端商务表', '/uploads/20181225/bf463c688f70408694bc93939e623e4e.jpg', '', '41', '1545718581', '777', '1000', '1545718530', '1577254530', '0.01', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('42', '33', '精美男士皮带', '0', '0', '2', '1545718621', '精美男士皮带', '精美男士皮带', '/uploads/20181225/1f2f8a63751580deb874aa25a8b9141a.jpg', '', '42', '1545718621', '888', '0', '1545718583', '1577254583', '90.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('43', '33', '佛珠手链', '0', '0', '2', '1545718674', '佛珠手链', '佛珠手链', '/uploads/20181225/51db16e842d773025eb966dc412150ad.jpg', '', '43', '1545718674', '0', '680', '1545718622', '1577254622', '0.10', '0', '', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('44', '34', '木吉他', '0', '0', '2', '1545718731', '木吉他', '木吉他', '/uploads/20181225/e37309ada78bbe341a977e02d238c281.jpg', '', '44', '1545718783', '8888', '8888', '1545718684', '1577254684', '8000.00', '0', 'index,hot,recommend,new', '-1', '0');
INSERT INTO `__PREFIX__leescore_goods` VALUES ('45', '34', '电吉他', '0', '0', '2', '1545718774', '电吉他', '电吉他', '/uploads/20181225/9c807eebea894c8d3ccc3e34cb97f37b.jpg', '', '45', '1545718774', '8888', '8888', '1545718732', '1577254732', '66666.00', '0', 'index,hot,recommend,new', '-1', '0');

-- ----------------------------
-- Table structure for __PREFIX__leescore_order
-- ----------------------------
CREATE TABLE IF NOT EXISTS `__PREFIX__leescore_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单ID',
  `uid` int(11) unsigned DEFAULT NULL COMMENT '用户ID',
  `address_id` int(11) DEFAULT NULL COMMENT '收货地址',
  `order_id` varchar(255) NOT NULL COMMENT '订单号',
  `trade_id` varchar(255) DEFAULT NULL COMMENT '回执单号',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `pay` enum('0','1','2','3') DEFAULT '0' COMMENT '付款状态:0=未付款,1=已付款,2=已退款',
  `status` enum('-3','-2','-1','0','1','2','3','4','5') DEFAULT '0' COMMENT '订单状态:-3=异常订单,-2=驳回, -1=取消订单,0=未支付,1=已支付,2=已发货,3=已签收,4=退货中,5=已退款',
  `paytime` int(11) DEFAULT NULL COMMENT '付款时间',
  `paytype` enum('score','money') DEFAULT 'score' COMMENT '支付方式:score=积分支付,money=余额支付',
  `isdel` int(11) DEFAULT '0' COMMENT '1',
  `other` text COMMENT '备注',
  `virtual_sn` varchar(255) DEFAULT NULL COMMENT '虚拟券序列号/快递单号',
  `virtual_name` varchar(255) DEFAULT NULL COMMENT '虚拟券名称/物流公司',
  `virtual_go_time` int(11) DEFAULT NULL COMMENT '发货时间',
  `virtual_sign_time` int(11) DEFAULT NULL COMMENT '签收时间',
  `score_total` int(11) DEFAULT '0' COMMENT '支付积分',
  `money_total` float(11,2) DEFAULT NULL COMMENT '支付货币',
  `result_other` varchar(255) DEFAULT NULL COMMENT '回馈备注',
  `trade_score` int(11) DEFAULT NULL COMMENT '实际支付积分',
  `trade_money` float(255,2) DEFAULT NULL COMMENT '实际付款',
  `trade_time` varchar(60) DEFAULT NULL COMMENT '实际付款时间',
  `auth_clear_level` int(11) DEFAULT '0' COMMENT '自动清理:0=清理,1=不清理',
  `pay_msg` varchar(255) DEFAULT '' COMMENT '订单处理结果',
  `weigh` int(11) DEFAULT '50',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='订单表';
