<?php

namespace addons\leescore\controller;

use addons\epay\library\Service;
use think\addons\Controller;
use think\Db;

/**
 * 感谢 Fastadmin 开源系统无偿支持
 * 积分商城支付回调处理
 * By:龙组的赵日天
 * Time: 2018-12-14
 * Version: v1.1.0
 */

/**
 * API接口控制器
 *
 * @package addons\leescore\controller
 */
class Api extends Controller
{

    protected $config = [];
    protected $order = null;
    protected $cart = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->order = model('addons\leescore\model\Order');
        $this->cart = model('addons\leescore\model\Cart');
    }

    /**
     * 默认方法
     */
    public function index()
    {
        $this->error();
    }

    /**
     * 支付成功回调
     */
    public function notify()
    {
        $type = $this->request->param('type');
        $pay = Service::checkNotify($type);
        if (!is_dir(LOG_PATH . 'leescorelogs')) {
            mkdir(LOG_PATH . 'leescorelogs');
        }
        //异步回调
        if (!$pay) {
            $path = LOG_PATH . 'leescorelogs' . DS . 'notify_error.log';
            if (!file_exists($path)) {
                $fso = fopen($path, 'w+');
                fwrite($fso, date('Y-m-d H:i:s') . ' ------------------ --- 签名验证失败\r\n');
                fclose($fso);
            } else {
                file_put_contents($path, date('Y-m-d H:i:s') . " ------------------ --- 签名验证失败\r\n", FILE_APPEND);
            }
            return;
        }

        //返回支付宝回调参数
        $result = $pay->verify();
        //网站订单号
        $w['order_id'] = $result['out_trade_no'];
        //获取订单信息
        $order = Db::name('leescore_order')->where($w)->find();
        //此订单已经回调成功了，通知支付商不需要再来通知了。 -3为异常订单，支付金额与下单金额不一致
        if ($order['status'] > 0 || $order['status'] == '-3') {
            echo $pay->success();
            return;
        }


        //回执订单号
        $data['trade_id'] = $result['trade_no'];
        //实际支付金额
        $data['trade_money'] = $result['total_amount'];
        //支付宝支付时间
        $data['trade_time'] = $result['timestamp'];
        //本地支付时间
        $data['paytime'] = time();
        $data['paytype'] = $type;


        //金额核对
        if ($result['total_amount'] != $order['money_total']) {
            //订单金额和付款金额不一致,订单将会被标记为异常状态
            $data['status'] = '-3';
            $data['pay_msg'] = '用户实际支付金额与下单金额不一致！应付金额为：【' . $order['money_total'] . "】元，实际支付金额为：【" . $result['total_amount'] . "】";
        } else {
            //金额无误，订单完成。
            $data['status'] = '1';
            $data['pay'] = '1';
        }

        // 启动事务
        Db::startTrans();
        try {
            //更新订单状态
            Db::name('leescore_order')->where('id', $order['id'])->update($data);
            //扣除消费积分。
            \app\common\model\User::score(-$order['score_total'], $this->auth->id, '消费积分兑换商品');

            //从购物车中移除已经购买的商品
            $order_goods = Db::name('leescore_order_goods')->where('order_id', $order['id'])->select();
            if ($order_goods) {
                foreach ($order_goods as $key => $val) {
                    $cart = $this->cart->get($val['goods_id']);
                    if ($cart) {
                        $cart->delete();
                    }
                }
            }
            Db::commit();
        } catch (Exception $e) {
            // 回滚
            Db::rollback();
        }
        //下面这句必须要执行,且在此之前不能有任何输出
        echo $pay->success();
        return;
    }

    /**
     * 支付成功返回
     */
    public function returnx()
    {
        $type = $this->request->param('type');
        $result = Service::checkReturn($type);
        if (!$result) {
            $this->error('签名错误');
        }
        //你可以在这里定义你的提示信息,但切记不可在此编写逻辑
        $this->success(__('pay success'), addon_url("leescore/order/index"));
        return;
    }

}
