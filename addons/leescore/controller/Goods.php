<?php

namespace addons\leescore\controller;

use think\addons\Controller;
use think\Db;

/**
 * 感谢 Fastadmin 开源系统无偿支持
 * 积分商城商品类
 * By:龙组的赵日天
 * Time: 2018-12-14
 * Version: v1.1.0
 */
class Goods extends Base
{

    protected $model = null;
    protected $member = null;
    protected $layout = 'default';
    protected $category = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('addons\leescore\model\Goods');
        $this->category = model('addons\leescore\model\Category');
        //开始展示时间
        $w['firsttime'] = ['elt', time()];
        //结束展示时间
        $w['lasttime'] = ['egt', time()];
        $w['status'] = 2;
        $usenumdesc = $this->model->where($w)->order('usenum desc')->limit(10)->select();
        $this->view->assign('usenumdesc', $usenumdesc);
    }

    // 兑换列表
    public function index()
    {
        //只要顶级分类
        $w['topid'] = 0;
        $w['status'] = 'normal';
        $goods_cate = $this->category->getCategory($w);
        $this->assign("goods_cate", $goods_cate);
        return $this->fetch();
    }


    //AJAX分页
    public function getList()
    {
        //排序
        $field = input("get.field");
        $sort = input('get.sort');
        $keywords = $this->request->param('keywords');
        //dump($keywords);
        //商品类型
        (input('?get.paytype') && !empty(input('get.paytype'))) ? $paytype = trim(input('get.paytype')) : '';

        if (isset($paytype)) {
            //根据积分和价格去识别商品类型。
            switch ($paytype) {
                case 'score':
                    //纯积分商品的货币价格应该为0
                    $w['scoreprice'] = ['gt', 0];
                    $w['money'] = ['elt', 0];
                    break;
                case 'money':
                    //纯货币商品的积分设置应该为0
                    $w['scoreprice'] = ['elt', 0];
                    $w['money'] = ['gt', 0];
                    break;
                case 'sam':
                    //积分+货币的商品 积分及货币价格都不应该为0.
                    $w['scoreprice'] = ['gt', 0];
                    $w['money'] = ['gt', 0];
                default:
                    //默认不筛选类型。
                    break;
            };
        }

        //商品分类
        (input('?get.category') && !empty(input('get.category'))) ? $category = trim(input('get.category')) : '';
        //积分查询
        $score_start = (input('?get.score_start') && !empty(input('get.score_start'))) ? abs((int)input("get.score_start")) : false;
        $score_end = (input('?get.score_end') && !empty(input('get.score_end'))) ? abs((int)input("get.score_end")) : false;

        //价格查询
        $money_start = (input('?get.money_start') && !empty(input('get.money_start'))) ? abs((int)input("get.money_start")) : false;
        $money_end = (input('?get.money_end') && !empty(input('get.money_end'))) ? abs((int)input("get.money_end")) : false;


        //关键词查询
        if (isset($keywords) && !empty($keywords)) {
            $w['name'] = ['like', "%" . $keywords . "%"];
        }


        //积分查询
        if ($score_start && $score_end) {
            $w['scoreprice'] = ['between', [$score_start, $score_end]];
        } else {
            if ($score_start) {
                $w['scoreprice'] = ['egt', $score_start];
            } else {
                if ($score_end) {
                    $w['scoreprice'] = ['elt', $score_end];
                }
            }
        }

        //价格查询
        if ($money_start && $money_end) {
            $w['money'] = ['between', [$money_start, $money_end]];
        } else {
            if ($money_start) {
                $w['money'] = ['egt', $money_start];
            } else {
                if ($money_end) {
                    $w['money'] = ['elt', $money_end];
                }
            }
        }

        //上架中的商品  0=删除，2=上架中，1=仓库中
        $w['status'] = 2;
        //开始展示时间
        $w['firsttime'] = ['elt', time()];
        //结束展示时间
        $w['lasttime'] = ['egt', time()];
        if (isset($category)) {
            $c['topid'] = $category;
            $c['status'] = 'normal';
            $arr = [];
            $cateMenu = $this->category->where($c)->field('id')->select();

            //将顶级分类加入查询条件中
            $arr[] = $category;
            foreach ($cateMenu as $k => $v) {
                $arr[] = $v['id'];
            }

            $category = implode(",", $arr);
            $w['category_id'] = ['in', $category];
        }

        $page = request()->param('page');
        $order = request()->param('field') . " " . request()->param('sort');
        $list = $this->model->where($w)->order($order)->paginate(16, false,
            [
                'path'     => 'javascript:ajaxPage([PAGE]);',
                'page'     => $page,
                'var_page' => 'page',
            ]
        );

        $html = count($list, true) < 1 ? "<div class='col-xs-12'><small>没有符合条件的产品哦~</small></div>" : '';
        foreach ($list as $key => $vo) {

            $borderRight = (($key + 1) % 4 == 0) ? 'style="border-right:0px solid #eee;">' : '>';
            $detailURL = addon_url('leescore/goods/details', array('gid' => $vo['id']));
            $disabled = ($vo['stock'] < 1) ? 'disabled' : '';
            $caseMoney = "<span class=\"text-red\"><i class=\"text-yellow glyphicon glyphicon-piggy-bank\"></i> " . $vo['scoreprice'] . __('score') . "</span> + <span class=\"text-red\"><i class=\"text-yellow glyphicon glyphicon-yen\"></i> " . $vo['money'] . __('money') . "</span>";
            $beihuo = ((int)$vo['stock'] < 1) ? "<span class=\"pull-right label label-warning\">备货中...</span>" : '';

            //var_dump($borderRight . $detailURL . $disabled .$caseMoney);

            $html .= "<li class=\"padding-min col-sm-3\" " . $borderRight . "<div class=\"f-list-box-item\"><a target='_blank' href=\"" . $detailURL . "\"><img class=\"pro-thumb center-block\" src=\"" . $vo['thumb'] . "\"></a><div class=\"pro-title h4 text-center text-muted more\"><a target='_blank' href=\"" . addon_url('leescore/goods/details',
                    array('gid' => $vo['id'])) . "\">" . $vo['name'] . "</a></div><div class=\"col-sm-12 padding-none line-height-25\">" . $caseMoney . "</div><div class=\"col-sm-12 padding-none text-muted\"><span>" . __('stock') . "：" . $vo['stock'] . "</span> " . $beihuo . " </div><div class=\"col-sm-12 margin-top text-center padding-none\"><a data-paramid=\"" . $vo['id'] . "\" data-url=\"" . addon_url('leescore/cart/add') . "\" href=\"javascript:;\" class=\"btn btn-warning btn-sm add-cart " . $disabled . "\">+" . __('add cart') . "</a> &nbsp;&nbsp;&nbsp;<a target='_blank' href=\"" . $detailURL . "\" class=\"btn btn-danger btn-sm " . $disabled . "\">" . __('buy') . "</a></div><div class=\"clearfix\"></div></div></li></div>";
        }

        $html .= "<div class=\"clearfix\"></div>";
        $page = $list->render();
        return json(['list' => $html, 'page' => $page]);
    }

    //商品详情
    public function details()
    {
        $id = input('get.gid');
        $w['id'] = $id;
        $detail = $this->model->where($w)->find();
        $add_config = get_addon_config('leescore');
        $this->view->assign('item', $detail);
        return $this->view->fetch();
    }


    //已有订单，并数量相同直接读取现有的订单
    public function getOrderOne()
    {
        $id = $this->request->param('id');
        $map['id'] = $id;
        $order = Db::name('leescore_order')->where($map)->find();
        //dump($order);
        $info = $this->model->where("id", $order['goods_id'])->find();
        $w['userid'] = $this->auth->id;
        $w['isdel'] = 0;
        $addressList = Db::name('leescore_address')->where($w)->order('status desc')->select();
        $this->view->assign('add', $addressList);
        $this->view->assign('item', $info);
        $this->view->assign('order', $order);
        $this->view->engine->layout(false);
        return $this->view->fetch('getorderone');
    }
}
