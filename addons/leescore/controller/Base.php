<?php

namespace addons\leescore\controller;

use think\Addons;
use think\Db;
use think\Config;
use think\Session;
use think\Request;
use think\Exception;
use think\Loader;

/**
 * 感谢 Fastadmin 开源系统无偿支持
 * 积分商城基类
 * By:龙组的赵日天
 * Time: 2018-12-14
 * Version: v1.1.0
 */
class Base extends \think\addons\Controller
{

    public function _initialize()
    {
        parent::_initialize();

        //$isCommand = Request::instance()->has('HTTP_HOST', 'server');

        //dump($isCommand);
        //命令行模式下不进行加载。
        //if ($isCommand) {
        $this->loadGlobalsConfig();
        //}
    }

    public function loadGlobalsConfig()
    {
        //获取插件配置信息
        $config = get_addon_config($this->addon);

        //是否开启订单清理
        if ((int)$config['open_clear'] == 1) {
            if (!is_dir(LOG_PATH)) {
                mkdir(LOG_PATH);
            }
            //判断日志目录是否存在
            if (!file_exists(LOG_PATH . "leescorelogs" . DS . "clear_log.log")) {
                //文件目录不存在，创建目录
                if (!is_dir(LOG_PATH . "leescorelogs" . DS)) {
                    mkdir(LOG_PATH . "leescorelogs" . DS);
                }
                $f = fopen(LOG_PATH . "leescorelogs" . DS . "clear_log.log", "w+") or die(LOG_PATH . "。缺少写入权限。");
                fclose($f);
            }

            //读取文件内容（里面只有时间戳）
            $val = file_get_contents(LOG_PATH . "leescorelogs" . DS . "clear_log.log");
            $open_time = (int)$config['open_clear_time'];

            //下次执行时间
            $times = (int)$val + ($open_time * 60);

            //上次执行时间
            if ($val == '' || $times < time()) {
                Db::startTrans();
                try {
                    //创建订单，但是没有走到付款步骤的订单
                    $cleTime = time() - ((int)$config['order_out_time'] * 3600);
                    $www['createtime'] = ['elt', $cleTime];
                    $www['auth_clear_level'] = 0;
                    $row = Db::name('leescore_order')->where($www)->select();
                    if ($row) {

                        //删除已创建但未选择地址的订单。
                        foreach ($row as $k => $v) {
                            $order_goods = Db::name("leescore_order_goods")->where("order_id", $v['id'])->select();
                            if ($order_goods) {
                                // 恢复库存
                                foreach ($order_goods as $key => $val) {
                                    Db::name("leescore_goods")->where("id", $val['goods_id'])->setInc('stock', $val['buy_num']);
                                }
                            } else {
                                Db::name("leescore_goods")->where("id", $v['goods_id'])->setInc('stock', $val['buy_num']);
                            }
                            $del['order_id'] = $v['id'];
                            Db::name("leescore_order_goods")->where($del)->delete();
                            Db::name('leescore_order')->where("id", $v['id'])->delete();
                        }
                    }

                    Db::commit();
                } catch (\Exception $e) {
                    // 回滚事务
                    Db::rollback();
                }

                //记录当前执行时间
                $file = fopen(LOG_PATH . "leescorelogs" . DS . "clear_log.log", "w+");
                fwrite($file, time());
                fclose($file);
            }
        }//订单清理


        //公共头部分类菜单
        $w['status'] = 'normal';
        $w['category_id'] = 0;

        //只要一级菜单
        $cates = Db::name('leescore_category')->order("weigh asc")->where($w)->limit(10)->select();

        //焦点按钮是否存在
        $cateID = input("?get.cateType") ? input("get.cateType") : false;

        //如果是商品详情页进来的，先找到该商品的分类。
        if (!$cateID && input('?get.gid')) {
            $gid = input('get.gid');
            $goods = model('addons\leescore\model\Goods')->where('id', $gid)->find();
            $cateID = $goods ? $goods['category_id'] : false;
        }

        if ($cateID) {
            $navs = model('addons\leescore\model\Category')->where('id', $cateID)->find();
            //存在焦点按钮参数，对非一级菜单做寻父处理
            $cateActive = $navs['topid'] == 0 ? $navs['id'] : $navs['topid'];
        } else {
            $cateActive = '';
        }

        session('cateActive', false);
        session("cateMenu", $cates);
        session('cateActive', $cateActive);
        Config::set("addonConfig", $config);
    }
}
