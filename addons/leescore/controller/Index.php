<?php

namespace addons\leescore\controller;

use think\addons\Controller;
use think\Db;

/**
 * 感谢 Fastadmin 开源系统无偿支持
 * 积分商城主页
 * By:龙组的赵日天
 * Time: 2018-12-14
 * Version: v1.1.0
 */
class Index extends Base
{

    protected $model = null;
    protected $layout = 'default';

    public function _initialize()
    {
        parent::_initialize();
        $this->model = model('addons\leescore\model\Goods');
        $adsModel = model('addons\leescore\model\Ads');
        $slider = $adsModel->getSliderList();
        $activicy = $adsModel->getActivicyList();
        $cate = $this->getCateList(10);
        $other = $adsModel->getOtherList();

        foreach ($cate as $k => $v) {
            //当前分类下所有下级分类
            $cate[$k]['navs'] = $this->getAllNavs($v);
            //当前分类下所有下级分类商品集合
            $cate[$k]['goods'] = $this->goodsList($v);
        }

        //dump($cate);
        $this->view->assign('cate', $cate);
        $this->assign('activicy', $activicy);
        $this->assign('slider', $slider);
        $this->assign('other', $other);
    }


    //查询所有一级菜单
    public function getCateList($limit = 15)
    {
        $w['status'] = 'normal';
        $w['category_id'] = 0;
        $row = Db::name('leescore_category')->order("weigh asc")->where($w)->limit($limit)->select();
        return $row;
    }

    //查询每个一级分类下所有下级分类
    public function getAllNavs($data)
    {
        $map['topid'] = $data['id'];
        $map['status'] = 'normal';
        $navs = Db::name('leescore_category')->where($map)->select();
        return $navs;
    }

    //XF列表数据提取
    public function goodsList($data)
    {
        $ids = $this->getAllNavs($data);
        (count($ids) == 0) ? $ids[0] = $data : $ids = $ids;
        $arrs = array();
        foreach ($ids as $key => $val) {
            $arrs[] = $val['id'];
        }

        $arrs = $arrs ? implode(",", $arrs) : false;

        $w['category_id'] = array('IN', $arrs);
        $w['status'] = '2';
        $lists = Db::name('leescore_goods')->where($w)->limit(8)->order('weigh asc')->select();
        return $lists;
    }


    public function index()
    {
        //热门商品
        $hotList = $this->model->getHotGoods();

        //推荐商品
        $recomm = $this->model->getRecommendGoods();

        //首页推荐商品
        $index = $this->model->getIndexGoods();

        //最新推荐商品
        $new = $this->model->getNewGoods();
        $this->view->assign('index', $index);

        $this->view->assign('title', __('home'));
        $this->assign('hotList', $hotList);
        return $this->fetch("index");
    }

}
