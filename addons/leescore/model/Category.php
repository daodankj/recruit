<?php

namespace addons\leescore\model;

use think\Model;

/**
 * 分类模型
 */
class Category Extends Model
{

    protected $name = "leescore_category";
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'int';
    // 定义时间戳字段名
    protected $createTime = true;
    protected $updateTime = false;
    // 追加属性
    protected $append = [
    ];
    protected static $config = [];

    //自定义初始化
    protected static function init()
    {
        $config = get_addon_config('leescore');
        self::$config = $config;
    }

    public function getCateNext()
    {
        return $this->hasMany('category', 'topid');
    }

    /**
     * @param array filter 筛选条件  TP5 SQL查询条件数组写法
     * @param string limit 查询条数  例： 10,10  或者  10
     * @return array 查询结果集
     */
    public function getCategory($filter = [], $limit = '')
    {
        $cate = Category::with('getCateNext')->where($filter)->limit($limit)->order('weigh', 'asc')->select();
        return $cate;
    }
}
